
DATA_PATH = '/Users/pietro/data/judith/'
# DATA_VERSION = 'data_V11_18_04'
# DATA_VERSION = 'data_V13_20_09'
#DATA_VERSION = 'data_V14_16_11'
DATA_VERSION = 'data_V15_06_05_2024'


PIXELS_TO_CM = 0.130

SKIP_SESSIONS = ['R2_S02']

# SESSION_IDS = [
#     'R2_S02',
#     #'R2_S03',
#     'R2_S04',
#     #'R2_S05',
#     #'R2_S06',
#     #'R2_S08',
#
#     #'R3_S02',
#     #'R3_S05',
#     #'R3_S06',
#     #'R3_S07',
#     #'R3_S08',
#
#     'R4_S01',
#     'R4_S02',
#     'R4_S03',
#     'R4_S04',
#     #'R4_S05',
#     #'R4_S06',
#     #'R4_S07',
#     #'R4_S08',
#
#     'R5_S01',
#     'R5_S02',
#     'R5_S04',
#     #'R5_S05',
#     #'R5_S06'
#     ]


AREAS = ['DG', 'CA3', 'CA1']
TREATMENTS = ['VEH', 'CNO']


bad_units = ['uid_R4_S01_015',
             'uid_R4_S01_016',
             'uid_R4_S01_020',
             'uid_R4_S01_024',

             'uid_R4_S02_007',
             'uid_R4_S02_012',
             'uid_R4_S02_013',
             'uid_R4_S02_020',
             'uid_R4_S02_024',
             'uid_R4_S02_031',
             'uid_R4_S02_034',
             'uid_R4_S02_037',
             'uid_R4_S02_040',
             'uid_R4_S02_044',
             'uid_R4_S02_047',
             'uid_R4_S02_048',
             'uid_R4_S02_049',

             'uid_R4_S03_010',
             'uid_R4_S03_013',
             'uid_R4_S03_014',
             'uid_R4_S03_015',
             'uid_R4_S03_016',
             'uid_R4_S03_017',
             'uid_R4_S03_018',
             'uid_R4_S03_020', # not sure
             'uid_R4_S03_021', # not sure
             'uid_R4_S03_034',
             'uid_R4_S03_038',
             'uid_R4_S03_039',

             'uid_R4_S04_000',
             'uid_R4_S04_004',
             'uid_R4_S04_008',
             'uid_R4_S04_009',
             'uid_R4_S04_014',
             'uid_R4_S04_015',
             'uid_R4_S04_018',

             'uid_R4_S05_009',
             'uid_R4_S05_010',
             'uid_R4_S05_011',
             'uid_R4_S05_012',
             'uid_R4_S05_013',
             'uid_R4_S05_014',
             'uid_R4_S05_032',
             'uid_R4_S05_033',
             'uid_R4_S05_034',
             'uid_R4_S05_035',
             'uid_R4_S05_036',
             'uid_R4_S05_037',
             'uid_R4_S05_038',
             'uid_R4_S05_039',
             'uid_R4_S05_040',
             'uid_R4_S05_041',
             'uid_R4_S05_042',
             'uid_R4_S05_045',
             'uid_R4_S05_046',
             'uid_R4_S05_047',

             'uid_R4_S06_003',

             'uid_R5_S01_004',
             'uid_R5_S01_008',
             'uid_R5_S01_012',
             'uid_R5_S01_017',
             'uid_R5_S01_018',
             'uid_R5_S01_020',
             'uid_R5_S01_024',
             'uid_R5_S01_025',
             'uid_R5_S01_027',
             'uid_R5_S01_029',
             'uid_R5_S01_031',
             'uid_R5_S01_034',
             'uid_R5_S01_042',
             'uid_R5_S01_043',

             'uid_R5_S02_001',
             'uid_R5_S02_003',
             'uid_R5_S02_004',
             'uid_R5_S02_005',
             'uid_R5_S02_009',
             'uid_R5_S02_017',
             'uid_R5_S02_023',

             'uid_R5_S03_000',
             'uid_R5_S03_001',
             'uid_R5_S03_003',
             'uid_R5_S03_004',
             'uid_R5_S03_005',
             'uid_R5_S03_008',
             'uid_R5_S03_009',
             'uid_R5_S03_012',
             'uid_R5_S03_019',
             'uid_R5_S03_033',
             'uid_R5_S03_044',
             'uid_R5_S03_063',

             'uid_R5_S04_027',
             'uid_R5_S04_031',
             'uid_R5_S04_036',

             'uid_R5_S07_009',
             'uid_R5_S07_011',
             'uid_R5_S07_012',
             'uid_R5_S07_013',
             'uid_R5_S07_014',

             'uid_R5_S09_020',
             'uid_R5_S09_022',
             'uid_R5_S09_023',
             'uid_R5_S09_024',
             'uid_R5_S09_025',
             'uid_R5_S09_026',
             ]