import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
import pandas as pd
from session import Session
from utils import *


"""
Loads pseudopopulation results (for convenience grouped in combos)
and plots them in two ways
1. Using sns.lineplot and the standard deviation of the bootstraps
2. Computing confidence intervals of the bootstraps. This can be 
expanded to also have a significance measure at each time point
and plot it as an horizontal line (as I did for Julien).
"""


plot_format = 'png'


# alpha for confidence intervals
CI_alpha = 0.95

settings_name = 'apr18_25trials_40units'
experiments = ['separation']

settings_name = 'apr18_20trials_25units_exclude_bad'
experiments = ['first_poke_side',
               'previous_poke_side',
               'correct',
               'separation']

settings_name = 'apr18_fine_20trials_25units_exclude_bad'
experiments = ['separation']

settings_name = 'apr18_fine_stim_on_20trials_25units_exclude_bad'
experiments = ['separation']

settings_name = 'apr22_fine_stim_on_35trials_100units_exclude_bad'
experiments = ['separation']

settings_name = 'apr26_reward_35trials_50units_exclude_bad'

settings_name = 'apr26_reward_25trials_50units_exclude_bad'

experiments = ['block_performance_binarized',
               'successful_block_binarized']

experiments = ['correct']

settings_name = 'sep26_choice_st'
experiments = ['first_poke_side',
               'previous_poke_side',
               'separation']

plots_folder = os.path.join(DATA_PATH, 'plots', 'pseudopop', settings_name)
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)


# --- LOAD RESULTS -------------------------------------------------------------

dfs = []
pars_dict = {}
for experiment in experiments:
    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_pseudopop', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    out = pickle.load(open(output_full_path, 'rb'))
    df = out['decoding_scores']
    pars = out['pars']
    pars_dict[experiment] = pars
    dfs.append(df)
df = pd.concat(dfs)

x0, x1 = df['time'].min(), df['time'].max()

#assert df['group_size'].unique().shape[0] == 1
#df =  df[df['group_size'] == df['group_size'].unique()[0]]


# --- LOAD TRIAL INFO ----------------------------------------------------------

tf, uf = load_tf_uf_global(session_ids=get_preprocessed_sessions(),
                        align_event=pars_dict[experiments[0]]['align_event'],
                       max_reaction_time=pars_dict[experiments[0]]['max_reaction_time'])

# --- LINEPLOT WITH STANDARD DEVIATION ERROR BARS ------------------------------

for experiment in experiments:
    for treatment in TREATMENTS:

        dx = df[(df['experiment'] == experiment)
                & (df['treatment'] == treatment)]

        f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
        sns.lineplot(data=dx, x='time', y='score', ci='sd',
                     ax=ax, hue='area_spikes',
                     palette=decoding_features_palette)
        ax.axhline(0.5, ls=':', c='grey')
        ax.set_ylim([0.4, 1])
        ax.set_xlim([x0, x1])
        plot_events(tf, ax)
        ax.set_xlim([x0, x1])
        ax.set_ylabel(experiment)
        ax.set_xlabel('Time [ms]')
        ax.set_ylabel('Decoding score ({})\nTarget: {}'.format(treatment, experiment))
        #ax.get_legend().remove()
        sns.despine()
        plt.tight_layout()

        plot_name = 'pseudopop_compare_areas_{}_{}_{}.{}'.format(experiment, treatment, settings_name, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



for area in AREAS+['all_areas']:
    for experiment in experiments :

        dx = df[(df['experiment'] == experiment)
                & (df['area_spikes'] == area)]

        if dx.shape[0] > 0:

            f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
            sns.lineplot(data=dx, x='time', y='score', ci='sd',
                         ax=ax, hue='treatment',
                         palette={t:decoding_features_palette[area] for t in TREATMENTS},
                         style='treatment', dashes=dashes_dict)
            ax.set_ylabel(experiment)
            ax.axhline(0.5, ls=':', c='grey')
            ax.set_ylim([0.4, 1])
            ax.set_xlim([x0, x1])
            plot_events(tf, ax)
            ax.set_xlim([x0, x1])
            ax.set_xlabel('Time [ms]')
            ax.set_ylabel('Decoding score ({})\nTarget: {}'.format(area, experiment))

            #ax.set_title(area)
            #ax.get_legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'pseudopop_compare_treatments_{}_{}_{}.{}'.format(area, experiment, settings_name, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

# TODO updates
if False:
    # --- LINEPLOT WITH CONFIDENCE INTERVALS ---------------------------------------

    dx = pd.DataFrame(columns=['experiment', 'time', 'mean', 'median',
                               'ci1', 'ci2', 'sig'])

    for experiment in experiments:
        for time in df['time'].unique():

            stats = df[(df['experiment'] == experiment) & (df['time'] == time)]['score']

            # TODO
            # p = ((1.0 - alpha) / 2.0) * 100
            # lower = max(0.0, np.percentile(stats, p))
            # p = (alpha + ((1.0 - alpha) / 2.0)) * 100
            # upper = min(1.0, np.percentile(stats, p))
            # sig = lower > 0.5 and upper > 0.5

            lower = np.quantile(stats, q=(1 - CI_alpha) / 2)
            upper = np.quantile(stats, q=(1 - (1 - CI_alpha) / 2))
            sig = lower > 0.5 and upper > 0.5

            dx.loc[dx.shape[0], :] = [experiment, time, stats.mean(), stats.median(),
                                      lower, upper, sig]
            # err.append([obs_val - lower, upper - obs_val])

    for col in ['time', 'mean', 'median', 'ci1', 'ci2']:
        dx[col] = pd.to_numeric(dx[col])


    f, ax = plt.subplots(1, 1, figsize=[3, 3])

    for i, experiment in enumerate(experiments):
        da = dx[dx['experiment'] == experiment]
        ax.plot(da['time'], da['mean'], c=experiment_palette[experiment], linewidth=3)

        ax.fill_between(da['time'], da['ci1'], da['ci2'], color=experiment_palette[experiment],
                        alpha=0.3, linewidth=0)

        # s = np.ma.masked_where(~da['sig'], (1+(i/100)) * np.ones(shape=da.shape[0]))
        # ax.plot(da['time'], s, c=areas_palette[area], linewidth=2)

        # t = da['time'][da['sig']]
        # x = (1+(i/100)) * np.ones(shape=t.shape[0])
        # ax.scatter(t, x, c=experiment_palette[experiment], linewidth=2,s=20, marker='_')


    if warped:
        ax.axvline(0,
                   c=sns.xkcd_rgb['grey'],
                   ls='--')
        for modality in modalities:
            ax.axvline(median_reaction_times[modality],
                       c=modality_palette[modality],
                       ls='--')

    #ax.set_ylabel('Correct decoding in V+M trials\n(decoding accuracy)')
    ax.set_ylabel(combo_labels[combo])


    ax.set_xlabel('Time [ms]')
    sns.despine()
    plt.tight_layout()

    plot_name = 'decoding_over_time_combo_CI_err_{}_{}.{}'.format(settings_name, '_'.join(experiments), plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)






