import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *

#settings_name = 'apr26_reward_25trials_50units_exclude_bad'
settings_name = 'sep26_stim_on'


min_trials_per_class = 25
min_units = 1

# TIME PARAMETERS
binned_data_settings = 'sep20_stim_on'

# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 500
min_total_units = 20
subpop_size = 50
subpop_size_all = 150
n_bootstraps = 200

# experiments = ['first_poke_side',
#                'previous_poke_side',
#                'correct',
#                'separation']

experiments = ['first_poke_side',
               'previous_poke_side',
               'separation']

#experiments = ['separation']

decoding_features = ['all_areas'] + AREAS + ['trackerdata+licks']


SESSION_IDS = get_preprocessed_sessions()


# --- LOAD DATA ----------------------------------------------------------------

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


for experiment in experiments:

    # --- OUTPUT FILES ---------------------------------------------------------
    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_pseudopop', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)


    sdf, tf_sel = select_data_experiment(experiment=experiment,
                                 session_ids=SESSION_IDS,
                                 align_event=binned_data_pars['align_event'],
                                 max_reaction_time=binned_data_pars['max_reaction_time'],
                                 filter_trials=True,
                                 return_trials=True,
                                 bad_units=bad_units)

    for features in decoding_features:
        for treatment in TREATMENTS:

            if np.isin(features, AREAS+['all_areas']) :
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf['treatment'] == treatment) &
                              (sdf[features] >= min_units)]
                tot_n_units = sdf_sel[features].sum()
                print(experiment, treatment, features, tot_n_units)
    print('\n\n')

    # --- GET ALL THE DATA -----------------------------------------------------

    binned_data_dict = {s : {} for s in sdf.index}

    for session_id in sdf.index :

        session = Session(session_id=session_id)
        session.load_data(bad_units=bad_units)
        session.load_binned_data(binned_data_settings=binned_data_settings,
                                 bad_units=bad_units)
        session.align_event_times(align_event=binned_data_pars['align_event'],
                                  convert_to_seconds=True)
        session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

        trial_ids = session.tf.index

        for features in decoding_features:

            if np.isin(features, AREAS) :
                unit_indx = session.binned_data['unit_indx_per_area'][features]
                binned_data = session.binned_data['binned_spikes']
                binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

            elif features == 'all_areas' :
                binned_data = session.binned_data['binned_spikes']
                unit_indx = session.binned_data['unit_indx']
                binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

            elif features == 'trackerdata' :
                binned_data = session.binned_data['binned_trackerdata']
                binned_data = [binned_data[t] for t in trial_ids]

            elif features == 'licks' :
                binned_data = session.binned_data['binned_licks']
                binned_data = [binned_data[t] for t in trial_ids]

            elif features == 'trackerdata+licks' :
                binned_tracker = session.binned_data['binned_trackerdata']
                binned_licks = session.binned_data['binned_licks']
                binned_data = [np.vstack([binned_tracker[t], binned_licks[t]]) for t in trial_ids]

            bc_aligned = session.binned_data['bin_centers_aligned']
            be_aligned = session.binned_data['bin_edges_aligned']

            binned_data_dict[session_id][features] = {t : s for t, s in zip(trial_ids, binned_data)}

    # --- DECODE -------------------------------------------------------------------
    df = pd.DataFrame(columns=['experiment', 'treatment', 'area_spikes',
                               'group_size', 'time_bin',
                               'time', #'t0', 't1',
                               'bootstrap', 'score'])

    #for area in ['all']+AREAS+['trackerdata']:
    for features in decoding_features:
        for treatment in TREATMENTS:

            if np.isin(features, AREAS+['all_areas']) :
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf['treatment'] == treatment) &
                              (sdf[features] >= min_units)]
                tot_n_units = sdf_sel[features].sum()
                print(experiment, treatment, features, tot_n_units)
                if tot_n_units > min_total_units:
                    proceed=True
                else:
                    proceed=False

            elif np.isin(features, ['trackerdata', 'trackerdata+licks']):
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf['treatment'] == treatment)]
                proceed = True

            if proceed:

                for boot in range(n_bootstraps) :

                    # RESAMPLE TRIALS
                    resampled_trials = {}
                    for session_id in sdf_sel.index:

                        tf_sel_sess = tf_sel[session_id]
                        tids0 = tf_sel_sess[tf_sel_sess['target'] == 0].index
                        tids1 = tf_sel_sess[tf_sel_sess['target'] == 1].index

                        t0 = np.random.choice(tids0,
                                              size=min_trials_per_class,
                                              replace=False)
                        t1 = np.random.choice(tids1,
                                              size=min_trials_per_class,
                                              replace=False)
                        resampled_trials[session_id] = np.hstack([t0, t1])


                    # RESAMPLE UNITS
                    if features == 'trackerdata':
                        u_indx = np.arange(sdf_sel.shape[0]*6)
                    elif features == 'trackerdata+licks':
                        u_indx = np.arange(sdf_sel.shape[0]*7)
                    elif np.isin(features, AREAS+['all_areas']):
                        if features == 'all_areas' :
                            subpop_size_use = subpop_size_all
                        else:
                            subpop_size_use = subpop_size
                        print('subsampling {} units'.format(subpop_size_use))
                        u_indx = np.random.choice(np.arange(tot_n_units), size=subpop_size_use,
                                                  replace=False)
                    else:
                        raise ValueError

                    # MAKE TARGET
                    y = np.hstack([np.zeros(min_trials_per_class),
                                   np.ones(min_trials_per_class)]).astype(int)

                    kfold = StratifiedKFold(n_splits=n_splits, shuffle=True)

                    print('bootstrap {}/{}'.format(boot, n_bootstraps))

                    for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :
                        #time0, time1 = time_bin_edges[time_bin]

                        X = []
                        for session_id in sdf_sel.index:
                            bd = binned_data_dict[session_id][features]
                            s = np.vstack([bd[tid][:, time_bin] for tid in resampled_trials[session_id]])
                            X.append(s)
                        X = np.hstack(X)
                        X = X[:, u_indx]

                        if np.isnan(X).sum():
                            raise ValueError('There are nans in the data')

                        kfold_scores = []
                        y_test_all, y_pred_all = [], []

                        for fold, (training_ind, testing_ind) in enumerate(
                                kfold.split(X, y)) :

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]

                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                            if decoder_name == 'random_forest':
                                decoder = RandomForestClassifier(n_estimators=n_estimators)
                            elif decoder_name == 'SGD' :
                                decoder = SGDClassifier()
                            else :
                                raise NotImplementedError

                            decoder.fit(X_train, y_train)
                            y_pred = decoder.predict(X_test)
                            scoring_function = accuracy_score
                            score = scoring_function(y_test, y_pred)
                            kfold_scores.append(score)
                            y_test_all.append(y_test)
                            y_pred_all.append(y_pred)

                        y_test_all = np.hstack(y_test_all)
                        y_pred_all = np.hstack(y_pred_all)

                        score = np.mean(kfold_scores)

                        # print(
                        #     'pop. size {}/{} bootstrap {}/{} - time bin {}/{} (t={}) - score={:.2f}'.format(
                        #         pi + 1, len(subpopulation_sizes),
                        #         boot, n_bootstraps,
                        #         time_bin, n_time_bins_per_trial, time_bin, score))

                        row = [experiment, treatment, features,
                               subpop_size_use, time_bin, time, #time0, time1,
                               boot, score]

                        df.loc[df.shape[0], :] = row

    numeric_cols = ['score']

    for col in numeric_cols :
        df[col] = pd.to_numeric(df[col])

    df['time'] = [t.rescale(pq.s).item() for t in df['time']]

    pars = {'settings_name' : settings_name,
            'binned_data_settings' : binned_data_settings,
            'experiment' : experiment,
            #'seed' : seed,
            'subpop_size' : subpop_size,
            'subpop_size_all' : subpop_size_all,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'n_repeats' : n_repeats}
            #'score_name' : score_name,
            #'shuffle_kfold' : shuffle_kfold,
            # not strictly user defined
            #'selected_trials' : dp,
            #'n_time_bins_per_trial' : n_time_bins_per_trial,
            #'warp' : warp,
            #'warp_per_modality' : warp_per_modality,
            #'larger_bins' : larger_bins,
            #'median_reaction_times' : median_reaction_times}
            #'time_bin_edges' : time_bin_edges,
            #'time_bin_centers' : time_bin_centers}

    for k in binned_data_pars.keys() :
        pars[k] = binned_data_pars[k]

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))



