import os
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
from constants import *
from plotting_style import *
from utils import *
import quantities as pq

settings_name = 'may11isi'

combos = ['first_poke_location',
          'first_poke_loc_previous_trial',
          'separation',
          'correct']
combos = ['separation']

plot_format = 'png'

subtract_tracker_data_vals = [False]

plt.rc('legend',fontsize=6) # using a size in points

SESSION_IDS = get_preprocessed_sessions()

# --- set up plots folder ------------------------------------------------------



for combo in combos:

    plots_folder = os.path.join(DATA_PATH, 'plots', 'decode', '{}'.format(settings_name), combo)
    if not os.path.isdir(plots_folder) :
        os.makedirs(plots_folder)

    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, combo)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    res = pickle.load(open(output_full_path, 'rb'))

    align_event = res['pars']['align_event']
    max_reaction_time = res['pars']['max_reaction_time']

    bin_edges = res['pars']['spike_bin_edges']
    bin_centers = res['pars']['spike_bin_centers']
    t_start = bin_centers[0].rescale(pq.s).item()
    t_stop = bin_centers[-1].rescale(pq.s).item()


    # --- LOAD TRIAL INFO ----------------------------------------------------------

    tf, uf = load_tf_uf_global(align_event=align_event,
                           max_reaction_time=max_reaction_time,
                               session_ids=SESSION_IDS)


    # --- PLOT -------------------------------------------------------------------

    for subtract_tracker_data in subtract_tracker_data_vals:

        res = pickle.load(open(output_full_path, 'rb'))
        df = res['decoding_scores']

        experiments = df['experiment'].unique()
        treatments = df['treatment'].unique()
        session_ids = df['session_id'].unique()

        if subtract_tracker_data:
            decoding_features = ['all_areas']+[a for a in df['decoding_features'].unique() if a in AREAS]
        else:
            decoding_features = df['decoding_features'].unique()

        # time_bin is redundant, t0 and t1 are quantities so they create problems
        df = df.drop(['t0', 't1', 'time_bin'], axis='columns')

        if subtract_tracker_data:
            groupbys = [c for c in df.columns if c not in ['repeat', 't0', 't1',
                                                           'score', 'time_bin']]
            dg = []
            for experiment in experiments :
                for features in ['all_areas']+AREAS:
                    for session_id in session_ids :

                        dx = df[(df['experiment'] == experiment) &
                                (df['decoding_features'] == features) &
                                (df['session_id'] == session_id)]

                        if dx.shape[0] > 0 :
                            dx = dx.groupby(groupbys).mean().reset_index()

                            dx2 = df[(df['experiment'] == experiment) &
                                     (df['decoding_features'] == 'trackerdata+licks') &
                                     (df['session_id'] == session_id)]
                            dx2 = dx2.groupby(groupbys).mean().reset_index()

                            dx['score'] = dx['score'] - dx2['score']
                            dg.append(dx)

            df = pd.concat(dg)

        # --- plot individual sessions for each area and experiment ----------------
        for experiment in experiments:
            for features in decoding_features:

                dx = df[(df['experiment'] == experiment) &
                        (df['decoding_features'] == features)]

                sids = dx['session_id'].unique()
                levels = np.linspace(0.5 ,1.5, len(sids))
                palette = {s : lighten_color(decoding_features_palette[features], levels[i]) for i, s in enumerate(sids)}

                f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
                sns.lineplot(data=dx, x='time', y='score', hue='session_id',
                             palette=palette, ci=None, style='treatment')
                ax.set_xlabel('Time [s]'.format(align_event))
                ax.set_ylabel('Decoding score - {}\nTarget: {}'.format(features, experiment))

                if subtract_tracker_data:
                    ax.axhline(0, c='grey', ls=':')
                else:
                    ax.axhline(0.5, c='grey', ls=':')

                if not subtract_tracker_data_vals:
                    ax.set_ylim([0.4, 1])

                if align_event == 'stimulus_on':
                    ax.axvline(-1, c='grey', ls='--')

                plot_events(tf, ax)

                ax.set_xlim([t_start, t_stop])
                #ax.set_title(features)
                sns.despine()
                plt.tight_layout()

                if subtract_tracker_data:
                    plot_name = 'subtrakt_decode_single_sessions_{}_{}_{}.{}'.format(experiment, features,
                                                                                     settings_name, plot_format)
                else:
                    plot_name = 'decode_single_sessions_{}_{}_{}.{}'.format(
                        experiment, features,
                        settings_name, plot_format)

                f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

                plt.close()


        # --- plot average per area ------------------------------------------------
        for experiment in experiments:
            for treatment in treatments:
                dx = df[(df['experiment'] == experiment)
                        & (df['treatment'] == treatment)]

                # average across repeats
                groupbys = [c for c in df.columns if c not in ['repeat', 'score']]
                dx = dx.groupby(groupbys).mean().reset_index()

                f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
                sns.lineplot(data=dx, x='time', y='score', hue='decoding_features',
                             palette=decoding_features_palette, ci=95)
                ax.set_xlabel('Time [s]'.format(align_event))
                ax.set_ylabel('Decoding score\nTarget: {} ({})'.format(experiment, treatment))

                if subtract_tracker_data:
                    ax.axhline(0, c='grey', ls=':')
                else:
                    ax.axhline(0.5, c='grey', ls=':')

                if not subtract_tracker_data_vals:
                    ax.set_ylim([0.4, 1])

                if align_event == 'stimulus_on' :
                    ax.axvline(-1, c='grey', ls='--')

                plot_events(tf, ax)

                ax.set_xlim([t_start, t_stop])
                sns.despine()
                plt.tight_layout()

                if subtract_tracker_data:
                    plot_name = 'subtrakt_decode_sessions_averaged_{}_{}_{}.{}'.format(experiment,
                                                treatment, settings_name, plot_format)
                else:
                    plot_name = 'decode_sessions_averaged_{}_{}_{}.{}'.format(experiment,
                                                treatment, settings_name, plot_format)

                f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

                plt.close()




