import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *
from sklearn.linear_model import LogisticRegression

"""
Single session decoding using predictability (Wool 2023)

The shift method can test significance on a single session, but
unless all sessions are significant we need a way to aggregate. 
We compute a global statistic (mean of accuracy over all 
sessions - should not be different from the sum) and test that 
against the surrogate distribution. 

"""

# TODO C is not optimized

settings_name = 'oct4_choice_st_shift'
binned_data_settings = 'sep28_choice_st_coarse'

# settings_name = 'sep28_stim_on_shuffle'
# binned_data_settings = 'sep28_stim_on_coarse'

experiments = ['correct']

min_trials_per_class = 15
min_trials_per_segment = 40
min_units = 15

# TIME PARAMETERS

# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 500

method = 'wool_deconf'
surrogate_method = 'shift'
n_shifts = 20
n_shuffles = 100
lr_penalty = 'l1'
lr_solver = 'liblinear'


neural_features_list = ['all_areas'] +AREAS

confound_features = 'trackerdata+licks' #['trackerdata', 'licks', 'trackerdata+licks']

SESSION_IDS = get_preprocessed_sessions()

# --- OUTPUT FILES -------------------------------------------------------------

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)

for experiment in experiments:


    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_predictabilit', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder) :
        os.makedirs(output_folder)


    df = pd.DataFrame(columns=['experiment', 'animal_id', 'session_id', 'treatment',
                               'neural_features', 'confound_features',
                               'time', 'time_bin', 't0', 't1',
                               'score', 'q1', 'q2', 'significant'])

    df_null = pd.DataFrame(columns=['experiment', 'animal_id', 'session_id', 'treatment',
                                    'neural_features', 'confound_features',
                                    'time', 'time_bin', 't0', 't1',
                                    'surrogate_indx','score'])

    sdf = select_data_experiment(experiment=experiment,
                                 session_ids=SESSION_IDS,
                                 align_event=binned_data_pars['align_event'],
                                 max_reaction_time=binned_data_pars['max_reaction_time'],
                                 filter_trials=True,
                                 bad_units=bad_units)

    print(sdf)

    for neural_features in neural_features_list:

        sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf[neural_features] >= min_units)]


        selected_session_ids = sdf_sel.index

        for session_id in selected_session_ids:

            session = Session(session_id=session_id)
            session.load_data()
            session.load_binned_data(binned_data_settings=binned_data_settings,
                                     bad_units=bad_units)
            session.align_event_times(align_event=binned_data_pars['align_event'],
                                      convert_to_seconds=True)
            session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

            # --- SELECT TRIALS ---
            tf_sel, y_original, n0, n1 = select_trials_experiment(experiment=experiment,
                                                         tf=session.tf)


            # to make sure data trial numbers match between the selection
            # and the actual data
            assert n0 == sdf.loc[session_id, 'n0']
            assert n1 == sdf.loc[session_id, 'n1']

            trial_ids = tf_sel.index
            print('\n\n\n{} {} {}'.format(neural_features, experiment, session_id))
            print(pd.value_counts(y_original))

            n_trials = tf_sel.shape[0]
            if n_trials < ( 2 * n_shifts + min_trials_per_segment) and surrogate_method == 'shift' :
                print('Skipping session {}, not enough trials'.format(session_id))
                continue
            binned_spikes = session.binned_data['binned_spikes']

            if neural_features == 'all_areas':
                unit_indx = session.binned_data['unit_indx']
                binned_spikes = [binned_spikes[t][unit_indx, :] for t in trial_ids]
            else:
                unit_indx = session.binned_data['unit_indx_per_area'][neural_features]
                binned_spikes = [binned_spikes[t][unit_indx, :] for t in trial_ids]


            if confound_features == 'trackerdata':
                binned_confound = session.binned_data['binned_trackerdata']
                binned_confound = [binned_confound[t]for t in trial_ids]

            elif confound_features == 'licks':
                binned_confound = session.binned_data['binned_licks']
                binned_confound = [binned_confound[t]for t in trial_ids]

            elif confound_features == 'trackerdata+licks':
                binned_tracker = session.binned_data['binned_trackerdata']
                binned_licks = session.binned_data['binned_licks']
                binned_confound = [np.vstack([binned_tracker[t], binned_licks[t]]) for t in trial_ids]


            bc_aligned = session.binned_data['bin_centers_aligned']
            be_aligned = session.binned_data['bin_edges_aligned']


            for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :

                X_full = np.vstack([s[:, time_bin] for s in binned_spikes])
                C_full = np.vstack([s[:, time_bin] for s in binned_confound])
                y_full = y_original

                T = X_full.shape[0]
                center_point = T // 2

                index_list, X_list, C_list, y_list = [], [], [], []

                if surrogate_method == 'shift':
                    for s in range(-n_shifts, n_shifts + 1):
                        #print(s)
                        X_shift = X_full[n_shifts + s :T - n_shifts + s, :]
                        C_shift = C_full[n_shifts :T - n_shifts, :]
                        y_shift = y_full[n_shifts :T - n_shifts]

                        X_list.append(X_shift)
                        C_list.append(C_shift)
                        y_list.append(y_shift)
                        index_list.append(s)

                elif surrogate_method == 'shuffle':
                    for s in range(n_shuffles + 1) :
                        # print(s)
                        if s == 0 :
                            X_shuffle = X_full
                        else :
                            X_shuffle = np.random.permutation(X_full)

                        X_list.append(X_shuffle)
                        C_list.append(C_full)
                        y_list.append(y_full)
                        index_list.append(s)

                for s, X, C, y in zip(index_list, X_list, C_list, y_list) :

                    # --- predictability with behavioral predictors ---
                    kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                            random_state=92)

                    # kfold = KFold(n_splits=n_splits, shuffle=True,
                    #                         random_state=92)


                    iterable = kfold.split(X, y)

                    kfold_scores = []
                    y_test_all, y_pred_full_all, y_pred_naive_all, c_all = [], [], [], []

                    for fold, (training_ind, testing_ind) in enumerate(iterable) :

                        #decoder = SGDClassifier(random_state=92)
                        #decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                        X_train = X[training_ind, :]
                        X_test = X[testing_ind, :]
                        y_train = y[training_ind]
                        y_test = y[testing_ind]
                        C_train = C[training_ind, :]
                        C_test = C[testing_ind, :]

                        ss = StandardScaler()
                        X_train = ss.fit_transform(X_train)
                        X_test = ss.transform(X_test)

                        ss = StandardScaler()
                        C_train = ss.fit_transform(C_train)
                        C_test = ss.transform(C_test)

                        if method == 'wool_deconf':
                            full_model = LogisticRegression(penalty=lr_penalty,
                                                            solver=lr_solver)
                            features_full_train = np.hstack([X_train, C_train])
                            features_full_test = np.hstack([X_test, C_test])
                            full_model.fit(features_full_train, y_train)
                            y_pred_full = full_model.predict_proba(features_full_test)

                            naive_model = LogisticRegression(penalty=lr_penalty,
                                                            solver=lr_solver)
                            features_naive_train = C_train
                            features_naive_test = C_test
                            naive_model.fit(features_naive_train, y_train)
                            y_pred_naive = naive_model.predict_proba(features_naive_test)

                        elif method == 'wool_nodeconf':
                            full_model = LogisticRegression(penalty=lr_penalty,
                                                            solver=lr_solver)
                            features_full_train = X_train
                            features_full_test = X_test
                            full_model.fit(features_full_train, y_train)
                            y_pred_full = full_model.predict_proba(features_full_test)

                            naive_model = LogisticRegression(penalty=lr_penalty,
                                                             solver=lr_solver)
                            features_naive_train = np.ones_like(X_train)
                            features_naive_test = np.ones_like(X_test)
                            naive_model.fit(features_naive_train, y_train)
                            y_pred_naive = naive_model.predict_proba(features_naive_test)

                        #score = balanced_accuracy_score(y_test, y_pred)
                        #kfold_scores.append(score)
                        y_test_all.append(y_test)
                        y_pred_full_all.append(y_pred_full)
                        y_pred_naive_all.append(y_pred_naive)
                        c_all.append(C_test[:, 0])

                    target = np.hstack(y_test_all)
                    predictions_full = np.vstack(y_pred_full_all)
                    predictions_naive = np.vstack(y_pred_naive_all)
                    confound = np.hstack(c_all)

                    predictions_full_binary = (predictions_full[:, 1] > 0.5).astype(int)
                    accuracy_full = accuracy_score(predictions_full_binary, target)

                    predictions_naive_binary = (predictions_naive[:, 1] > 0.5).astype(int)
                    accuracy_naive = accuracy_score(predictions_naive_binary, target)

                    # y_true = target
                    # y_pred = predictions_naive[:, 1]
                    # terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
                    # ll = np.sum(terms) / len(y_true)

                    #log_likelihood_full = -log_loss(target, predictions_full)
                    #log_likelihood_naive = -log_loss(target, predictions_naive)
                    log_likelihood_full = log2_likelihood(target, predictions_full)
                    log_likelihood_naive = log2_likelihood(target, predictions_naive)
                    #print(log_likelihood_naive)

                    llratio = log_likelihood_full - log_likelihood_naive

                    predictability = llratio
                    mean_score = predictability
                    # if llratio > 2:
                    #     raise ValueError
                    #print(mean_score)


                    if s == 0:
                        full_p = np.nan
                        full_sig = False
                        partial_p = np.nan
                        partial_sig = False

                        row = [experiment, session.animal_id, session_id,
                               session.treatment,
                               neural_features, confound_features,
                               time, time_bin, t0, t1,
                               mean_score, np.nan, np.nan, False]

                        df.loc[df.shape[0], :] = row

                    else:
                        row = [experiment, session.animal_id, session_id, session.treatment,
                               neural_features, confound_features,
                               time, time_bin, t0, t1,
                               s, mean_score]


                        df_null.loc[df_null.shape[0], :] = row



    df['time'] = [t.rescale(pq.s).item() for t in df['time']]
    df_null['time'] = [t.rescale(pq.s).item() for t in df_null['time']]


    dx = pd.DataFrame(columns=['experiment', 'treatment',
                               'neural_features', 'confound_features',
                                 'time', 'time_bin', 't0', 't1',
                                 'score', 'score_sem', 'q1', 'q2', 'significant'])

    # average (or sum?) across all sessions
    grouping_features = ['experiment', 'treatment', 'neural_features',
                         'confound_features', 'time']
    exclude_features = ['animal_id', 'session_id']

    ds = df.drop(exclude_features, axis=1).groupby(grouping_features).mean().reset_index()

    # and similarly for the surrogates, summing/averaging per shift
    grouping_features = ['experiment', 'treatment', 'neural_features',
                         'confound_features', 'time', 'surrogate_indx']
    exclude_features = ['animal_id', 'session_id']

    ds_null = df_null.drop(exclude_features, axis=1).groupby(
        grouping_features).mean().reset_index()


    for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :

        for treatment in TREATMENTS:

            for neural_features in neural_features_list:

                ds_sel = ds.loc[(ds['time_bin'] == time_bin) &
                                (ds['neural_features'] == neural_features) &
                                (ds['treatment'] == treatment)]

                if ds_sel.shape[0] > 0:

                    ds_null_sel = ds_null.loc[(ds_null['time_bin'] == time_bin) &
                                            (ds_null['neural_features'] == neural_features) &
                                            (ds_null['treatment'] == treatment)]

                    surrogate_scores = ds_null_sel['score']
                    q1, q2 = surrogate_scores.quantile(q=[0.025, 0.975])
                    obs_score = ds_sel['score'].mean()
                    obs_score_sem = ds_sel['score'].sem()
                    n_higher = (surrogate_scores > obs_score).sum()

                    if surrogate_method == 'shift' :
                        significant = n_higher == 0
                    elif surrogate_method == 'shuffle' :
                        p_val = n_higher / len(surrogate_scores)
                        significant = p_val <= (0.05/2)

                    exceeds_ci = obs_score > q2
                    #assert exceeds_ci == significant

                    row = [experiment, treatment,
                          neural_features, confound_features,
                          time, time_bin, t0, t1,
                          obs_score, obs_score_sem, q1, q2, significant]
                    dx.loc[dx.shape[0], :] = row

    dx['time'] = [t.rescale(pq.s).item() for t in dx['time']]

    numeric_cols = ['score', 'score_sem', 'q1', 'q2']

    for col in numeric_cols :
        dx[col] = pd.to_numeric(dx[col])

    pars = {'settings_name' : settings_name,
            'binned_data_settings' : binned_data_settings,
            'experiments' : experiments,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'spike_bin_centers' : bc_aligned,
            'spike_bin_edges' : be_aligned,
            'n_repeats' : n_repeats}
    # 'score_name' : score_name,
    # 'shuffle_kfold' : shuffle_kfold,
    # not strictly user defined

    for k in binned_data_pars.keys() :
        pars[k] = binned_data_pars[k]

    out = {'pars' : pars,
           'decoding_scores' : ds,
           'decoding_null' : ds_null,
           'decoding_summary' : dx}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))


