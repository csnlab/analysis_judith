import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *


"""
Bins spiking data over time and saves results. 

As binning is quite slow especially on slow bins, we run the binning 
separately and only load prebinned data. 
"""


settings_name = 'may7_1000'

# TIME PARAMETERS
align_event = 'stimulus_on'#'stimulus_on'
time_before_in_s = 10
time_after_in_s = 20
binsize_in_ms = 1000
slide_by_in_ms = 500
sliding_window = True

unit_type = 'single_units+mua'  #single_untis+mua, mua, single_units

# settings_name = 'sep28_stim_on_coarse'
# align_event = 'stimulus_on'#'stimulus_on'
# time_before_in_s = 3.2
# time_after_in_s = 2.2
# binsize_in_ms = 400
# slide_by_in_ms = 100
# sliding_window = True

if align_event == 'stimulus_on':
    max_reaction_time = 15
elif align_event == 'first_valid_poke_time':
    max_reaction_time = 5
elif align_event == 'PokeC_off':
    max_reaction_time = 5
else:
    raise ValueError

# --- OUTPUT FILES ---------------------------------------------------------
output_folder = os.path.join(DATA_PATH, 'binned_data', DATA_VERSION,
                             settings_name)
if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)

pars = {'align_event' : align_event,
       'time_before_in_s' : time_before_in_s,
       'time_after_in_s' : time_after_in_s,
       'binsize_in_ms' : binsize_in_ms,
       'slide_by_in_ms' : slide_by_in_ms,
       'sliding_window' : sliding_window,
       'max_reaction_time' : max_reaction_time}

output_file_name = 'binned_{}_pars.pkl'.format(settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
print('Saving parameters to {}'.format(output_full_path))
pickle.dump(pars, open(output_full_path, 'wb'))

SESSION_IDS = get_preprocessed_sessions()

for session_id in SESSION_IDS:
    print('\n\n\n{}'.format(session_id))

    session = Session(session_id=session_id)
    session.load_data(bad_units=None)

    session.align_event_times(align_event=align_event,
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)

    trial_ids = session.tf.index
    #unit_ids = session.uf.index

    uf = session.uf

    #print(uf.index.__len__())

    if unit_type == 'mua':
        uf = uf[np.isin(uf['ClusterGroup'], ['MUA'])]

    elif unit_type == 'single_units':
        uf = uf[np.isin(uf['ClusterGroup'], ['SUA'])]

    elif unit_type == 'single_units+mua':
        uf = uf[np.isin(uf['ClusterGroup'], ['SUA', 'MUA'])]

    unit_ids = uf.index
    print('Binning data for {} units - total of {} units'.format(unit_type, len(unit_ids)))
    print(uf['ClusterGroup'].value_counts())

    # --- BIN SPIKES ---
    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
                                       trial_ids=trial_ids,
                                       align_event=align_event,
                                       time_before_in_s=time_before_in_s,
                                       time_after_in_s=time_after_in_s)

    binned_spikes, bin_centers, bin_edges = session.bin_spikes_per_trial(
                                       binsize=binsize_in_ms * pq.ms,
                                       trial_starts=trial_starts,
                                       trial_ends=trial_ends,
                                       sliding_window=sliding_window,
                                       slide_by=slide_by_in_ms * pq.ms,
                                       unit_ids=unit_ids)

    binned_isi, bin_centers, bin_edges = session.bin_ISI_per_trial(
                                       binsize=binsize_in_ms * pq.ms,
                                       trial_starts=trial_starts,
                                       trial_ends=trial_ends,
                                       sliding_window=sliding_window,
                                       slide_by=slide_by_in_ms * pq.ms,
                                       unit_ids=unit_ids)

    binned_trackerdata, bin_centers, bin_edges = session.bin_trackerdata_per_trial(
                                       binsize=binsize_in_ms * pq.ms,
                                       trial_starts=trial_starts,
                                       trial_ends=trial_ends,
                                       sliding_window=sliding_window,
                                       slide_by=slide_by_in_ms * pq.ms)

    binned_licks, bin_centers, bin_edges = session.bin_licks_per_trial(
                                       binsize=binsize_in_ms * pq.ms,
                                       trial_starts=trial_starts,
                                       trial_ends=trial_ends,
                                       sliding_window=sliding_window,
                                       slide_by=slide_by_in_ms * pq.ms,
                                       binarize=True)

    binned_spikes = {t : s for t, s in zip(trial_ids, binned_spikes)}
    binned_isi = {t : s for t, s in zip(trial_ids, binned_isi)}
    binned_trackerdata = {t : s for t, s in zip(trial_ids, binned_trackerdata)}
    binned_licks = {t : s for t, s in zip(trial_ids, binned_licks)}

    unit_ids_per_area, unit_indx_per_area = {}, {}
    for area in AREAS:
        unit_ids_per_area[area] = uf.index[uf['RecArea'] == area]
        unit_indx_per_area[area] = np.where(uf['RecArea'] == area)[0]

    bc_aligned, be_aligned = session.get_aligned_bins(
            spike_bin_centers=bin_centers,
            spike_bin_edges=bin_edges,
            align_event=align_event_times)

    out = {'session_id' : session_id,
           'binned_spikes' : binned_spikes,
           'binned_isi' : binned_isi,
           'binned_trackerdata' : binned_trackerdata,
           'binned_licks' : binned_licks,
           'bin_centers_aligned' : bc_aligned,
           'bin_edges_aligned' : be_aligned,
           'unit_ids' : unit_ids,
           'unit_indx' : np.arange(len(unit_ids)),
           'unit_ids_per_area' : unit_ids_per_area,
           'unit_indx_per_area' : unit_indx_per_area,
           'align_event' : align_event,
           'time_before_in_s' : time_before_in_s,
           'time_after_in_s' : time_after_in_s,
           'binsize_in_ms' : binsize_in_ms,
           'slide_by_in_ms' : slide_by_in_ms,
           'sliding_window' : sliding_window,
           'max_reaction_time' : max_reaction_time}

    output_file_name = 'binned_{}_{}.pkl'.format(session_id, settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))






