import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session

"""
Decoding on a single session level using neural data stacked with 
behavioral variables. Compare score of intact data with surrogates where
neural data is shuffled across trial. 

Other idea not implemented yet: as shuffling destroys autocorrelations in the 
data, instead of shuffling substitute with neural data from other sessions. 
"""


from utils import *

settings_name = 'may1'
binned_data_settings = 'apr26_reward'

experiments = ['separation']

min_trials_per_class = 30
min_units = 10

# TIME PARAMETERS

# DECODING PARAMETERS
n_repeats = 10
n_splits = 3
decoder_name='SGD'
n_estimators = 500
n_repeats_perm = 100
standardize = True
decoding_features = AREAS
retrain_decoder = True

# --- OUTPUT FILES -------------------------------------------------------------

SESSION_IDS = get_preprocessed_sessions()

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


for experiment in experiments:

    output_file_name = 'decode_perm_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_perm', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder) :
        os.makedirs(output_folder)

    df = pd.DataFrame(
        columns=['animal_id', 'session_id', 'treatment', 'n_neurons',
                 'decoding_features', 'experiment',
                 'decoder', 'time', 'time_bin', 't0', 't1', 'repeat',
                 'ref_score', 'corr_score', 'importance'])

    sdf = select_data_experiment(experiment=experiment,
                                 session_ids=SESSION_IDS,
                                 align_event=binned_data_pars['align_event'],
                                 max_reaction_time=binned_data_pars['max_reaction_time'],
                                 filter_trials=True,
                                 bad_units=bad_units)


    for features in decoding_features :
        sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                      (sdf['n1'] >= min_trials_per_class) &
                      (sdf[features] >= min_units)]

        print(sdf_sel)
        selected_session_ids = sdf_sel.index

        for session_id in selected_session_ids:

            session = Session(session_id=session_id)
            session.load_data()
            session.load_binned_data(binned_data_settings=binned_data_settings,
                                     bad_units=bad_units)
            session.align_event_times(align_event=binned_data_pars['align_event'],
                                      convert_to_seconds=True)
            session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

            # --- SELECT TRIALS ---
            tf_sel, y, n0, n1 = select_trials_experiment(experiment=experiment,
                                                         tf=session.tf)

            # to make sure data trial numbers match between the selection
            # and the actual data
            assert n0 == sdf.loc[session_id, 'n0']
            assert n1 == sdf.loc[session_id, 'n1']

            trial_ids = tf_sel.index
            print('\n\n\n{} {} {}'.format(features, experiment, session_id))
            print(pd.value_counts(y))

            if np.isin(features, AREAS):
                unit_indx = session.binned_data['unit_indx_per_area'][features]
            elif features == 'all_areas':
                unit_indx = session.binned_data['unit_indx']

            neural_data = session.binned_data['binned_spikes']
            neural_data = [neural_data[t][unit_indx, :] for t in trial_ids]

            binned_tracker = session.binned_data['binned_trackerdata']
            binned_licks = session.binned_data['binned_licks']
            confound_data = [np.vstack([binned_tracker[t], binned_licks[t]]) for t  in trial_ids]

            bc_aligned = session.binned_data['bin_centers_aligned']
            be_aligned = session.binned_data['bin_edges_aligned']

            for repeat in range(n_repeats):
                for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :

                    X_neural = np.vstack([s[:, time_bin] for s in neural_data])
                    X_confound = np.vstack([s[:, time_bin] for s in confound_data])

                    kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                            random_state=repeat)
                    kfold_reference_scores = np.zeros(n_splits)
                    kfold_corrupted_scores = np.zeros(n_splits)

                    for fold, (training_ind, testing_ind) in enumerate(kfold.split(X_neural, y)) :

                        if decoder_name == 'random_forest' :
                            decoder = RandomForestClassifier(
                                n_estimators=n_estimators)
                        elif decoder_name == 'SGD' :
                            decoder = SGDClassifier()
                        else :
                            raise NotImplementedError

                        X_neural_train = X_neural[training_ind, :]
                        X_neural_test = X_neural[testing_ind, :]

                        X_confound_train = X_confound[training_ind, :]
                        X_confound_test = X_confound[testing_ind, :]

                        y_train = y[training_ind]
                        y_test = y[testing_ind]

                        if standardize :
                            ss = StandardScaler()
                            X_neural_train = ss.fit_transform(X_neural_train)
                            X_neural_test = ss.transform(X_neural_test)

                            ss = StandardScaler()
                            X_confound_train = ss.fit_transform(X_confound_train)
                            X_confound_test = ss.transform(X_confound_test)

                        X_train = np.hstack([X_neural_train, X_confound_train])
                        X_test = np.hstack([X_neural_test, X_confound_test])

                        # --- FIT DECODER ---
                        decoder.fit(X_train, y_train)

                        # --- COMPUTE REFERENCE SCORE ---
                        y_pred = decoder.predict(X_test)
                        scoring_function = balanced_accuracy_score
                        reference_score = scoring_function(y_test, y_pred)
                        kfold_reference_scores[fold] = reference_score

                        # --- COMPUTE CORRUPTED SCORES ---
                        corrupted_scores = []
                        for perm_repeat in range(n_repeats_perm):
                            X_neural_test_perm = np.random.permutation(X_neural_test)
                            X_test_perm = np.hstack([X_neural_test_perm, X_confound_test])

                            if retrain_decoder:
                                if decoder_name == 'random_forest' :
                                    decoder = RandomForestClassifier(n_estimators=n_estimators)
                                elif decoder_name == 'SGD' :
                                    decoder = SGDClassifier()
                                else :
                                    raise NotImplementedError
                                X_neural_train_perm = np.random.permutation(X_neural_train)
                                X_train_perm = np.hstack([X_neural_train_perm, X_confound_train])
                                decoder.fit(X_train_perm, y_train)

                            y_pred_perm = decoder.predict(X_test_perm)
                            scoring_function = balanced_accuracy_score
                            corrupted_scores.append(scoring_function(y_test, y_pred_perm))
                        corrupted_score = np.mean(corrupted_scores)
                        kfold_corrupted_scores[fold] = corrupted_score

                    mean_reference_score = kfold_reference_scores.mean()
                    mean_corrupted_score = kfold_corrupted_scores.mean()
                    mean_importance = np.mean(kfold_reference_scores - kfold_corrupted_scores)


                    row = [session.animal_id, session_id, session.treatment,
                           X_neural.shape[1],
                           features, experiment,
                           decoder_name, time, time_bin, t0, t1, repeat,
                           mean_reference_score, mean_corrupted_score,
                           mean_importance]

                    df.loc[df.shape[0], :] = row

    numeric_cols = ['ref_score', 'corr_score', 'importance']

    for col in numeric_cols :
        df[col] = pd.to_numeric(df[col])

    df['time'] = [t.rescale(pq.s).item() for t in df['time']]

    pars = {'settings_name' : settings_name,
            'binned_data_settings' : binned_data_settings,
            'experiment' : experiment,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'spike_bin_centers' : bc_aligned,
            'spike_bin_edges' : be_aligned,
            'n_repeats' : n_repeats,
            'n_repeats_perm' : n_repeats_perm,
            'retrain_decoder' : retrain_decoder}
    # 'score_name' : score_name,
    # 'shuffle_kfold' : shuffle_kfold,
    # not strictly user defined

    for k in binned_data_pars.keys() :
        pars[k] = binned_data_pars[k]

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))