import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import RepeatedKFold, KFold
from sklearn.ensemble import RandomForestRegressor
from sklearn.linear_model import SGDRegressor
from utils import *
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import Lasso
from sklearn.multioutput import MultiOutputRegressor

"""
Instantaneous position decoding
"""

# TODO: plot after averaging per session

# TODO: currently not excluding bad units

# the settings_name is used to identify, save, and load the particular
# combination of parameters used for the decoding
settings_name = 'nov6_v0_test'

binned_data_settings = 'may7_500'#'nov30_1000'

# --- type of decoding
experiment = 'position'

# --- decoding parameters
n_repeats = 3#10 # repeats of cross-validation
n_splits = 3 # cross-validation splits
min_units = 20 # minimum number of cells
n_subsample_units = 15 # size of subsamples to randomly draw
decoder = 'random_forest'
n_estimators = 50 # number of random forest trees
sgd_penalty = 'l1' # penalty if using stochastic gradient descent decoder

# if True, for every repeat of the cross-validation routine, we randomly
# draw a subset of neurons equal to min_units
subsample_units = True

# if True, for every repeat of the cross-validation routine, we randomly
# draw a subset of trials equal to minimum number of trials across all sessions
# advice: use all available trials, and subsample only as a control
subsample_trials = False

# if True, units and trials are subsampled with replacement, if False without.
with_replacement = True

# if True, we use speed (together with neural data - to predict position)
include_speed = False

# if None, we decode on all sessions which meet the criteria
selected_sessions = None #['R4_S08']#None#['R4_S08', 'R5_S08', 'R5_S09']

# decode using either neurons from all areas combined, as well as from individual areas
neural_features_list = ['all_areas']+AREAS

SESSION_IDS = get_preprocessed_sessions()

# --- OUTPUT FILES -------------------------------------------------------------

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


if selected_sessions is None:
    sdf = select_data_experiment(experiment=experiment,
                                 session_ids=SESSION_IDS,
                                 align_event=binned_data_pars['align_event'],
                                 max_reaction_time=binned_data_pars['max_reaction_time'],
                                 filter_trials=True,
                                 bad_units=bad_units)

    min_trials = sdf['n0'].min()


position = {n : {} for n in neural_features_list}

for neural_features in neural_features_list:

    if selected_sessions is None:
        sdf_sel = sdf[(sdf[neural_features] >= min_units)]
        selected_session_ids = sdf_sel.index
        print(sdf_sel.shape[0])
    else:
        selected_session_ids = selected_sessions

    # iterate over sessions
    for session_id in selected_session_ids:

        # TODO first session has no binned data
        if session_id == 'R4_S01':
            continue

        output_file_name = 'decode_setting_{}_{}_{}_{}.pkl'.format(settings_name,  experiment,
                                                                   neural_features, session_id)
        output_folder = os.path.join(DATA_PATH, 'results', 'decode_position', settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)

        if not os.path.isdir(output_folder) :
            os.makedirs(output_folder)

        df = pd.DataFrame(columns=['experiment', 'animal_id', 'session_id', 'treatment',
                     'neural_features',  # 'confound_features',
                     'trial_id', 'mean_error', 'total_error'])

        # load sessions
        session = Session(session_id=session_id)
        session.load_data()
        session.load_binned_data(binned_data_settings=binned_data_settings,
                                 bad_units=None)
        session.align_event_times(align_event=binned_data_pars['align_event'],
                                  convert_to_seconds=True)
        session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

        # --- SELECT TRIALS ---
        tf_sel, y_original, n0, n1 = select_trials_experiment(experiment=experiment,
                                                     tf=session.tf)

        # to make sure data trial numbers match between the selection
        # and the actual data
        # assert n0 == sdf.loc[session_id, 'n0']
        # assert n1 == sdf.loc[session_id, 'n1']
        bc_aligned = session.binned_data['bin_centers_aligned']
        be_aligned = session.binned_data['bin_edges_aligned']

        print('\n\n Decoding position - {} - {}'.format(session_id, neural_features))

        n_trials = tf_sel.shape[0]

        binned_spikes = session.binned_data['binned_spikes']
        binned_trackerdata = session.binned_data['binned_trackerdata']

        if neural_features == 'all_areas':
            unit_indx = session.binned_data['unit_indx']
        else:
            unit_indx = session.binned_data['unit_indx_per_area'][neural_features]

        y_test_all, y_pred_all = [], []

        trial_ids = tf_sel.index
        true_position = {}
        for trial_id in trial_ids:
            xpos_test = binned_trackerdata[trial_id][0, :]
            ypos_test = binned_trackerdata[trial_id][1, :]

            y_test = np.hstack([xpos_test.reshape(-1, 1), ypos_test.reshape(-1, 1)])
            true_position[trial_id] = y_test

        predicted_position = {t : [] for t in trial_ids}

        # we repeat the cross-validation procedure n_repeats times, so
        # for each trial we have n_repeats predictions
        for repeat in range(n_repeats):

            trial_ids = tf_sel.index

            if subsample_trials:
                trial_ids_subsamp = np.random.default_rng(seed=repeat).choice(trial_ids,
                                                                              min_trials,
                                                                              replace=with_replacement)
            else:
                trial_ids_subsamp = trial_ids.copy()

            if subsample_units:
                unit_indx_subsamp = np.random.default_rng(seed=repeat).choice(unit_indx,
                                                                              n_subsample_units,
                                                                              replace=with_replacement)
            else:
                unit_indx_subsamp = unit_indx.copy()

            # split the trials into train and test
            kfold = KFold(n_splits=n_splits, shuffle=True, random_state=repeat)
            iterable = kfold.split(trial_ids_subsamp)

            for fold, (training_ind, testing_ind) in enumerate(iterable):

                print('____ repeat {:02d} of {:02d} - '
                      'fold {} of {}'.format(repeat, n_repeats, fold, n_splits))

                trials_ids_train = trial_ids_subsamp[training_ind]
                trials_ids_test = trial_ids_subsamp[testing_ind]

                # assemble the training data
                X_train = np.hstack([binned_spikes[t][unit_indx_subsamp, :] for t in trials_ids_train]).T
                xpos_train = np.hstack([binned_trackerdata[t][0, :] for t in trials_ids_train])
                ypos_train = np.hstack([binned_trackerdata[t][1, :] for t in trials_ids_train])
                y_train = np.hstack([xpos_train.reshape(-1, 1), ypos_train.reshape(-1, 1)])

                if include_speed:
                    velo_train = np.hstack([binned_trackerdata[t][3, :] for t in trials_ids_train])
                    X_train = np.hstack([X_train, velo_train.reshape(-1, 1)])

                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)

                if decoder == 'random_forest':
                    dec = RandomForestRegressor(n_estimators=n_estimators,
                                                random_state=repeat)
                elif decoder == 'sgd_regressor':
                    dec = SGDRegressor(penalty=sgd_penalty)
                else:
                    raise ValueError

                # the multioutput regressor essentially fits two regressors,
                # one for the X and one for the Y coordinates of position
                regressor = MultiOutputRegressor(dec)
                regressor.fit(X_train, y_train)

                # iterate over the trials in the test set
                for test_id in trials_ids_test:
                    # assemble and scale the test data
                    X_test = binned_spikes[test_id][unit_indx_subsamp, :].T
                    if include_speed:
                        velo_test = binned_trackerdata[test_id][3, :]
                        X_test = np.hstack([X_test, velo_test.reshape(-1, 1)])
                    X_test = ss.transform(X_test)

                    # combine test position
                    xpos_test = binned_trackerdata[test_id][0, :]
                    ypos_test = binned_trackerdata[test_id][1, :]
                    y_test = np.hstack([xpos_test.reshape(-1, 1), ypos_test.reshape(-1, 1)])

                    # get the predicted position
                    y_pred = regressor.predict(X_test)

                    # as the error metric: distance between predicted and true location,
                    # either averaged across the trial or summed
                    instantaneous_error = [np.linalg.norm(d) for d in y_test - y_pred]
                    mean_trial_error = np.mean(instantaneous_error)
                    total_trial_error = np.sum(instantaneous_error)

                    row = [experiment, session.animal_id, session_id,
                           session.treatment, neural_features,
                           test_id, mean_trial_error, total_trial_error]

                    df.loc[df.shape[0], :] = row

                    predicted_position[test_id].append(y_pred)

        position[neural_features][session_id] = {'predicted' : None, 'true' : None}
        position[neural_features][session_id]['predicted'] = predicted_position
        position[neural_features][session_id]['true'] = true_position

        pars = {'settings_name' : settings_name,
                'binned_data_settings' : binned_data_settings,
                'experiment' : experiment,
                'n_repeats' : n_repeats,
                'n_splits' : n_splits,
                'min_units' : min_units,
                'decoder' : decoder,
                'n_estimators' : n_estimators,
                'sgd_penalty' : sgd_penalty,
                'subsample_units' : subsample_units,
                'subsample_trials' : subsample_trials,
                'with_replacement' : with_replacement,
                'include_speed' : include_speed,
                'neural_features_list' : neural_features_list}

        out = {'pars' : pars,
               'binned_data_pars' : binned_data_pars,
               'decoding_scores' : df,
               'position' : position}

        print('Saving output to {}'.format(output_full_path))
        pickle.dump(out, open(output_full_path, 'wb'))