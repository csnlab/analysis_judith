import os
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
from constants import *
from plotting_style import *
from utils import *
import quantities as pq
from sklearn.metrics import roc_auc_score
from scipy.stats import pearsonr

settings_name = 'mar27'
decoding_settings_name = 'mar27'

experiment = 'separation'
binsize_in_s = 2
slide_by_in_s = 1
#target_names = ['y_test', 'correct', 'block_performance']
target_names = ['correct', 'successful_block']
n_bootstraps = 200

plot_format = 'png'

subtract_tracker_data_vals = [False]


plt.rc('legend',fontsize=6) # using a size in points

# --- set up plots folder ------------------------------------------------------

plots_folder = os.path.join(DATA_PATH, 'plots', 'decode_tbt', '{}'.format(settings_name), experiment)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


output_file_name = 'decode_tbt_setting_{}_{}.pkl'.format(decoding_settings_name, experiment)
output_folder = os.path.join(DATA_PATH, 'results', 'decode_tbt', decoding_settings_name)
output_full_path = os.path.join(output_folder, output_file_name)
res = pickle.load(open(output_full_path, 'rb'))


df = res['decoding_scores']
dt = res['decoding_tbt']
df = df.drop(['t0', 't1', 'time_bin'], axis='columns')
dt = dt.drop(['t0', 't1', 'time_bin', 'n_neurons'], axis='columns')

treatments = df['treatment'].unique()
session_ids = df['session_id'].unique()
decoding_features = [d for d in df['decoding_features'].unique() if d != 'licks']

align_event = res['pars']['align_event']
max_reaction_time = res['pars']['max_reaction_time']

bin_edges = res['pars']['spike_bin_edges']
bin_centers = res['pars']['spike_bin_centers']
t_start = bin_centers[0].rescale(pq.s).item()
t_stop = bin_centers[-1].rescale(pq.s).item()

left_edges = np.arange(t_start, t_stop, slide_by_in_s)
bins = [(e, e + binsize_in_s) for e in left_edges[:-1]]
bin_centers = [(b1 + b2) / 2 for b1, b2 in bins]

# --- GET ALL TRIAL INFO ---
tf, uf = load_tf_uf_global(align_event=align_event,
                           max_reaction_time=max_reaction_time)

# ------ SIMPLER PLOTS ----


rs_all = []

t1 = -3
t2 = 0

dx = dt[(dt['time'] >= t1) & (dt['time'] <= t2)]
n_repeats = dx['repeat'].unique().shape[0]
n_time_points = dx['time'].unique().shape[0]

# group over repeats
groupbys = [c for c in dx.columns if c not in ['repeat', 'y_test', 'y_pred']]
dx = dx.groupby(groupbys).sum().reset_index()
assert dx['repeat'].unique().shape[0] == 1

# at every time point, compute the fraction of repeats in which prediction is 1
dx['y_test'] = dx['y_test'] / n_repeats
assert dx['y_test'].unique().shape[0] == 2
dx['y_pred'] = dx['y_pred'] / n_repeats

# TODO plot over time

# group across time
groupbys = [c for c in dx.columns if c not in ['time', 'y_test', 'y_pred']]
dx = dx.groupby(groupbys).sum().reset_index().drop('time', axis='columns')

dx['y_test'] = dx['y_test'] / n_time_points
assert dx['y_test'].unique().shape[0] == 2
dx['y_pred'] = dx['y_pred'] / n_time_points
# compute the fraction of time points in which prediction is 1

# now from the likelihood we want a measure of how wrong the
# context representation is
dx['decoding_error'] = np.abs(dx['y_pred'] - dx['y_test'])

# separate dataframes per area and add other trial info
das = {}
cols = ['correct', 'block_performance', 'successful_block', 'separation',
        'first_poke_time_aligned', 'first_valid_poke_time_aligned']

for feat in dx['decoding_features'].unique():
    da = dx[dx['decoding_features'] == feat]

    # make sure that for every decoding feature we have only unique trial ids
    # at this point
    assert len(da['trial_id']) == da['trial_id'].unique().shape[0]

    da.index = da['trial_id']
    da = da.drop(['trial_id'], axis='columns')

    da = pd.merge(da, tf[cols], how='left', left_index=True, right_index=True)
    das[feat] = da

f, ax = plt.subplots(1, 1)
for feat in ['DG']:
    ax.scatter(das[feat]['decoding_error'], das[feat]['block_performance'])


for treatment in treatments:
    f, ax = plt.subplots(1, 1)
    for feat in decoding_features:
        d1 = das[feat]
        d1 = d1[d1['first_poke_time_aligned'] < 5]
        d1 = d1[d1['treatment'] == treatment]
        if len(d1) > 0:
            ax.scatter(d1['decoding_error'],d1['first_poke_time_aligned'],
                       c=decoding_features_palette[feat], alpha=0.3)
            ax.set_xlabel('decoding error')
            ax.set_ylabel('reaction time [s]')
            ax.set_title(treatment)

            print(pearsonr(d1['decoding_error'], d1['first_poke_time_aligned']))



for treatment in treatments:
    f, ax = plt.subplots(1, 1)
    for feat in ['DG']:
        d0 = das[feat][das[feat]['correct'] == 0]
        d1 = das[feat][das[feat]['correct'] == 1]
        d0 = d0[d0['treatment'] == treatment]
        d1 = d1[d1['treatment'] == treatment]
        ax.scatter(d0['decoding_error'],d0['successful_block'], c='r', alpha=0.3)
        ax.scatter(d1['decoding_error'],d1['successful_block'], c='g', alpha=0.3)
        ax.set_xlabel('decoding error')
        ax.set_ylabel('block_performance')
        ax.set_title(treatment)


# ---------------- ROC BOOTSTRAP -----

rs_all = []

for bin_center, (t1, t2) in zip(bin_centers, bins):

    dx = dt[(dt['time'] >= t1) & (dt['time'] <= t2)]
    n_repeats = dx['repeat'].unique().shape[0]
    n_time_points = dx['time'].unique().shape[0]

    # group over repeats
    groupbys = [c for c in dx.columns if c not in ['repeat', 'y_test', 'y_pred']]
    dx = dx.groupby(groupbys).sum().reset_index()
    assert dx['repeat'].unique().shape[0] == 1

    # at every time point, compute the fraction of repeats in which prediction is 1
    dx['y_test'] = dx['y_test'] / n_repeats
    assert dx['y_test'].unique().shape[0] == 2
    dx['y_pred'] = dx['y_pred'] / n_repeats

    # TODO plot over time

    # group across time
    groupbys = [c for c in dx.columns if c not in ['time', 'y_test', 'y_pred']]
    dx = dx.groupby(groupbys).sum().reset_index().drop('time', axis='columns')

    dx['y_test'] = dx['y_test'] / n_time_points
    assert dx['y_test'].unique().shape[0] == 2
    dx['y_pred'] = dx['y_pred'] / n_time_points
    # compute the fraction of time points in which prediction is 1

    # now from the likelihood we want a measure of how wrong the
    # context representation is
    dx['decoding_error'] = np.abs(dx['y_pred'] - dx['y_test'])

    # separate dataframes per area and add other trial info
    das = {}
    cols = ['correct', 'block_performance', 'successful_block', 'separation',
            'first_poke_time_aligned', 'first_valid_poke_time_aligned']

    for feat in dx['decoding_features'].unique():
        da = dx[dx['decoding_features'] == feat]

        # make sure that for every decoding feature we have only unique trial ids
        # at this point
        assert len(da['trial_id']) == da['trial_id'].unique().shape[0]

        da.index = da['trial_id']
        da = da.drop(['trial_id'], axis='columns')

        da = pd.merge(da, tf[cols], how='left', left_index=True, right_index=True)
        das[feat] = da


    # this computes the roc / correlation PER SESSION, you could also do it
    # across all sessions of the same treatment


    # rs = pd.DataFrame(columns=['session_id', 'treatment',
    #                            'decoding_features', 'target',
    #                            'score_type', 'n_boot', 'score'])
    # for features in decoding_features :
    #     for treatment in TREATMENTS :
    #         da = das[features]
    #         da = da[da['treatment'] == treatment]
    #         for session_id in da['session_id'].unique() :
    #             for target_name in target_names :
    #
    #                 variable = da['decoding_error'].__array__()
    #                 target = da[target_name].__array__().astype(int)
    #
    #                 for boot in range(n_bootstraps):
    #
    #                     resample_indx = np.random.choice(np.arange(target.shape[0]),
    #                                                      size=target.shape[0],
    #                                                      replace=True)
    #                     variable_r = variable[resample_indx]
    #                     target_r = target[resample_indx]
    #
    #                     if np.unique(target).shape[0]>2:
    #                         r, p = pearsonr(target_r, variable_r)
    #                         score_type = 'pearson_r'
    #                         score = r
    #                     else :
    #                         roc_score = roc_auc_score(y_true=target_r,
    #                                                   y_score=variable_r)
    #                         score_type = 'roc'
    #                         score = roc_score
    #
    #                     rs.loc[rs.shape[0], :] = [session_id, treatment,
    #                                               features, target_name,
    #                                               score_type, boot,
    #                                               score]


    rs = pd.DataFrame(columns=['treatment',
                               'decoding_features', 'target',
                               'score_type', 'n_boot', 'score'])
    for features in decoding_features :
        for treatment in TREATMENTS :
            da = das[features]
            da = da[da['treatment'] == treatment]

            if da.shape[0] > 0:
                for target_name in target_names :

                    variable = da['decoding_error'].__array__()
                    target = da[target_name].__array__().astype(int)

                    for boot in range(n_bootstraps):

                        resample_indx = np.random.choice(np.arange(target.shape[0]),
                                                         size=target.shape[0],
                                                         replace=True)
                        variable_r = variable[resample_indx]
                        target_r = target[resample_indx]

                        if np.unique(target).shape[0]>2:
                            r, p = pearsonr(target_r, variable_r)
                            score_type = 'pearson_r'
                            score = r
                        else :
                            roc_score = roc_auc_score(y_true=target_r,
                                                      y_score=variable_r)
                            score_type = 'roc'
                            score = roc_score

                        rs.loc[rs.shape[0], :] = [treatment,
                                                  features, target_name,
                                                  score_type, boot,
                                                  score]


    groupbys = [c for c in rs.columns if c not in ['score', 'n_boot']]
    rs = rs.groupby(groupbys).mean().reset_index().drop('n_boot', axis='columns')
    rs['time'] = bin_center

    rs_all.append(rs)

rs = pd.concat(rs_all)



# --- PLOT ---------------------------------------------------------------------
# TODO need to generate confidence intervals from bootstraps
for target_name in target_names:
    for treatment in treatments:
        dx = rs[(rs['treatment'] == treatment) &
                (rs['target'] == target_name)]

        # average across repeats
        groupbys = [c for c in rs.columns if c not in ['repeat', 'score']]
        dx = dx.groupby(groupbys).mean().reset_index()

        f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
        sns.lineplot(data=dx, x='time', y='score', hue='decoding_features',
                     palette=decoding_features_palette, ci=95)
        ax.set_xlabel('Time [s]'.format(align_event))
        ax.set_ylabel('Likelihood: {}\nTarget: {} ({})'.format(experiment, target_name, treatment))

        if dx['score_type'].iloc[0] == 'pearson_r':
            ax.axhline(0, c='grey', ls=':')

        elif dx['score_type'].iloc[0] == 'roc':
            ax.axhline(0.5, c='grey', ls=':')

        # if not subtract_tracker_data_vals:
        #     ax.set_ylim([0.4, 1])

        ax.axvline(0, c='grey', ls='--')
        ax.text(x=0, y=ax.get_ylim()[1]+0.01, s=event_labels[align_event], ha='center',
                size=7)
        if align_event == 'stimulus_on' :
            ax.axvline(-1, c='grey', ls='--')

        plot_events(tf, ax)

        ax.set_xlim([t_start, t_stop])
        sns.despine()
        plt.tight_layout()

        plot_name = 'decode_tbt_sessav_{}_{}_{}_{}.{}'.format(experiment,
                                        treatment, target_name, settings_name, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


# VERSION WITHOUT BOOTSTRAP

#target_names = ['y_test', 'correct', 'block_performance']
# target_names = ['correct']
# rs = pd.DataFrame(columns=['session_id', 'treatment',
#                            'decoding_features', 'target',
#                            'score_type', 'score'])
# for features in decoding_features :
#     for treatment in TREATMENTS :
#         da = das[features]
#         da = da[da['treatment'] == treatment]
#         for session_id in da['session_id'].unique():
#             for target_name in target_names:
#
#                 decoder_likelihood = da['y_pred'].__array__()
#                 target = da[target_name].__array__().astype(int)
#
#
#                 if target_name == 'block_performance' :
#                     r, p = pearsonr(target, decoder_likelihood)
#                     score_type = 'pearson_r'
#                     score = r
#                 else :
#                     roc_score = roc_auc_score(y_true=target,
#                                               y_score=decoder_likelihood)
#                     score_type = 'roc'
#                     score = roc_score
#
#                 rs.loc[rs.shape[0], :] = [session_id, treatment,
#                                           features, target_name,
#                                           score_type,
#                                           score]

# --- STANDARD DECODING PLOTS --------------------------------------------------


if False:
    # --- plot individual sessions for each area and experiment ----------------

    for features in decoding_features:

        dx = df[(df['experiment'] == experiment) &
                (df['decoding_features'] == features)]

        sids = dx['session_id'].unique()
        levels = np.linspace(0.5 ,1.5, len(sids))
        palette = {s : lighten_color(decoding_features_palette[features], levels[i]) for i, s in enumerate(sids)}

        f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
        sns.lineplot(data=dx, x='time', y='score', hue='session_id',
                     palette=palette, ci=None, style='treatment')
        ax.set_xlabel('Time [s]'.format(align_event))
        ax.set_ylabel('Decoding score - {}\nTarget: {}'.format(features, experiment))

        ax.axhline(0.5, c='grey', ls=':')

        ax.set_ylim([0.4, 1])

        ax.axvline(0, c='grey', ls='--')
        ax.text(x=0, y=ax.get_ylim()[1]+0.01, s=event_labels[align_event], ha='center',
                size=7)
        if align_event == 'stimulus_on':
            ax.axvline(-1, c='grey', ls='--')

        for event in events:
            q1, q2, q3 = tf[event].quantile([0.05, 0.5, 0.95]).__array__()
            ax.axvspan(q1, q3, alpha=0.15, color='grey', linewidth=0,
                       zorder=-10)
            ax.axvline(q2, alpha=0.3, c='grey', zorder=-10)
            ax.text(x=q2, y=ax.get_ylim()[1]+0.01, s=event_labels[event], ha='center',
                    size=7)

        ax.set_xlim([t_start, t_stop])
        #ax.set_title(features)
        sns.despine()
        plt.tight_layout()

        plot_name = 'decode_single_sessions_{}_{}_{}.{}'.format(
                experiment, features,
                settings_name, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


    # --- plot average per area ------------------------------------------------
    for treatment in treatments:
        dx = df[(df['experiment'] == experiment)
                & (df['treatment'] == treatment)]

        # average across repeats
        groupbys = [c for c in df.columns if c not in ['repeat', 'score']]
        dx = dx.groupby(groupbys).mean().reset_index()

        f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
        sns.lineplot(data=dx, x='time', y='score', hue='decoding_features',
                     palette=decoding_features_palette, ci=95)
        ax.set_xlabel('Time [s]'.format(align_event))
        ax.set_ylabel('Decoding score\nTarget: {} ({})'.format(experiment, treatment))

        ax.axhline(0.5, c='grey', ls=':')

        if not subtract_tracker_data_vals:
            ax.set_ylim([0.4, 1])

        ax.axvline(0, c='grey', ls='--')
        ax.text(x=0, y=ax.get_ylim()[1]+0.01, s=event_labels[align_event], ha='center',
                size=7)
        if align_event == 'stimulus_on' :
            ax.axvline(-1, c='grey', ls='--')

        for event in events :
            q1, q2, q3 = tf[event].quantile(
                [0.05, 0.5, 0.95]).__array__()
            ax.axvspan(q1, q3, alpha=0.15, color='grey', linewidth=0,
                       zorder=-10)
            ax.axvline(q2, alpha=0.3, c='grey', zorder=-10)
            ax.text(x=q2, y=ax.get_ylim()[1]+0.01, s=event_labels[event], ha='center',
                    size=7)


        ax.set_xlim([t_start, t_stop])
        sns.despine()
        plt.tight_layout()

        plot_name = 'decode_sessions_averaged_{}_{}_{}.{}'.format(experiment,
                                        treatment, settings_name, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()




