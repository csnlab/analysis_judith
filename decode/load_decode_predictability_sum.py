import os
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
from constants import *
from plotting_style import *
from utils import *
import quantities as pq

settings_name = 'oct4_choice_st_shift'

experiments = ['correct']

plot_format = 'png'

plt.rc('legend',fontsize=6) # using a size in points

SESSION_IDS = get_preprocessed_sessions()

# --- set up plots folder ------------------------------------------------------


for experiment in experiments:

    plots_folder = os.path.join(DATA_PATH, 'plots', 'decode_predictabilit', '{}'.format(settings_name), experiment)
    if not os.path.isdir(plots_folder) :
        os.makedirs(plots_folder)

    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_predictabilit', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    res = pickle.load(open(output_full_path, 'rb'))

    align_event = res['pars']['align_event']
    max_reaction_time = res['pars']['max_reaction_time']

    bin_edges = res['pars']['spike_bin_edges']
    bin_centers = res['pars']['spike_bin_centers']
    t_start = bin_centers[0].rescale(pq.s).item()
    t_stop = bin_centers[-1].rescale(pq.s).item()

    tf, uf = load_tf_uf_global(align_event=align_event,
                           max_reaction_time=max_reaction_time,
                               session_ids=SESSION_IDS)

    df = res['decoding_summary']
    df_null = res['decoding_null']

    for features in df['neural_features'].unique():

        for treatment in TREATMENTS:

            dx = df[(df['neural_features'] == features) &
                    (df['treatment'] == treatment)]
            print(dx)

            if dx.shape[0] >0:
                f, ax = plt.subplots(1, 1, figsize=[5, 5], sharex=True, sharey=True)

                ax.plot(dx['time'], dx['score'], c=decoding_features_palette[features],
                        label='decoding from {}'.format(features))

                ax.errorbar(dx['time'], dx['score'], yerr=dx['score_sem'], fmt="o",
                            c=decoding_features_palette[features])

                ax.set_xlabel('Time [s]')
                ax.set_ylabel('Predictability of {} ({})'.format(experiment, treatment))
                ax.axhline(0.0, ls=':', c='grey')

                # ax.fill_between(dx['time'], dx['q1'], dx['q2'], color='grey',
                #                 zorder=-10, alpha=0.5, label='Null distribution',
                #                 linewidth=0)

                for surrogate in df_null['surrogate_indx'].unique():
                    dx_null = df_null[(df_null['neural_features'] == features) &
                                      (df_null['treatment'] == treatment) &
                                      (df_null['surrogate_indx'] == surrogate)]

                    ax.plot(dx_null['time'], dx_null['score'],
                            c=sns.xkcd_rgb['light grey'], zorder=-10)


                dxx = dx[dx['significant']]
                ax.scatter(dxx['time'], np.repeat(0.29, dxx.shape[0]), marker='X',
                           c=decoding_features_palette[features], zorder=10, alpha=1, s=40,
                           label='Significant p<0.05')
                ax.legend()
                ax.set_ylim([-0.5, 0.6])
                ax.set_xlim([t_start, t_stop])

                plot_events(tf, ax)

                ax.set_xlim([t_start, t_stop])
                sns.despine()
                plt.tight_layout()

                plot_name = 'decode_average_predictability_{}_{}_{}_{}.{}'.format(
                    experiment,
                    treatment, features, settings_name, plot_format)

                f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

                plt.close()

            # if save_plots :
        #     plot_name = 'decoding_{}_{}_{}_{}.{}'.format(method, surrogate_method,
        #                                                  experiment, settings_name,
        #                                                  plot_format)
        #     f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
        #               bbox_inches="tight")
        #

