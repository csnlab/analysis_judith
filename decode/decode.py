import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *

"""
Basic single session decoding.
"""


settings_name = 'apr18'
binned_data_settings = 'apr18'

combos = ['first_poke_location',
          'first_poke_loc_previous_trial',
          'separation',
          'correct']

min_trials_per_class = 15
min_units = 10

# TIME PARAMETERS

# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 500

decoding_features = ['all_areas'] + AREAS + ['trackerdata', 'licks', 'trackerdata+licks']

SESSION_IDS = get_preprocessed_sessions()


# --- OUTPUT FILES -------------------------------------------------------------

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


for combo in combos:

    if combo == 'first_poke_location':
        experiments = ['first_poke_location_small_separation',
                       'first_poke_location_large_separation']

    elif combo == 'first_poke_loc_previous_trial':
        experiments = ['first_poke_loc_previous_trial_large_sep',
                       'first_poke_loc_previous_trial_small_sep']

    elif combo == 'response_location':
        experiments = ['response_location_large_separation',
                       'response_location_small_separation']

    elif combo == 'previous_response_location' :
        experiments = ['previous_response_location_large_separation',
                       'previous_response_location_small_separation']

    elif combo == 'separation':
        experiments = ['separation',
                       'separation_only_correct',
                       'separation_only_incorrect']
        experiments = ['separation']

    elif combo == 'block_performance_binarized':
        experiments = ['block_performance_binarized']

    elif combo == 'successful_block_binarized':
        experiments = ['successful_block_binarized']

    elif combo == 'correct':
        experiments = ['correct']

    else:
        raise ValueError

    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, combo)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder) :
        os.makedirs(output_folder)

    df = pd.DataFrame(columns=['animal_id', 'session_id', 'treatment',
                               'decoding_features', 'combo', 'experiment',
                               'decoder', 'time', 'time_bin', 't0', 't1',
                               'repeat', 'score', 'n_neurons'])

    for experiment in experiments:

        sdf = select_data_experiment(experiment=experiment,
                                     session_ids=SESSION_IDS,
                                     align_event=binned_data_pars['align_event'],
                                     max_reaction_time=binned_data_pars['max_reaction_time'],
                                     filter_trials=True,
                                     bad_units=bad_units)

        print(sdf)

        for features in decoding_features:

            if np.isin(features, AREAS+['all_areas']) :
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf[features] >= min_units)]

            elif np.isin(features, ['trackerdata', 'licks', 'trackerdata+licks']):
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class)]

            selected_session_ids = sdf_sel.index

            for session_id in selected_session_ids:

                session = Session(session_id=session_id)
                session.load_data()
                session.load_binned_data(binned_data_settings=binned_data_settings,
                                         bad_units=bad_units)
                session.align_event_times(align_event=binned_data_pars['align_event'],
                                          convert_to_seconds=True)
                session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

                # --- SELECT TRIALS ---
                tf_sel, y, n0, n1 = select_trials_experiment(experiment=experiment,
                                                             tf=session.tf)


                # to make sure data trial numbers match between the selection
                # and the actual data
                assert n0 == sdf.loc[session_id, 'n0']
                assert n1 == sdf.loc[session_id, 'n1']

                trial_ids = tf_sel.index
                print('\n\n\n{} {} {}'.format(features, experiment, session_id))
                print(pd.value_counts(y))

                if np.isin(features, AREAS) :
                    unit_indx = session.binned_data['unit_indx_per_area'][features]
                    binned_data = session.binned_data['binned_spikes']
                    binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

                elif features == 'all_areas' :
                    binned_data = session.binned_data['binned_spikes']
                    unit_indx = session.binned_data['unit_indx']
                    binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

                elif features == 'trackerdata':
                    binned_data = session.binned_data['binned_trackerdata']
                    binned_data = [binned_data[t]for t in trial_ids]

                elif features == 'licks':
                    binned_data = session.binned_data['binned_licks']
                    binned_data = [binned_data[t]for t in trial_ids]

                elif features == 'trackerdata+licks':
                    binned_tracker = session.binned_data['binned_trackerdata']
                    binned_licks = session.binned_data['binned_licks']
                    binned_data = [np.vstack([binned_tracker[t], binned_licks[t]]) for t in trial_ids]


                bc_aligned = session.binned_data['bin_centers_aligned']
                be_aligned = session.binned_data['bin_edges_aligned']


                for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :

                    X = np.vstack([s[:, time_bin] for s in binned_data])

                    #print('\n\nTime bin {} of {}'.format(time_bin + 1, len(bc_aligned)))

                    for repeat in range(n_repeats) :

                        #print('- Repeat {}/{}'.format(repeat, n_repeats))


                        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                                random_state=repeat)
                        kfold_scores = []
                        y_test_all, y_pred_all = [], []

                        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                            if decoder_name == 'random_forest' :
                                decoder = RandomForestClassifier(n_estimators=n_estimators)
                            elif decoder_name == 'SGD' :
                                decoder = SGDClassifier()
                            else :
                                raise NotImplementedError

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]

                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                            decoder.fit(X_train, y_train)
                            y_pred = decoder.predict(X_test)
                            # TODO balanced?
                            scoring_function = balanced_accuracy_score
                            score = scoring_function(y_test, y_pred)
                            kfold_scores.append(score)
                            y_test_all.append(y_test)
                            y_pred_all.append(y_pred)

                        y_test_all = np.hstack(y_test_all)
                        y_pred_all = np.hstack(y_pred_all)

                        mean_score = np.mean(kfold_scores)

                        row = [session.animal_id, session_id, session.treatment,
                               features, combo, experiment,
                               decoder_name, time, time_bin, t0, t1, repeat, mean_score,
                               X.shape[1]]

                        df.loc[df.shape[0], :] = row

                        # rs[decoding_input]['true'][repeat, :] = targ_true
                        # rs[decoding_input]['pred'][repeat, :] = targ_pred

    numeric_cols = ['score']

    for col in numeric_cols :
        df[col] = pd.to_numeric(df[col])

    df['time'] = [t.rescale(pq.s).item() for t in df['time']]

    pars = {'settings_name' : settings_name,
            'binned_data_settings' : binned_data_settings,
            'combo' : combo,
            'experiments' : experiments,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'spike_bin_centers' : bc_aligned,
            'spike_bin_edges' : be_aligned,
            'n_repeats' : n_repeats}
    # 'score_name' : score_name,
    # 'shuffle_kfold' : shuffle_kfold,
    # not strictly user defined

    for k in binned_data_pars.keys() :
        pars[k] = binned_data_pars[k]

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))