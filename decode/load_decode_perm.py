import os
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
from constants import *
from plotting_style import *
from utils import *
import quantities as pq

settings_name = 'may1'

experiments = ['separation']

plot_format = 'png'

plt.rc('legend',fontsize=6) # using a size in points

SESSION_IDS = get_preprocessed_sessions()


# --- set up plots folder ------------------------------------------------------

for experiment in experiments:

    plots_folder = os.path.join(DATA_PATH, 'plots', 'decode_perm', '{}'.format(settings_name), experiment)
    if not os.path.isdir(plots_folder) :
        os.makedirs(plots_folder)

    output_file_name = 'decode_perm_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_perm', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)
    res = pickle.load(open(output_full_path, 'rb'))

    align_event = res['pars']['align_event']
    max_reaction_time = res['pars']['max_reaction_time']

    bin_edges = res['pars']['spike_bin_edges']
    bin_centers = res['pars']['spike_bin_centers']
    t_start = bin_centers[0].rescale(pq.s).item()
    t_stop = bin_centers[-1].rescale(pq.s).item()

    res = pickle.load(open(output_full_path, 'rb'))
    df = res['decoding_scores']

    experiments = df['experiment'].unique()
    treatments = df['treatment'].unique()
    session_ids = df['session_id'].unique()

    decoding_features = df['decoding_features'].unique()
    df = df.drop(['t0', 't1', 'time_bin'], axis='columns')

    # --- LOAD TRIAL INFO ----------------------------------------------------------

    tf, uf = load_tf_uf_global(align_event=align_event,
                           max_reaction_time=max_reaction_time,
                               session_ids=SESSION_IDS)


    # --- plot average per area ------------------------------------------------
    for treatment in treatments:
        dx = df[(df['experiment'] == experiment)
                & (df['treatment'] == treatment)]

        avoid_group = ['repeat', 'importance', 'ref_score', 'corr_score']
        groupbys = [c for c in df.columns if c not in avoid_group]
        dx = dx.groupby(groupbys).mean().reset_index()

        f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
        sns.lineplot(data=dx, x='time', y='importance', hue='decoding_features',
                     palette=decoding_features_palette, ci=95)
        ax.set_xlabel('Time [s]'.format(align_event))
        ax.set_ylabel('Decoding importance\nTarget: {} ({})'.format(experiment, treatment))

        ax.axhline(0, c='grey', ls=':')

        # if not subtract_tracker_data_vals:
        #     ax.set_ylim([0.4, 1])

        if align_event == 'stimulus_on' :
            ax.axvline(-1, c='grey', ls='--')

        plot_events(tf, ax)

        ax.set_xlim([t_start, t_stop])
        sns.despine()
        plt.tight_layout()

        plot_name = 'decode_perm_sessions_averaged_{}_{}_{}.{}'.format(experiment,
                                        treatment, settings_name, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


    for area in AREAS:
        dx = df[(df['experiment'] == experiment)
                & (df['decoding_features'] == area)]

        if dx.shape[0] > 0 :
            f, ax = plt.subplots(1, 1, figsize=[4.5, 4.5])
            sns.lineplot(data=dx, x='time', y='importance', ci='sd',
                         ax=ax, hue='treatment',
                         palette={t : decoding_features_palette[area] for t
                                  in TREATMENTS},
                         style='treatment', dashes=dashes_dict)
            ax.set_ylabel(experiment)
            ax.axhline(0.5, ls=':', c='grey')
            ax.set_ylim([-0.1, 0.3])
            plot_events(tf, ax)
            ax.set_xlim([t_start, t_stop])
            ax.set_xlabel('Time [ms]')
            ax.set_ylabel(
                'Decoding score ({})\nTarget: {}'.format(area, experiment))

            # ax.set_title(area)
            # ax.get_legend().remove()
            sns.despine()
            plt.tight_layout()

            plot_name = 'decode_perm_compare_treatments_{}_{}_{}.{}'.format(
                area, experiment, settings_name, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


    for area in AREAS:

        dx = df[(df['experiment'] == experiment) &
                (df['decoding_features'] == area)]

        sids = dx['session_id'].unique()
        levels = np.linspace(0.5, 1.5, len(sids))
        palette = {
        s : lighten_color(decoding_features_palette[area], levels[i])
        for i, s in enumerate(sids)}

        f, ax = plt.subplots(1, 1, figsize=[7.5, 4.5])
        sns.lineplot(data=dx, x='time', y='importance', hue='session_id',
                     palette=palette, ci=None, style='treatment',
                     dashes=dashes_dict)
        ax.set_xlabel('Time [s]'.format(align_event))
        ax.set_ylabel(
            'Decoding score - {}\nTarget: {}'.format(area, experiment))

        ax.axhline(0, c='grey', ls=':')


        if align_event == 'stimulus_on' :
            ax.axvline(-1, c='grey', ls='--')

        plot_events(tf, ax)

        ax.set_xlim([t_start, t_stop])
        # ax.set_title(features)
        sns.despine()
        plt.tight_layout()


        plot_name = 'decode_perm_single_sessions_{}_{}_{}.{}'.format(
                experiment, area,
                settings_name, plot_format)

        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


    for treatment in treatments:
        for area in AREAS:

            dx = df[(df['experiment'] == experiment) &
                    (df['decoding_features'] == area) &
                    (df['treatment'] == treatment)]

            sids = dx['session_id'].unique()
            levels = np.linspace(0.5, 1.5, len(sids))
            palette = {
            s : lighten_color(decoding_features_palette[area], levels[i])
            for i, s in enumerate(sids)}

            f, ax = plt.subplots(1, 1, figsize=[7.5, 4.5])
            sns.lineplot(data=dx, x='time', y='importance', hue='session_id',
                         palette=palette, ci=None, style='treatment',
                         dashes=dashes_dict)
            ax.set_xlabel('Time [s]'.format(align_event))
            ax.set_ylabel(
                'Decoding score - {}\nTarget: {}'.format(area, experiment))

            ax.axhline(0, c='grey', ls=':')


            if align_event == 'stimulus_on' :
                ax.axvline(-1, c='grey', ls='--')

            plot_events(tf, ax)

            ax.set_xlim([t_start, t_stop])
            # ax.set_title(features)
            sns.despine()
            plt.tight_layout()


            plot_name = 'decode_perm_single_sessions{}_{}_{}_{}.{}'.format(
                    experiment, area, treatment,
                    settings_name, plot_format)

            f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

            plt.close()

