import os
import pickle
import matplotlib.pyplot as plt
import seaborn as sns
from constants import *
from plotting_style import *
import glob
import pandas as pd
from _pickle import UnpicklingError
from scipy.stats import mannwhitneyu


settings_name = 'nov6_v0_test'

experiment = 'position'
plot_format = 'png'

plot_swarmplot_simple = True
plot_swarmplot_bars = True
plot_histogram = True
plot_single_trials = False


# prepare plots folder
plots_folder = os.path.join(DATA_PATH, 'plots', 'decode_position',
                            '{}'.format(settings_name), experiment)

if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)
neural_features_list = AREAS

# load data
output_folder = os.path.join(DATA_PATH, 'results', 'decode_position', settings_name)
output_files = os.listdir(output_folder)

dfs = []
positions = []
for file in output_files:
    try:
        res = pickle.load(open(os.path.join(output_folder, file), 'rb'))
    except UnpicklingError:
        continue
    position = res['position']
    df = res['decoding_scores']
    pars = res['pars']
    dfs.append(df)
    positions.append(position)

df = pd.concat(dfs)


# average the average or total error per trial across sessions
gbcols = ['session_id', 'animal_id','treatment', 'neural_features']
dm = df.drop(['experiment'], axis=1).groupby(gbcols).mean().reset_index()


# statistics: non-parametric test for difference between VEH and CNO
for i, area in enumerate(neural_features_list):
    dfsel = dm[dm['neural_features']==area]
    col = 'mean_error'

    x1 = dfsel[dfsel['treatment'] == 'VEH'][col]
    x2 = dfsel[dfsel['treatment'] == 'CNO'][col]
    stat, p = mannwhitneyu(x1, x2)
    print('{} - p = {:.2f}'.format(area, p))


# swarmplot simple
if plot_swarmplot_simple:
    f, ax = plt.subplots(1, 3, figsize=[5, 3], sharex=True, sharey=True)
    for i, area in enumerate(neural_features_list):
        dfsel = dm[dm['neural_features']==area]
        sns.swarmplot(data=dfsel,
                      y='mean_error',
                      hue='treatment',
                      hue_order=['VEH', 'CNO'],
                      palette=treatment_palette,
                      ax=ax[i],
                      dodge=True)
        ax[i].set_title('{}'.format(area))
        ax[i].get_legend().remove()
        ax[i].set_xticks([-0.2, 0.2])
        ax[i].set_xticklabels(['VEH', 'CNO'])

    ax[0].set_ylabel('Average error per session [cm]')
    sns.despine()
    plt.tight_layout()
    plot_name = 'decoding_pos_treatment_averaged.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    plt.close()




# swarmplot with bars
# TODO: change sem with bootstrap
# https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.bootstrap.html

if plot_swarmplot_bars:
    treatment_loc = {'VEH': -0.2, 'CNO': 0.2}
    f, ax = plt.subplots(1, 3, figsize=[5, 3], sharex=True, sharey=True)
    for i, area in enumerate(neural_features_list):
        dfsel = df[df['neural_features']==area]
        sessions = dfsel['session_id'].unique()

        noise = np.linspace(-0.05, 0.05, len(sessions))
        for n, sess in enumerate(sessions):

            ds = dfsel[dfsel['session_id'] == sess]

            treatment = ds['treatment'].iloc[0]
            vals = ds[ds['treatment'] == treatment][col]

            # center = vals.quantile(0.5)
            # error = vals.quantile([0.25, 0.75]).__array__().reshape(-1, 1)

            center = vals.mean()
            error = vals.sem()
            ax[i].errorbar(x=treatment_loc[treatment]+noise[n],
                           y=center, yerr=error, ls='none', marker='o',
                         c=treatment_palette[treatment])

        ax[i].set_xlim(-0.4, 0.4)
        ax[i].set_title('{}'.format(area))
        ax[i].set_xticks([-0.2, 0.2])
        ax[i].set_xticklabels(['VEH', 'CNO'])

    ax[0].set_ylabel('Average error per session [cm]')
    sns.despine()
    plt.tight_layout()
    plot_name = 'decoding_pos_treatment_averaged_bars.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    plt.close()



# plot histogram of average error per trial, combining all sessions
if plot_histogram:
    f, ax = plt.subplots(1, 3, figsize=[10, 3], sharex=True)
    for i, area in enumerate(neural_features_list):
        dfsel = df[df['neural_features']==area]
        try:
            ncno = dfsel.groupby(['treatment'])['session_id'].unique()['CNO'].shape[0]
        except:
            ncno = 0

        try:
            nveh = dfsel.groupby(['treatment'])['session_id'].unique()['VEH'].shape[0]
        except:
            nveh = 0

        sns.histplot(data=dfsel, stat='density',
                     common_norm=False,
                     x='mean_error', hue='treatment',
                     hue_order=['VEH', 'CNO'],
                     ax=ax[i],
                     palette=treatment_palette)
        ax[i].set_title('{}\n({}, {}) sess. (CNO, VEH)'.format(area, ncno, nveh))
        ax[i].set_xlim(0, 20)
    sns.despine()
    plt.tight_layout()

    plot_name = 'decoding_pos_treatment.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    plt.close()




# plot predictions of a couple of trials per area
if plot_single_trials:
    for neural_features in neural_features_list:

        for treatment in ['VEH', 'CNO']:

            for i in range(8):

                dx = df[(df['neural_features'] == neural_features) &
                        (df['treatment'] == treatment)]
                row = dx.sort_values(by='mean_error').iloc[i]
                session_id = row['session_id']
                trial_id = row['trial_id']
                treatment = row['treatment']
                neural_features = row['neural_features']

                tp = position[neural_features][session_id]['true'][trial_id]
                pp = position[neural_features][session_id]['predicted'][trial_id]

                f, ax = plt.subplots(1, 2, figsize=[6, 3])
                time = pars['spike_bin_centers']
                ax[0].set_ylabel('X position')
                ax[0].set_xlabel('Time [ms]')
                ax[0].plot(time, tp[:, 0], c='k', label='True')

                # for i, p in enumerate(pp):
                #     if i == 0:
                #         ax[0].plot(time, p[:, 0], c=decoding_features_palette[neural_features],
                #                    label='Predicted by {}'.format(neural_features))
                #     else:
                #         ax[0].plot(time, p[:, 0], c=decoding_features_palette[neural_features])

                px = np.vstack([p[:, 0] for p in pp])
                stdx = px.std(axis=0)

                ax[0].plot(time, px.mean(axis=0),
                           c=decoding_features_palette[neural_features],
                           label='Predicted by {}'.format(neural_features))
                ax[0].fill_between(time, px.mean(axis=0)-stdx/2, px.mean(axis=0)+stdx/2,
                           color=decoding_features_palette[neural_features], alpha=0.5,
                            linewidth=0)

                #axvspan
                ax[0].legend(loc='lower right')

                ax[1].set_ylabel('Y position')
                ax[1].set_xlabel('Time [ms]')
                ax[1].plot(time, tp[:, 1], c='k', label='True')
                # for i, p in enumerate(pp):
                #     if i == 0:
                #         ax[1].plot(time, p[:, 1], c=decoding_features_palette[neural_features],
                #                    label='Predicted by {}'.format(neural_features))
                #     else:
                #         ax[1].plot(time, p[:, 1], c=decoding_features_palette[neural_features])

                py = np.vstack([p[:, 1] for p in pp])
                stdy = py.std(axis=0)

                ax[1].plot(time, py.mean(axis=0),
                           c=decoding_features_palette[neural_features],
                           label='Predicted by {}'.format(neural_features))
                ax[1].fill_between(time, py.mean(axis=0)-stdy/2, py.mean(axis=0)+stdy/2,
                           color=decoding_features_palette[neural_features], alpha=0.5,
                                   linewidth=0)

                ax[1].legend(loc='lower right')

                f.suptitle('{}, {} - {}, trial {}'.format(neural_features, treatment, session_id, trial_id))
                sns.despine()
                plt.tight_layout()

                plot_name = 'decoding_pos_single_trial_{}_{}.{}'.format(neural_features, trial_id, plot_format)

                f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

                plt.close()



#df[(df['treatment'] == 'CNO') & (df['neural_features'] == 'CA3')].sort_values(by='mean_error')
# session_id = 'R5_S08'
# trial_id = 'tid_R5_S08_024'
