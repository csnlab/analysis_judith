import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *

sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))

settings_name = 'mar23'

min_trials_per_class = 30
min_units = 1
# TIME PARAMETERS
align_event = 'PokeC_off'
time_before_in_s = 4
time_after_in_s = 4
binsize_in_ms = 500
slide_by_in_ms = 250
sliding_window = True
max_reaction_time = 5
# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 500
min_total_units = 20
subpopulation_sizes = [15]
n_bootstraps = 100
areas = ['DG']

experiments = ['separation']

for experiment in experiments:

    # --- OUTPUT FILES ---------------------------------------------------------
    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode_pseudopop', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)


    sdf, tf_sel = select_data_experiment(experiment=experiment,
                                 session_ids=SESSION_IDS,
                                 align_event=align_event,
                                 max_reaction_time=max_reaction_time,
                                 filter_trials=True,
                                 return_trials=True)

    # --- BIN ALL THE DATA -----------------------------------------------------

    binned_data_dict = {s : {} for s in sdf.index}

    for session_id in sdf.index :

        session = Session(session_id=session_id)
        session.load_data()
        session.align_event_times(align_event=align_event,
                                  convert_to_seconds=True)
        session.filter_trials(max_reaction_time=max_reaction_time)

        trial_ids = session.tf.index

        trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
            trial_ids=trial_ids,
            align_event=align_event,
            time_before_in_s=time_before_in_s,
            time_after_in_s=time_after_in_s)

        for area in AREAS+['trackerdata']:

            if np.isin(area, AREAS) :
                # --- SELECT UNITS ---
                selected_unit_ids = session.uf[session.uf['RecArea'] == area].index

                binned_data, bin_centers, bin_edges = session.bin_spikes_per_trial(
                    binsize=binsize_in_ms * pq.ms,
                    trial_starts=trial_starts,
                    trial_ends=trial_ends,
                    sliding_window=sliding_window,
                    slide_by=slide_by_in_ms * pq.ms,
                    unit_ids=selected_unit_ids)

            # TODO need to adapt the pseudopop composition for trackerdata
            elif area == 'trackerdata' :
                binned_data, bin_centers, bin_edges = session.bin_trackerdata_per_trial(
                    binsize=binsize_in_ms * pq.ms,
                    trial_starts=trial_starts,
                    trial_ends=trial_ends,
                    sliding_window=sliding_window,
                    slide_by=slide_by_in_ms * pq.ms)

            elif area == 'all' :
                selected_unit_ids = session.uf.index
                binned_data, bin_centers, bin_edges = session.bin_spikes_per_trial(
                    binsize=binsize_in_ms * pq.ms,
                    trial_starts=trial_starts,
                    trial_ends=trial_ends,
                    sliding_window=sliding_window,
                    slide_by=slide_by_in_ms * pq.ms,
                    unit_ids=selected_unit_ids)

            binned_data_dict[session_id][area] = {t : s for t, s in
                                                 zip(trial_ids, binned_data)}

            bc_aligned, be_aligned = session.get_aligned_bins(
                spike_bin_centers=bin_centers,
                spike_bin_edges=bin_edges,
                align_event=align_event_times)

    # --- DECODE -------------------------------------------------------------------
    df = pd.DataFrame(columns=['experiment', 'treatment', 'area_spikes',
                               'group_size', 'time_bin',
                               'time', #'t0', 't1',
                               'bootstrap', 'score'])

    #for area in ['all']+AREAS+['trackerdata']:
    for area in AREAS+['trackerdata']:
        for treatment in TREATMENTS:

            if np.isin(area, AREAS) :
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf['treatment'] == treatment) &
                              (sdf[area] >= min_units)]
                tot_n_units = sdf_sel[area].sum()
                if tot_n_units > min_total_units:
                    proceed=True
                else:
                    proceed=False

            elif area == 'trackerdata':
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf['treatment'] == treatment)]
                proceed=True

            if proceed:
                for pi, subpop_size in enumerate(subpopulation_sizes):
                    for boot in range(n_bootstraps) :

                        # RESAMPLE TRIALS
                        resampled_trials = {}
                        for session_id in sdf_sel.index:

                            tf_sel_sess = tf_sel[session_id]
                            tids0 = tf_sel_sess[tf_sel_sess['target'] == 0].index
                            tids1 = tf_sel_sess[tf_sel_sess['target'] == 1].index

                            t0 = np.random.choice(tids0,
                                                  size=min_trials_per_class,
                                                  replace=False)
                            t1 = np.random.choice(tids1,
                                                  size=min_trials_per_class,
                                                  replace=False)
                            resampled_trials[session_id] = np.hstack([t0, t1])


                        # RESAMPLE UNITS
                        if area == 'trackerdata':
                            u_indx = np.arange(sdf_sel.shape[0]*6)
                        else:
                            u_indx = np.random.choice(np.arange(tot_n_units), size=subpop_size,
                                                      replace=False)


                        # MAKE TARGET
                        y = np.hstack([np.zeros(min_trials_per_class),
                                       np.ones(min_trials_per_class)]).astype(int)

                        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True)

                        print(
                            'pop. size {}/{} bootstrap {}/{}'.format(
                                pi + 1, len(subpopulation_sizes),
                                boot, n_bootstraps))

                        for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :
                            #time0, time1 = time_bin_edges[time_bin]

                            X = []
                            for session_id in sdf_sel.index:
                                bd = binned_data_dict[session_id][area]
                                s = np.vstack([bd[tid][:, time_bin] for tid in resampled_trials[session_id]])
                                X.append(s)
                            X = np.hstack(X)
                            X = X[:, u_indx]

                            if np.isnan(X).sum():
                                raise ValueError('There are nans in the data')

                            kfold_scores = []
                            y_test_all, y_pred_all = [], []

                            for fold, (training_ind, testing_ind) in enumerate(
                                    kfold.split(X, y)) :

                                X_train = X[training_ind, :]
                                X_test = X[testing_ind, :]
                                y_train = y[training_ind]
                                y_test = y[testing_ind]

                                ss = StandardScaler()
                                X_train = ss.fit_transform(X_train)
                                X_test = ss.transform(X_test)

                                if decoder_name == 'random_forest':
                                    pass
                                elif decoder_name == 'SGD' :
                                    decoder = SGDClassifier()
                                else :
                                    raise NotImplementedError

                                decoder.fit(X_train, y_train)
                                y_pred = decoder.predict(X_test)
                                scoring_function = accuracy_score
                                score = scoring_function(y_test, y_pred)
                                kfold_scores.append(score)
                                y_test_all.append(y_test)
                                y_pred_all.append(y_pred)

                            y_test_all = np.hstack(y_test_all)
                            y_pred_all = np.hstack(y_pred_all)

                            score = np.mean(kfold_scores)

                            # print(
                            #     'pop. size {}/{} bootstrap {}/{} - time bin {}/{} (t={}) - score={:.2f}'.format(
                            #         pi + 1, len(subpopulation_sizes),
                            #         boot, n_bootstraps,
                            #         time_bin, n_time_bins_per_trial, time_bin, score))

                            row = [experiment, treatment, area,
                                   subpop_size, time_bin, time, #time0, time1,
                                   boot, score]

                            df.loc[df.shape[0], :] = row


    df['time'] = [t.rescale(pq.s).item() for t in df['time']]

    pars = {'settings_name' : settings_name,
            'experiment' : experiment,
            #'seed' : seed,
            'binsize_in_ms' : binsize_in_ms,
            'subpopulation_sizes' : subpopulation_sizes,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'n_repeats' : n_repeats}
            #'score_name' : score_name,
            #'shuffle_kfold' : shuffle_kfold,
            # not strictly user defined
            #'selected_trials' : dp,
            #'n_time_bins_per_trial' : n_time_bins_per_trial,
            #'warp' : warp,
            #'warp_per_modality' : warp_per_modality,
            #'larger_bins' : larger_bins,
            #'median_reaction_times' : median_reaction_times}
            #'time_bin_edges' : time_bin_edges,
            #'time_bin_centers' : time_bin_centers}

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))



