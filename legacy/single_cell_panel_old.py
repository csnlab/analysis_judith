import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn  as sns
import matplotlib
from plotting_style import *

# TODO add left vs right sorted by block performance

sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))

settings_name = 'mar22'
plot_format = 'png'
data_version = DATA_VERSION

# TIME PARAMETERS
align_event = 'PokeC_off'#'stimulus_on'
time_before_in_s = 1.5
time_after_in_s = 5.5
binsize_in_ms = 500
slide_by_in_ms = 100
xticks = [-1, 0, 1, 2, 3, 4, 5]
sliding_window = True

max_reaction_time = 5 # in seconds

smooth_sigma = 1.4
spatial_bin_size = 10
occupancy_threshold = 5
#min_spikes_bin = 3
x_max_global = 576
y_max_global = 720

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'single_cell_panels', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


# --- LOAD SESSION ---

# TODO
# TODO >>>
# TODO >>>>>
# TODO >>>>>>> occupancy is not real time now
# TODO >>>>> add poke times: first poke and actual poke
# TODO >>> need to get those in the trial dataframe first
# TODO

def get_vals(combo) :
    if combo == 'correct':
        vals = [0, 1]
    elif combo == 'separation' :
        vals = ['small', 'large']
    elif combo == 'response_location':
        vals = [2, 4, 5, 7]
    elif combo == 'first_poke_location': # assuming it is filtered
        vals = [2, 4, 5, 7]
    elif combo == 'response_location_correct':
        vals = [2, 4, 5, 7]
    elif combo == 'block_id':
        vals = None
    elif combo == 'successful_block':
        vals = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    else :
        raise ValueError
    return vals

def get_col(combo) :
    if combo == 'correct' :
        col = 'correct'
    elif combo == 'separation' :
        col = 'separation'
    elif combo == 'response_location':
        col = 'response_location'
    elif combo == 'first_poke_location':
        col = 'first_poke_location'
    elif combo == 'response_location_correct':
        col = 'response_location'
    elif combo == 'block_id':
        col = 'block_id'
    elif combo == 'successful_block':
        col = 'successful_block'
    else:
        raise ValueError
    return col

combos = ['correct', 'separation', 'first_poke_location',
          'successful_block']

ratemap_selections = ['all', 'small_separation', 'large_separation']

for session_id in SESSION_IDS:

    # --- LOAD SESSION ---
    session = Session(session_id=session_id, data_version=data_version)
    session.load_data()
    session.align_event_times(align_event=align_event, convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)

    uf = session.uf
    tf = session.tf

    tf_sorted = tf.sort_values(by=['first_poke_location', 'first_poke_time'])

    uf['unit_index'] = np.arange(uf.shape[0])
    trial_ids = tf.index


    # --- BIN SPIKES ---
    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(trial_ids=trial_ids,
                                                                                  align_event=align_event,
                                                                                  time_before_in_s=time_before_in_s,
                                                                                  time_after_in_s=time_after_in_s)

    binned_spikes, spike_bin_centers, spike_bin_edges = session.bin_spikes_per_trial(binsize=binsize_in_ms * pq.ms,
                                    trial_starts=trial_starts,
                                    trial_ends=trial_ends,
                                    sliding_window=sliding_window,
                                    slide_by=slide_by_in_ms * pq.ms)

    binned_spikes = {t : s for t, s in zip(trial_ids, binned_spikes)}

    bc_aligned, be_aligned = session.get_aligned_bins(spike_bin_centers=spike_bin_centers,
                                                      spike_bin_edges=spike_bin_edges,
                                                      align_event=align_event_times)

    bc_aligned = bc_aligned.rescale(pq.s)


    # --- COMPUTE OCCUPANCY ---

    #occupancy[occupancy < occupancy_threshold] = 0
    x_pos = session.trackerdata_filtered['head']['x']
    y_pos = session.trackerdata_filtered['head']['y']

    x_max_crop = x_pos.max()
    y_max_crop = y_pos.max()

    x_min_crop = x_pos[x_pos>0].min()
    y_min_crop = y_pos[y_pos>0].min()

    occupancies = {}

    for selection in ratemap_selections :

        if selection == 'all' :
            trial_ids = tf.index
        elif selection == 'small_separation' :
            trial_ids = tf[tf['separation'] == 'small'].index
        elif selection == 'large_separation' :
            trial_ids = tf[tf['separation'] == 'large'].index

        occupancy = session.compute_spatial_occupancy(spatial_bin_size=spatial_bin_size,
                                                      x_max=x_max_global,
                                                      y_max=y_max_global,
                                                      trial_ids=trial_ids,
                                                      smooth_sigma=smooth_sigma)

        occupancy[occupancy < occupancy_threshold] = 0
        occupancy = np.ma.masked_where(occupancy == 0, occupancy)
        occupancies[selection] = occupancy
    oms = np.hstack([occupancies[s] for s in ratemap_selections])
    oms = oms[np.isfinite(oms)]
    vmax_occupancies = np.nanmax(oms)



    for unit_id, row in uf.iterrows():#uf.iloc[0:1].iterrows():#uf.iterrows():#

        print('Plotting unit {}'.format(unit_id))
        unit_ind = row['unit_index']
        unit_area = row['RecArea']

        # --- GET SPIKING DATA OVER TIME ---
        unit_data = {c : {} for c in combos}
        unit_data_trials = {c : [] for c in combos}

        for combo in combos:
            col = get_col(combo)
            if np.isin(combo, ['block_id']):
                vals = tf['block_id'].unique()
            else:
                vals = get_vals(combo)
            for ir, val in enumerate(vals):
                sdf = tf[tf[col] == val]
                if np.isin(combo, ['block_id']):
                    sdf = sdf[sdf['successful_block']>=1]
                if np.isin(combo, ['response_location_correct']):
                    sdf = sdf[sdf['correct'] == 1]
                try:
                    spikes = np.vstack([binned_spikes[tid][unit_ind, :] for tid in sdf.index])
                except ValueError:
                    spikes = np.zeros(shape=[0, len(bc_aligned)])
                unit_data[combo][val] = spikes
                unit_data_trials[combo].append(sdf.index)
        for combo in combos:
            unit_data_trials[combo] = np.hstack(unit_data_trials[combo])



        unit_data['all'] = np.vstack([binned_spikes[tid][unit_ind, :] for tid in tf.index])
        unit_data['all_sorted'] = np.vstack([binned_spikes[tid][unit_ind, :] for tid in tf_sorted.index])

        vmax = unit_data['all'].max()

        # sorted data

        # --- COMPUTE RATEMAPS ---
        ratemaps = {}
        maxes = []
        for selection in ratemap_selections:
            if selection == 'all':
                trial_ids = tf.index
            elif selection == 'small_separation':
                trial_ids = tf[tf['separation'] == 'small'].index
            elif selection == 'large_separation':
                trial_ids = tf[tf['separation'] == 'large'].index

            spikes_mat = session.compute_spatial_firing(unit_id=unit_id,
                                                        x_max=x_max_global,
                                                        y_max=y_max_global,
                                                        spatial_bin_size=spatial_bin_size,
                                                        trial_ids=trial_ids,
                                                        smooth_sigma=smooth_sigma)

            ratemaps[selection] = spikes_mat / occupancies[selection]
            maxes.append(ratemaps[selection].max())
        vmax_ratemaps = np.max(maxes)


        f = plt.figure(tight_layout=True, figsize=[12, 7])
        gs = gridspec.GridSpec(4,  6)

        # --- ADD AXES -------------------------
        subplot_all = f.add_subplot(gs[:, 2])
        subplot_all_sorted = f.add_subplot(gs[:, 3])


        imshow_axes = {c : {} for c in combos}

        imshow_axes[combos[0]] = f.add_subplot(gs[0:2, 0])
        imshow_axes[combos[1]] = f.add_subplot(gs[0:2, 1])
        imshow_axes[combos[2]] = f.add_subplot(gs[2:4, 0])
        imshow_axes[combos[3]] = f.add_subplot(gs[2:4, 1])

        # placements = [(0, 0), (0, 1), (1, 0), (1, 1)]
        # for p, combo in zip(placements, combos):
        #     imshow_axes[combo] = f.add_subplot(gs[p[0], p[1]])

        occupancy_axes = {}
        occupancy_axes[ratemap_selections[0]] = f.add_subplot(gs[0, 4])
        occupancy_axes[ratemap_selections[1]] = f.add_subplot(gs[1, 4])
        occupancy_axes[ratemap_selections[2]] = f.add_subplot(gs[2, 4])

        ratemap_axes = {}
        ratemap_axes[ratemap_selections[0]] = f.add_subplot(gs[0, 5])
        ratemap_axes[ratemap_selections[1]] = f.add_subplot(gs[1, 5])
        ratemap_axes[ratemap_selections[2]] = f.add_subplot(gs[2, 5])

        # --- PLOT IMSHOW ALL TRIALS ----------


        n_trials_all = unit_data['all'].shape[0]
        subplot_all.imshow(unit_data['all'],
                            vmin=0, vmax=vmax,
                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                    n_trials_all + 0.5, 0.5),
                            aspect='auto',
                            cmap=rasters_cmap)

        subplot_all.set_xticks(xticks)
        subplot_all.set_yticks([1]+[n_trials_all])
        for t in [-1, 0]:
            subplot_all.axvline(t, c=sns.xkcd_rgb['white'], ls='--')
        subplot_all.set_xlabel('Time (s)')
        subplot_all.set_title('All trials')
        subplot_all.set_xticks(xticks)

        # for trial_indx, trial_id in enumerate(tf.index):
        #     poke_location = tf.loc[trial_id, 'first_poke_location']
        #     t_first_poke_aligned = tf.loc[trial_id, 'first_poke_time_aligned']
        #     subplot_all.scatter(t_first_poke_aligned, trial_indx + 1,
        #                                    marker='|',
        #                                    color=location_palette[poke_location])
        xlim = subplot_all.get_xlim()
        for trial_indx, trial_id in enumerate(tf.index):
            for poke_location, poke_label in zip(session.poke_locations, session.poke_on_labels):
                t_first_poke = tf.loc[trial_id, poke_label]
                print(t_first_poke)
                if t_first_poke is not None:
                    subplot_all.scatter(t_first_poke, trial_indx + 1,
                                                   marker='|',
                                                   color=location_palette[poke_location])
        subplot_all.set_xlim(xlim)


        # --- PLOT IMSHOW ALL TRIALS SORTED ----------

        n_trials_all = unit_data['all_sorted'].shape[0]
        subplot_all_sorted.imshow(unit_data['all_sorted'],
                            vmin=0, vmax=vmax,
                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                    n_trials_all + 0.5, 0.5),
                            aspect='auto',
                            cmap = rasters_cmap)

        subplot_all_sorted.set_xticks(xticks)
        subplot_all_sorted.set_yticks([1]+[n_trials_all])
        for t in [-1, 0]:
            subplot_all_sorted.axvline(t, c=sns.xkcd_rgb['white'], ls='--')
        subplot_all_sorted.set_xlabel('Time (s)')
        subplot_all_sorted.set_title('All trials (sorted)')
        subplot_all_sorted.set_xticks(xticks)

        xlim = subplot_all_sorted.get_xlim()
        for trial_indx, trial_id in enumerate(tf_sorted.index):
            for poke_location, poke_label in zip(session.poke_locations, session.poke_on_labels):
                t_first_poke = tf.loc[trial_id, poke_label]
                subplot_all_sorted.scatter(t_first_poke, trial_indx + 1,
                                           marker='|',
                                           color=location_palette[poke_location])
        subplot_all_sorted.set_xlim(xlim)

        # --- PLOT IMSHOW ----------------------------------------------------------

        for ic, combo in enumerate(combos) :

            if np.isin(combo, ['block_id']):
                vals = tf['block_id'].unique()
            else:
                vals = get_vals(combo)

            n_trials = [unit_data[combo][v].shape[0] for v in vals]
            n_trials_tot = np.sum(n_trials)
            ticks = list(np.cumsum(n_trials)[:-1])
            ticks = [t for t in ticks if t!=0]

            data = np.vstack([unit_data[combo][v] for v in vals])

            imshow_axes[combo].imshow(data, vmin=0, vmax=vmax,
                                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                                    n_trials_tot + 0.5, 0.5),
                                            aspect='auto',
                                            cmap=rasters_cmap)

            imshow_axes[combo].set_xticks(xticks)
            imshow_axes[combo].set_yticks([1]+ ticks + [n_trials_tot])

            # for trial_indx, trial_id in enumerate(unit_data_trials[combo]):
            #     poke_location = tf.loc[trial_id, 'first_poke_location']
            #     t_first_poke_aligned = tf.loc[trial_id, 'first_poke_time_aligned']
            #     imshow_axes[combo].scatter(t_first_poke_aligned, trial_indx+1,
            #                                marker='|', color=location_palette[poke_location])
            imshow_axes[combo].get_xlim()
            for trial_indx, trial_id in enumerate(unit_data_trials[combo]):
                for poke_location, poke_label in zip(session.poke_locations, session.poke_on_labels):
                    t_first_poke = tf.loc[trial_id, poke_label]
                    imshow_axes[combo].scatter(t_first_poke, trial_indx+1,
                                                   marker='|', color=location_palette[poke_location])
            imshow_axes[combo].set_xlim(xlim)

            for n in ticks:
                if n != data.shape[0]:
                    imshow_axes[combo].axhline(n, c=sns.xkcd_rgb['white'])

            for t in [-1, 0]:
                imshow_axes[combo].axvline(t, c=sns.xkcd_rgb['white'], ls='--')

        for combo in combos:
            imshow_axes[combo].set_ylabel(combo)

            sns.despine(ax=imshow_axes[combo])

        sns.despine(ax=subplot_all)

        imshow_axes[combos[0]].set_title('{} - {}'.format(unit_id, unit_area))
        imshow_axes[combos[2]].set_xlabel('Time (s)')
        imshow_axes[combos[3]].set_xlabel('Time (s)')

        # --- PLOT OCCUPANCIES ----------------
        cmap = rasters_cmap
        cmap.set_bad(color='white')
        for selection in ratemap_selections:
            occupancy_axes[selection].imshow(occupancies[selection].T,
                                           extent=[0, x_max_crop, 0, y_max_crop],
                                           cmap=cmap,
                                           vmin=0,
                                           vmax=vmax_occupancies)
            occupancy_axes[selection].set_xticks([])
            occupancy_axes[selection].set_yticks([])
            for spine in ['left', 'right', 'top', 'bottom']:
                occupancy_axes[selection].spines[spine].set_visible(False)
            #sns.despine(ax=ratemap_axes[selection], bottom=True, left=True)
            occupancy_axes[selection].set_ylabel(selection)
            for l in list(session.poke_locations)+['C']:
                x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
                y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
                occupancy_axes[selection].scatter(x, y, c=location_palette[l], zorder=10)
            occupancy_axes[selection].set_xlim([x_min_crop, x_max_crop])
            occupancy_axes[selection].set_ylim([y_min_crop, y_max_crop])

        # --- PLOT RATEMAPS ----------------
        for selection in ratemap_selections:
            ratemap_axes[selection].imshow(ratemaps[selection].T,
                                           extent=[0, x_max_crop, 0, y_max_crop],
                                           cmap=ratemaps_cmap,
                                           vmin=0,
                                           vmax=vmax_ratemaps)
            ratemap_axes[selection].set_xticks([])
            ratemap_axes[selection].set_yticks([])
            for spine in ['left', 'right', 'top', 'bottom']:
                ratemap_axes[selection].spines[spine].set_visible(False)
                #ratemap_axes[selection].set_ylabel(selection)

            for l in list(session.poke_locations)+['C']:
                x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
                y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
                ratemap_axes[selection].scatter(x, y, c=location_palette[l], zorder=10)
            ratemap_axes[selection].set_xlim([x_min_crop, x_max_crop])
            ratemap_axes[selection].set_ylim([y_min_crop, y_max_crop])

        occupancy_axes[ratemap_selections[0]].set_title('Occupancy')
        ratemap_axes[ratemap_selections[0]].set_title('Ratemap')

        plt.tight_layout()

        gs.update(wspace=0.35)

        plot_name = 'psth_simple_{}_{}.{}'.format(settings_name, unit_id,
                                                  plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


