import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *


settings_name = 'mar22'

combos = ['first_poke_location',
          'first_poke_loc_previous_trial',
          'separation']

min_trials_per_class = 15
min_units = 10
# TIME PARAMETERS
align_event = 'PokeC_off'
time_before_in_s = 7
time_after_in_s = 5.5
binsize_in_ms = 400
slide_by_in_ms = 100
sliding_window = True
max_reaction_time = 5
# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 500


# --- OUTPUT FILES ---------------------------------------------------------


for combo in combos:

    if combo == 'first_poke_location':
        experiments = ['first_poke_location_small_separation',
                       'first_poke_location_large_separation']

    if combo == 'first_poke_loc_previous_trial':
        experiments = ['first_poke_loc_previous_trial_large_sep',
                       'first_poke_loc_previous_trial_small_sep']

    if combo == 'response_location':
        experiments = ['response_location_large_separation',
                       'response_location_small_separation']

    elif combo == 'previous_response_location' :
        experiments = ['previous_response_location_large_separation',
                       'previous_response_location_small_separation']

    elif combo == 'separation':
        experiments = ['separation',
                       'separation_only_correct',
                       'separation_only_incorrect']

    output_file_name = 'decode_setting_{}_{}.pkl'.format(settings_name, combo)
    output_folder = os.path.join(DATA_PATH, 'results', 'decode', settings_name)
    output_full_path = os.path.join(output_folder, output_file_name)

    if not os.path.isdir(output_folder) :
        os.makedirs(output_folder)

    df = pd.DataFrame(columns=['animal_id', 'session_id', 'treatment',
                               'area_spikes', 'combo', 'experiment',
                               'decoder', 'time', 'time_bin', 't0', 't1',
                               'repeat', 'score', 'n_neurons'])

    for experiment in experiments:

        sdf = select_data_experiment(experiment=experiment,
                                     session_ids=SESSION_IDS,
                                     align_event=align_event,
                                     max_reaction_time=max_reaction_time,
                                     filter_trials=True)

        print(sdf)

        for area in ['all']+AREAS+['trackerdata']:

            if np.isin(area, AREAS) :
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf[area] >= min_units)]
            elif area == 'trackerdata':
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class)]

            elif area == 'all':
                sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
                              (sdf['n1'] >= min_trials_per_class) &
                              (sdf['total'] >= min_units)]

            selected_session_ids = sdf_sel.index

            for session_id in selected_session_ids:

                session = Session(session_id=session_id)
                session.load_data()
                session.align_event_times(align_event=align_event,
                                          convert_to_seconds=True)
                session.filter_trials(max_reaction_time=max_reaction_time)

                # --- SELECT TRIALS ---
                tf_sel, y, n0, n1 = select_trials_experiment(experiment=experiment,
                                                             tf=session.tf)

                # to make sure data trial numbers match between the selection
                # and the actual data
                assert n0 == sdf.loc[session_id, 'n0']
                assert n1 == sdf.loc[session_id, 'n1']

                trial_ids = tf_sel.index
                print('\n\n\n{} {} {}'.format(area, experiment, session_id))
                print(pd.value_counts(y))

                # --- BIN SPIKES ---
                trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
                                                   trial_ids=trial_ids,
                                                   align_event=align_event,
                                                   time_before_in_s=time_before_in_s,
                                                   time_after_in_s=time_after_in_s)

                if np.isin(area, AREAS):
                    # --- SELECT UNITS ---
                    selected_unit_ids = session.uf[session.uf['RecArea'] == area].index

                    binned_spikes, bin_centers, bin_edges = session.bin_spikes_per_trial(
                                                       binsize=binsize_in_ms * pq.ms,
                                                       trial_starts=trial_starts,
                                                       trial_ends=trial_ends,
                                                       sliding_window=sliding_window,
                                                       slide_by=slide_by_in_ms * pq.ms,
                                                       unit_ids=selected_unit_ids)

                elif area == 'trackerdata':
                    binned_trackerdata, bin_centers, bin_edges = session.bin_trackerdata_per_trial(
                                                       binsize=binsize_in_ms * pq.ms,
                                                       trial_starts=trial_starts,
                                                       trial_ends=trial_ends,
                                                       sliding_window=sliding_window,
                                                       slide_by=slide_by_in_ms * pq.ms)

                elif area == 'all':
                    selected_unit_ids = session.uf.index
                    binned_spikes, bin_centers, bin_edges = session.bin_spikes_per_trial(
                                                       binsize=binsize_in_ms * pq.ms,
                                                       trial_starts=trial_starts,
                                                       trial_ends=trial_ends,
                                                       sliding_window=sliding_window,
                                                       slide_by=slide_by_in_ms * pq.ms,
                                                       unit_ids=selected_unit_ids)


                bc_aligned, be_aligned = session.get_aligned_bins(
                        spike_bin_centers=bin_centers,
                        spike_bin_edges=bin_edges,
                        align_event=align_event_times)
                # --- DECODE ---

                for time_bin, (time, (t0, t1)) in enumerate(zip(bc_aligned, be_aligned)) :

                    if np.isin(area, ['all']+AREAS) :
                        X = np.vstack([s[:, time_bin] for s in binned_spikes])
                    elif area == 'trackerdata' :
                        X = np.vstack([s[:, time_bin] for s in binned_trackerdata])

                    #print('\n\nTime bin {} of {}'.format(time_bin + 1, len(bc_aligned)))

                    for repeat in range(n_repeats) :

                        #print('- Repeat {}/{}'.format(repeat, n_repeats))


                        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                                random_state=repeat)
                        kfold_scores = []
                        y_test_all, y_pred_all = [], []

                        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                            if decoder_name == 'random_forest' :
                                decoder = RandomForestClassifier(n_estimators=n_estimators)
                            elif decoder_name == 'SGD' :
                                decoder = SGDClassifier()
                            else :
                                raise NotImplementedError

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]

                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                            decoder.fit(X_train, y_train)
                            y_pred = decoder.predict(X_test)
                            # TODO balanced?
                            scoring_function = balanced_accuracy_score
                            score = scoring_function(y_test, y_pred)
                            kfold_scores.append(score)
                            y_test_all.append(y_test)
                            y_pred_all.append(y_pred)

                        y_test_all = np.hstack(y_test_all)
                        y_pred_all = np.hstack(y_pred_all)

                        mean_score = np.mean(kfold_scores)

                        row = [session.animal_id, session_id, session.treatment,
                               area, combo, experiment,
                               decoder_name, time, time_bin, t0, t1, repeat, mean_score,
                               X.shape[1]]

                        df.loc[df.shape[0], :] = row

                        # rs[decoding_input]['true'][repeat, :] = targ_true
                        # rs[decoding_input]['pred'][repeat, :] = targ_pred

    numeric_cols = ['score']

    for col in numeric_cols :
        df[col] = pd.to_numeric(df[col])

    df['time'] = [t.item() for t in df['time']]


    pars = {'settings_name' : settings_name,
            'combo' : combo,
            'experiments' : experiments,
            'binsize_in_ms' : binsize_in_ms,
            'min_trials_per_class' : min_trials_per_class,
            'min_units' : min_units,
            #'sessions' : selected_session_ids,
            'decoder_name' : decoder_name,
            'n_splits' : n_splits,
            'spike_bin_centers' : bc_aligned,
            'spike_bin_edges' : be_aligned,
            'n_repeats' : n_repeats,
            'max_reaction_time' : max_reaction_time}
            #'score_name' : score_name,
            #'shuffle_kfold' : shuffle_kfold,
            # not strictly user defined
            #'selected_trials' : dp,
            #'n_time_bins_per_trial' : n_time_bins_per_trial,
            #'warp' : warp,
            #'warp_per_modality' : warp_per_modality,
            #'larger_bins' : larger_bins,
            #'median_reaction_times' : median_reaction_times}
    # 'time_bin_edges' : time_bin_edges,
    # 'time_bin_centers' : time_bin_centers}

    out = {'pars' : pars,
           'decoding_scores' : df}

    print('Saving output to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))

