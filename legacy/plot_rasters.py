import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn  as sns

# TODO add left vs right sorted by block performance

sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))

settings_name = 'mar15'
plot_format = 'png'


# TIME PARAMETERS
align_event = 'trial_start'
time_before_in_s = 1.5
time_after_in_s = 3.5
binsize_in_ms = 200
slide_by_in_ms = 100
xticks = [0, 1, 2, 3, 4]
sliding_window = True

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'psths_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


# --- LOAD SESSION ---


def get_vals(combo) :
    if combo == 'correct':
        vals = [0, 1]
    elif combo == 'separation' :
        vals = ['small', 'large']
    elif combo == 'response_location':
        vals = [2, 4, 5, 7]
    elif combo == 'block_id':
        vals = None
    else :
        raise ValueError
    return vals

def get_col(combo) :
    if combo == 'correct' :
        col = 'correct'
    elif combo == 'separation' :
        col = 'separation'
    elif combo == 'response_location' :
        col = 'response_location'
    elif combo == 'block_id':
        col = 'block_id'
    else:
        raise ValueError
    return col

combos = ['correct', 'separation', 'response_location',
          'block_id']

for session_id in SESSION_IDS:

    session = Session(session_id=session_id)
    # data = pickle.load(
    #     open(os.path.join(session.data_path, '{}.pickle'.format(session.session_id)),
    #          'rb'))

    session.load_data()


    # --- SELECT TRIALS ---
    #trial_ids = session.tf[np.isin(session.tf['response_location'], [2, 7])].index
    trial_ids = session.tf.index


    # --- BIN SPIKES ---
    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(trial_ids=trial_ids,
                                                                                  align_event=align_event,
                                                                                  time_before_in_s=time_before_in_s,
                                                                                  time_after_in_s=time_after_in_s)

    binned_spikes, spike_bin_centers, spike_bin_edges = session.bin_spikes_per_trial(binsize=binsize_in_ms * pq.ms,
                                    trial_starts=trial_starts,
                                    trial_ends=trial_ends,
                                    sliding_window=sliding_window,
                                    slide_by=slide_by_in_ms * pq.ms)

    binned_spikes = {t : s for t, s in zip(trial_ids, binned_spikes)}


    bc_aligned, be_aligned = session.get_aligned_bins(spike_bin_centers=spike_bin_centers,
                                                      spike_bin_edges=spike_bin_edges,
                                                      align_event=align_event_times)




    bc_aligned = bc_aligned.rescale(pq.s)

    uf = session.uf
    tf = session.tf
    uf['unit_index'] = np.arange(uf.shape[0])


    for unit_id, row in uf.iterrows():

        unit_ind = row['unit_index']
        unit_area = row['RecArea']

        unit_data = {c : {} for c in combos}

        for combo in combos:
            col = get_col(combo)

            if np.isin(combo, ['block_id']):
                vals = tf['block_id'].unique()
            else:
                vals = get_vals(combo)
            for ir, val in enumerate(vals):
                sdf = tf[tf[col] == val]
                if np.isin(combo, ['block_id']):
                    sdf = sdf[sdf['successful_block']>=1]
                try:
                    spikes = np.vstack([binned_spikes[tid][unit_ind, :] for tid in sdf.index])
                except ValueError:
                    spikes = np.zeros(shape=[0, len(bc_aligned)])
                unit_data[combo][val] = spikes

        unit_data['all'] = np.vstack([binned_spikes[tid][unit_ind, :] for tid in tf.index])

        # vmaxes = []
        # for ic, combo in enumerate(combos):
        #     vals = get_vals(combo)
        #     for im, modality in enumerate(modalities) :
        #         for val in vals:
        #             try:
        #                 vmaxes.append(unit_data[combo][modality][val].max())
        #             except ValueError:
        #                 pass
        # vmax = np.max(vmaxes)

        vmax = unit_data['all'].max()


        f = plt.figure(tight_layout=True, figsize=[10, 7])
        gs = gridspec.GridSpec(2,  3)
        #f.suptitle('Unit: {} - {}'.format(unit_id, unit_area), y=1.02)

        # --- PLOT IMSHOW ALL TRIALS ----------

        subplot_all = f.add_subplot(gs[:, -1])
        n_trials_all = unit_data['all'].shape[0]
        subplot_all.imshow(unit_data['all'],
                            vmin=0, vmax=vmax,
                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                    n_trials_all + 0.5, 0.5),
                            aspect='auto')

        subplot_all.set_xticks(xticks)
        subplot_all.set_yticks([1]+[n_trials_all])
        for t in [-1, 0]:
            subplot_all.axvline(t, c=sns.xkcd_rgb['white'], ls='--')
        subplot_all.set_xlabel('Time (s)')
        subplot_all.set_title('All trials')
        subplot_all.set_xticks(xticks)
        # --- PLOT IMSHOW ----------------------------------------------------------

        imshow_axes = {c : {} for c in combos}

        placements = [(0, 0), (0, 1), (1, 0), (1, 1)]
        for p, combo in zip(placements, combos):
            imshow_axes[combo] = f.add_subplot(gs[p[0], p[1]])

        for ic, combo in enumerate(combos) :

            if np.isin(combo, ['block_id']):
                vals = tf['block_id'].unique()
            else:
                vals = get_vals(combo)

            n_trials = [unit_data[combo][v].shape[0] for v in vals]
            n_trials_tot = np.sum(n_trials)
            print(n_trials)
            ticks = list(np.cumsum(n_trials)[:-1])
            ticks = [t for t in ticks if t!=0]

            data = np.vstack([unit_data[combo][v] for v in vals])

            imshow_axes[combo].imshow(data, vmin=0, vmax=vmax,
                                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                                    n_trials_tot + 0.5, 0.5),
                                            aspect='auto')

            imshow_axes[combo].set_xticks(xticks)
            imshow_axes[combo].set_yticks([1]+ ticks + [n_trials_tot])
            #imshow_axes[combo][modality].set_yticks([1, n_trials_tot])
            for n in ticks:
                if n != data.shape[0]:
                    imshow_axes[combo].axhline(n, c=sns.xkcd_rgb['white'])

            for t in [-1, 0]:
                imshow_axes[combo].axvline(t, c=sns.xkcd_rgb['white'], ls='--')

        for combo in combos:
            imshow_axes[combo].set_ylabel(combo)

        imshow_axes[combos[0]].set_title('{} - {}'.format(unit_id, unit_area))
        imshow_axes[combos[2]].set_xlabel('Time (s)')
        imshow_axes[combos[3]].set_xlabel('Time (s)')

        # for im, modality in enumerate(modalities) :
        #     imshow_axes[combos[-1]][modality].set_xlabel('Time (s)')
        #     imshow_axes[combos[0]][modality].set_title(modality)

        sns.despine()
        plt.tight_layout()

        plot_name = 'psth_simple_{}_{}.{}'.format(settings_name, unit_id,
                                                  plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


