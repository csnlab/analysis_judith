import numpy as np
from sklearn.preprocessing import LabelEncoder
import pandas as pd
from constants import *
from session import Session
import os
import pickle
from plotting_style import *
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score


def lighten_color(color, amount=0.5) :
    """
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try :
        c = mc.cnames[color]
    except :
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])


def select_trials_experiment(experiment, tf):

    tf_sel = tf.copy()

    if experiment == 'position':
        tf_sel = tf_sel.copy()
        n0 = tf_sel.shape[0]
        n1 = tf_sel.shape[0]
        target_col = None

    elif experiment == 'first_poke_side':
        tf_sel = tf_sel[np.isin(tf_sel['first_poke_location'], [2, 4, 5, 7])]
        n0 = tf_sel[np.isin(tf_sel['first_poke_location'], [2, 4])].shape[0]
        n1 = tf_sel[np.isin(tf_sel['first_poke_location'], [5, 7])].shape[0]
        target_col = 'first_poke_side'

    elif experiment == 'first_poke_location_large_separation':
        tf_sel = tf_sel[tf_sel['separation'] == 'large']
        tf_sel = tf_sel[np.isin(tf_sel['first_poke_location'], [2, 7])]
        n0 = tf_sel[tf_sel['first_poke_location'] == 2].shape[0]
        n1 = tf_sel[tf_sel['first_poke_location'] == 7].shape[0]
        target_col = 'first_poke_location'

    elif experiment == 'first_poke_location_small_separation':
        tf_sel = tf_sel[tf_sel['separation'] == 'small']
        tf_sel = tf_sel[np.isin(tf_sel['first_poke_location'], [4, 5])]
        n0 = tf_sel[tf_sel['first_poke_location'] == 4].shape[0]
        n1 = tf_sel[tf_sel['first_poke_location'] == 5].shape[0]
        target_col = 'first_poke_location'

    elif experiment == 'first_poke_loc_previous_trial_large_sep':
        tf_sel = tf_sel[tf_sel['separation'] == 'large']
        tf_sel = tf_sel[np.isin(tf_sel['previous_first_poke_location'], [2, 7])]
        n0 = tf_sel[tf_sel['previous_first_poke_location'] == 2].shape[0]
        n1 = tf_sel[tf_sel['previous_first_poke_location'] == 7].shape[0]
        target_col = 'previous_first_poke_location'

    elif experiment == 'first_poke_loc_previous_trial_small_sep':
        tf_sel = tf_sel[tf_sel['separation'] == 'small']
        tf_sel = tf_sel[np.isin(tf_sel['previous_first_poke_location'], [4, 5])]
        n0 = tf_sel[tf_sel['previous_first_poke_location'] == 4].shape[0]
        n1 = tf_sel[tf_sel['previous_first_poke_location'] == 5].shape[0]
        target_col = 'previous_first_poke_location'

    elif experiment == 'previous_poke_side':
        tf_sel = tf_sel[np.isin(tf_sel['previous_first_poke_location'], [2, 4, 5, 7])]
        n0 = tf_sel[np.isin(tf_sel['previous_first_poke_location'], [2, 4])].shape[0]
        n1 = tf_sel[np.isin(tf_sel['previous_first_poke_location'], [5, 7])].shape[0]
        target_col = 'previous_poke_side'

    elif experiment == 'response_location_large_separation':
        tf_sel = tf_sel[tf_sel['separation'] == 'large']
        tf_sel = tf_sel[np.isin(tf_sel['response_location'], [2, 7])]
        n0 = tf_sel[tf_sel['response_location'] == 2].shape[0]
        n1 = tf_sel[tf_sel['response_location'] == 7].shape[0]
        target_col = 'response_location'

    elif experiment == 'response_location_small_separation':
        tf_sel = tf_sel[tf_sel['separation'] == 'small']
        tf_sel = tf_sel[np.isin(tf_sel['response_location'], [4, 5])]
        n0 = tf_sel[tf_sel['response_location'] == 4].shape[0]
        n1 = tf_sel[tf_sel['response_location'] == 5].shape[0]
        target_col = 'response_location'

    elif experiment == 'previous_response_location_large_separation':
        tf_sel = tf_sel[tf_sel['separation'] == 'large']
        tf_sel = tf_sel[np.isin(tf_sel['previous_response_location'], [2, 7])] 
        n0 = tf_sel[tf_sel['previous_response_location'] == 2].shape[0]
        n1 = tf_sel[tf_sel['previous_response_location'] == 7].shape[0]
        target_col = 'previous_response_location'

    elif experiment == 'previous_response_location_small_separation':
        tf_sel = tf_sel[tf_sel['separation'] == 'small']
        tf_sel = tf_sel[np.isin(tf_sel['previous_response_location'], [4, 5])] 
        n0 = tf_sel[tf_sel['previous_response_location'] == 4].shape[0]
        n1 = tf_sel[tf_sel['previous_response_location'] == 5].shape[0]
        target_col = 'previous_response_location'

    elif experiment == 'separation':
        n0 = tf_sel[tf_sel['separation'] == 'small'].shape[0]
        n1 = tf_sel[tf_sel['separation'] == 'large'].shape[0]
        target_col = 'separation'

    elif experiment == 'separation_only_correct':
        tf_sel = tf_sel[tf_sel['correct'] == 1]
        n0 = tf_sel[tf_sel['separation'] == 'small'].shape[0]
        n1 = tf_sel[tf_sel['separation'] == 'large'].shape[0]
        target_col = 'separation'

    elif experiment == 'separation_only_incorrect':
        tf_sel = tf_sel[tf_sel['correct'] == 0]
        n0 = tf_sel[tf_sel['separation'] == 'small'].shape[0]
        n1 = tf_sel[tf_sel['separation'] == 'large'].shape[0]
        target_col = 'separation'

    elif experiment == 'block_performance_binarized':
        tf_sel['block_performance_bin'] = [0 if b <= 3 else 1 for b in tf_sel['block_performance']]
        n0 = tf_sel[tf_sel['block_performance_bin'] == 0].shape[0]
        n1 = tf_sel[tf_sel['block_performance_bin'] == 1].shape[0]
        target_col = 'block_performance_bin'

    elif experiment == 'successful_block_binarized':
        tf_sel['successful_block_bin'] = [0 if b == 0 else 1 for b in tf_sel['successful_block']]
        n0 = tf_sel[tf_sel['successful_block_bin'] == 0].shape[0]
        n1 = tf_sel[tf_sel['successful_block_bin'] == 1].shape[0]
        target_col = 'successful_block_bin'

    elif experiment == 'correct':
        n0 = tf_sel[tf_sel['correct'] == 0].shape[0]
        n1 = tf_sel[tf_sel['correct'] == 1].shape[0]
        target_col = 'correct'

    elif experiment == 'correct_only_valid':
        tf_sel = tf_sel[np.isin(tf_sel['first_poke_location'], [2, 4, 5, 7])]
        n0 = tf_sel[tf_sel['correct'] == 0].shape[0]
        n1 = tf_sel[tf_sel['correct'] == 1].shape[0]
        target_col = 'correct'
    else:
        raise ValueError


    if target_col is not None:
        assert np.unique(tf_sel[target_col]).shape[0] == 2
        ll = LabelEncoder()
        target = ll.fit_transform(tf_sel[target_col])
        tf_sel['target'] = target
    else:
        target = None
        tf_sel['target'] = target

    return tf_sel, target, n0, n1


def select_data_experiment(experiment, session_ids, filter_trials=True,
                        max_reaction_time=5, align_event='stimulus_on',
                           return_trials=False, bad_units=None):

    sdf = pd.DataFrame(columns=['experiment', 'treatment', 'n0', 'n1']+AREAS+['all_areas'])

    tf_sel_dict = {}
    for session_id in session_ids:
        session = Session(session_id=session_id, verbose=0,
                          raise_warnings=False)
        session.load_data(bad_units=bad_units)
        if filter_trials:
            session.align_event_times(align_event=align_event,
                                      convert_to_seconds=True)
            session.filter_trials(max_reaction_time=max_reaction_time)

        tf_sel, target, n0, n1 = select_trials_experiment(experiment=experiment,
                                                          tf=session.tf)

        for area in AREAS:
            sdf.loc[session_id, area] = session.uf[session.uf['RecArea'] == area].shape[0]
        sdf.loc[session_id, 'all_areas'] = np.sum([sdf.loc[session_id, a] for a in AREAS])
        sdf.loc[session_id, ['experiment', 'n0', 'n1']] = [experiment, n0, n1]
        sdf.loc[session_id, 'treatment'] = session.treatment

        tf_sel['target'] = target
        tf_sel_dict[session_id] = tf_sel

    if return_trials:
        return sdf, tf_sel_dict
    else:
        return sdf


def get_vals(combo) :
    if combo == 'correct':
        vals = [0, 1]
    elif combo == 'separation' :
        vals = ['small', 'large']
    elif combo == 'response_location':
        vals = [2, 4, 5, 7]
    elif combo == 'first_poke_location': # assuming it is filtered
        vals = [2, 4, 5, 7]
    elif combo == 'response_location_correct':
        vals = [2, 4, 5, 7]
    elif combo == 'block_id':
        vals = None
    elif combo == 'successful_block':
        vals = [1, 2, 3, 4, 5, 6, 7, 8, 9]
    else :
        raise ValueError
    return vals

def get_col(combo) :
    if combo == 'correct' :
        col = 'correct'
    elif combo == 'separation' :
        col = 'separation'
    elif combo == 'response_location':
        col = 'response_location'
    elif combo == 'first_poke_location':
        col = 'first_poke_location'
    elif combo == 'response_location_correct':
        col = 'response_location'
    elif combo == 'block_id':
        col = 'block_id'
    elif combo == 'successful_block':
        col = 'successful_block'
    else:
        raise ValueError
    return col



def load_binned_data_pars(binned_data_settings,
                          data_path=None,
                          data_version=None):
    if data_path is None:
        data_path = DATA_PATH
    if data_version is None:
        data_version = DATA_VERSION
    binned_data_pars_folder = os.path.join(data_path, 'binned_data', data_version,
                                           binned_data_settings)
    binned_data_pars_file_name = 'binned_{}_pars.pkl'.format(binned_data_settings)
    binned_data_pars_fullpath = os.path.join(binned_data_pars_folder, binned_data_pars_file_name)
    binned_data_pars = pickle.load(open(binned_data_pars_fullpath, 'rb'))
    return binned_data_pars



def plot_events(tf, ax, events=None):
    if events is None :
        events = ['PokeC_off_aligned',
                  'stimulus_on_aligned',
                  'first_poke_time_aligned']
    xlim1 = ax.get_xlim()[0]
    xlim2 = ax.get_xlim()[1]

    for event in events :
        q1, q2, q3 = tf[event].quantile([0.05, 0.5, 0.95]).__array__()
        ax.axvspan(max(q1, xlim1), min(q3, xlim2), alpha=0.15,
                   color='grey', linewidth=0,
                   zorder=-10)
        if xlim1 < q2 < xlim2 :
            # TODO you could write on the edge of the
            # TODO vspan
            ax.axvline(q2, alpha=0.3, c='grey', zorder=-10)
            ax.text(x=q2, y=ax.get_ylim()[1] + 0.01,
                    s=event_labels[event],
                    ha='center',
                    size=7)


def load_tf_uf_global(align_event, max_reaction_time, session_ids) :
    tfs, ufs = [], []
    for session_id in session_ids:
        session = Session(session_id=session_id)
        session.load_data(load_spiketrains=False)
        session.align_event_times(align_event=align_event,
                                  convert_to_seconds=True)
        session.filter_trials(max_reaction_time=max_reaction_time)
        tfs.append(session.tf)
        ufs.append(session.uf)

    tf = pd.concat(tfs)
    uf = pd.concat(ufs)
    return tf, uf


def crossval_routine(X, y, n_splits, shuffle,
                     random_state, decoder_name,
                     n_estimators,
                     standardize=True) :
    kfold = StratifiedKFold(n_splits=n_splits, shuffle=shuffle,
                            random_state=random_state)
    kfold_scores = []
    y_test_all, y_pred_all = [], []

    for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

        if decoder_name == 'random_forest' :
            decoder = RandomForestClassifier(n_estimators=n_estimators)
        elif decoder_name == 'SGD' :
            decoder = SGDClassifier()
        else :
            raise NotImplementedError

        X_train = X[training_ind, :]
        X_test = X[testing_ind, :]
        y_train = y[training_ind]
        y_test = y[testing_ind]

        if standardize :
            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

        decoder.fit(X_train, y_train)
        y_pred = decoder.predict(X_test)

        scoring_function = balanced_accuracy_score
        score = scoring_function(y_test, y_pred)
        kfold_scores.append(score)
        y_test_all.append(y_test)
        y_pred_all.append(y_pred)

    y_test_all = np.hstack(y_test_all)
    y_pred_all = np.hstack(y_pred_all)
    mean_score = np.mean(kfold_scores)
    return mean_score, y_test_all, y_pred_all


def get_session_overview(data_path=None, data_version=None):
    if data_path is None:
        data_path = DATA_PATH
    if data_version is None:
        data_version = DATA_VERSION

    sf = pd.read_excel(os.path.join(data_path, data_version, 'session_overview.xlsx'))
    sf = sf.drop('Unnamed: 0', axis='columns').set_index('sid')
    return sf


def get_preprocessed_sessions(data_path=None, data_version=None):

    if data_path is None:
        data_path = DATA_PATH
    if data_version is None:
        data_version = DATA_VERSION

    sf = pd.read_excel(os.path.join(data_path, data_version, 'session_overview.xlsx'))
    sf = sf.drop('Unnamed: 0', axis='columns').set_index('sid')

    sfpp = sf.copy()
    sfpp = sfpp[sfpp['has_spiketrains'] == 1]
    sfpp = sfpp[sfpp['is_processed'] == 1]
    #sfpp = sfpp[sfpp['video_check'] == 1]

    if data_version == 'data_V15_06_05_2024':
        print('DATA VERSION {} - MUA ONLY FOR ANIMALS 4 and 5')
        print('\nREMOVING ANIMAL 2')
        sfpp = sfpp[np.isin(sfpp['Animal ID'], [4, 5])]

    SESSION_IDS = list(sfpp.index)
    SESSION_IDS = [s for s in SESSION_IDS if s not in SKIP_SESSIONS]

    print('SKIPPING SESSIONS: {}'.format(SKIP_SESSIONS))

    return SESSION_IDS


def log2_likelihood(true, predictions) :
    y_true = true
    y_pred = predictions[:, 1]
    eps = np.finfo(y_pred.dtype).eps
    y_pred = np.clip(y_pred, eps, 1 - eps)
    terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
    ll = np.sum(terms) / len(y_true)
    return ll
