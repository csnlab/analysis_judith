import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from utils import *

SESSION_IDS = get_preprocessed_sessions()

for session_id in SESSION_IDS:
    print('\n\n')
    print(session_id)
    session = Session(session_id=session_id, data_version=DATA_VERSION)

    session.load_data(trial_preprocessing=True, bad_units=None)
    #print(session.tf['stimulus_on'])

    uf = session.uf
    print(uf['ClusterGroup'].value_counts())

    rev = session.raw_events
    session.tf['PokeC_off'] = None

    for trial_id in session.tf.index :


        firstpoke = session.tf.loc[trial_id, 'first_valid_poke_time']
        pokeloc = session.tf.loc[trial_id, 'first_valid_poke_location']

        licks = rev['Lick{}_on'.format(pokeloc)]
        firstlicktime = licks[licks>=firstpoke][0]

        session.tf.loc[trial_id, 'first_valid_lick_time'] = firstlicktime



tf = session.tf

for i in range(tf.shape[0]-1):
    if tf.iloc[i]['separation'] != tf.iloc[i+1]['separation']:
        session.block_switch_time = tf.iloc[i]['trial_start'] * pq.ms

session.task_start_time = tf.iloc[0]['trial_start'] * pq.ms
session.task_end_time = tf.iloc[-1]['trial_end'] * pq.ms


binned_spikes, spike_bin_centers, spike_bin_edges = session._bin_spikes(binsize=5*pq.s,
                                                                        t_start=session.first_spike_time,
                                                                        t_stop=session.last_spike_time,
                                                                        unit_ids=[session.uf.index[0]])


tf, uf = load_tf_uf_global(align_event='PokeC_off',
                           max_reaction_time=7,
                           session_ids=SESSION_IDS)


for treatment in TREATMENTS:
    print(treatment)
    for area in AREAS:

        n = uf[(uf['treatment'] == treatment) & (uf['RecArea'] == area)].shape[0]
        print(area, n)



#
# for session_id in SESSION_IDS:
#     session = Session(session_id=session_id,  data_version='data_V6_22_03')
#     session.load_data(trial_preprocessing=True, bad_units=None)
#
#     for id in bad_units:
#         try:
#             sel = session.uf.loc[id, :]
#             print('\nUnit ID: {}\nSession ID: {}\nTetrode n: {}\n'
#                   'Cluster ID: {}\nCluster group: {}'.format(id,
#                                                              sel['session_id'],
#                                                              sel['TetrodeNr'],
#                                                              sel['ClusterID'],
#                                                              sel['ClusterGroup']))
#         except KeyError:
#             pass
#
#
#
