import os
from constants import *
import pandas as pd
import quantities as pq
import pickle
from session import Session
import matplotlib.pyplot as plt
from matplotlib import gridspec
from plotting_style import *
from utils import *

settings_name = 'may2'
plot_format = 'png'
data_version = DATA_VERSION

binned_data_settings = 'apr26_reward'

plot_decoding = True
decode_settings = 'may2'
decode_experiments = ['separation', 'correct']

max_reaction_time = 5 # in seconds

smooth_sigma = 1.4
spatial_bin_size = 23 # 3 cm as in rat robot
occupancy_threshold_in_s = 1
#min_spikes_bin = 3
x_max_global = 576
y_max_global = 720

plt.rc('legend', fontsize=6)

plt.rc('xtick', labelsize=8)
plt.rc('ytick', labelsize=8)
plt.rc('axes', titlesize=9)
plt.rc('axes', labelsize=9)

binsize_firing_rate_global = 5 * pq.s

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'single_cell_panels', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


combos = ['correct', 'separation', 'first_poke_location',
          'successful_block']

ratemap_selections = ['all', 'small\nseparation', 'large\nseparation']


# --- LOAD DECODING ------------------------------------------------------------

if plot_decoding:
    dfs = []
    for experiment in decode_experiments:
        output_file_name = 'decode_ubu_setting_{}_{}.pkl'.format(decode_settings,
                                                                 experiment)
        output_folder = os.path.join(DATA_PATH, 'results', 'decode_ubu',
                                     decode_settings)
        output_full_path = os.path.join(output_folder, output_file_name)
        res = pickle.load(open(output_full_path, 'rb'))
        align_event = res['pars']['align_event']
        max_reaction_time = res['pars']['max_reaction_time']

        res = pickle.load(open(output_full_path, 'rb'))
        df = res['decoding_scores']

        df = df.drop(['t0', 't1', 'time_bin'], axis='columns')


        dfs.append(df)
    df = pd.concat(dfs)
    experiments = df['experiment'].unique()


SESSION_IDS = get_preprocessed_sessions()


for session_id in SESSION_IDS:

    # --- LOAD SESSION ---
    session = Session(session_id=session_id, data_version=data_version)
    session.load_data(load_spiketrains=True)
    session.load_binned_data(binned_data_settings=binned_data_settings,
                             bad_units=None)
    session.align_event_times(align_event=session.binned_data['align_event'],
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)

    binsize_in_ms = session.binned_data['binsize_in_ms']
    uf = session.uf
    tf = session.tf

    tf_sorted = tf.sort_values(by=['first_poke_location',
                                   'first_poke_time_aligned'])

    uf['unit_index'] = np.arange(uf.shape[0])
    trial_ids = tf.index

    binned_spikes = session.binned_data['binned_spikes']
    bc_aligned = session.binned_data['bin_centers_aligned'].rescale(pq.s)
    be_aligned =  session.binned_data['bin_edges_aligned']


    # --- GET EVENT TIMES ----------------------------------------------------------



    # --- COMPUTE OCCUPANCY ---

    #occupancy[occupancy < occupancy_threshold] = 0
    x_pos = session.trackerdata_filtered['head']['x']
    y_pos = session.trackerdata_filtered['head']['y']

    x_max_crop = x_pos.max()
    y_max_crop = y_pos.max()

    x_min_crop = x_pos[x_pos>0].min()
    y_min_crop = y_pos[y_pos>0].min()

    occupancies = {}

    for selection in ratemap_selections :

        if selection == 'all' :
            trial_ids = tf.index
        elif selection == 'small\nseparation' :
            trial_ids = tf[tf['separation'] == 'small'].index
        elif selection == 'large\nseparation' :
            trial_ids = tf[tf['separation'] == 'large'].index

        occupancy = session.compute_spatial_occupancy(spatial_bin_size=spatial_bin_size,
                                                      x_max=x_max_global,
                                                      y_max=y_max_global,
                                                      trial_ids=trial_ids,
                                                      smooth_sigma=smooth_sigma)

        occupancy[occupancy < occupancy_threshold_in_s * pq.s] = 0 * pq.s
        occupancy = np.ma.masked_where(occupancy == 0, occupancy)
        occupancies[selection] = occupancy
    oms = np.hstack([occupancies[s] for s in ratemap_selections])
    oms = oms[np.isfinite(oms)]
    vmax_occupancies = np.nanmax(oms)


    for unit_id, row in uf.iterrows():#uf.iloc[0:1].iterrows():#uf.iterrows():#

        print('Plotting unit {}'.format(unit_id))
        unit_ind = row['unit_index']
        unit_area = row['RecArea']

        # --- GET SPIKING DATA OVER TIME ---
        unit_data = {c : {} for c in combos}
        unit_data_trials = {c : [] for c in combos}

        for combo in combos:
            col = get_col(combo)
            if np.isin(combo, ['block_id']):
                vals = tf['block_id'].unique()
            else:
                vals = get_vals(combo)
            for ir, val in enumerate(vals):
                sdf = tf[tf[col] == val]
                if np.isin(combo, ['block_id']):
                    sdf = sdf[sdf['successful_block']>=1]
                if np.isin(combo, ['response_location_correct']):
                    sdf = sdf[sdf['correct'] == 1]
                try:
                    spikes = np.vstack([binned_spikes[tid][unit_ind, :] for tid in sdf.index])
                except ValueError:
                    spikes = np.zeros(shape=[0, len(bc_aligned)])
                unit_data[combo][val] = spikes
                unit_data_trials[combo].append(sdf.index)
        for combo in combos:
            unit_data_trials[combo] = np.hstack(unit_data_trials[combo])


        unit_data['all'] = np.vstack([binned_spikes[tid][unit_ind, :] for tid in tf.index])
        unit_data['all_sorted'] = np.vstack([binned_spikes[tid][unit_ind, :] for tid in tf_sorted.index])

        vmax = unit_data['all'].max()

        # sorted data

        # --- COMPUTE RATEMAPS ---
        ratemaps = {}
        maxes = []
        for selection in ratemap_selections:
            if selection == 'all':
                trial_ids = tf.index
            elif selection == 'small_separation':
                trial_ids = tf[tf['separation'] == 'small'].index
            elif selection == 'large_separation':
                trial_ids = tf[tf['separation'] == 'large'].index

            spikes_mat = session.compute_spatial_firing(unit_id=unit_id,
                                                        x_max=x_max_global,
                                                        y_max=y_max_global,
                                                        spatial_bin_size=spatial_bin_size,
                                                        trial_ids=trial_ids,
                                                        smooth_sigma=smooth_sigma)

            ratemaps[selection] = spikes_mat / occupancies[selection]
            maxes.append(ratemaps[selection].max())
        vmax_ratemaps = np.max(maxes)


        # --- COMPUTE FIRING RATE ACROSS FULL SESSION ---

        binned_spikes_all, spike_bin_centers, spike_bin_edges = session._bin_spikes(
                                                            binsize=binsize_firing_rate_global,
                                                            t_start=session.first_spike_time,
                                                            t_stop=session.last_spike_time,
                                                            unit_ids=[unit_id])

        binned_td, td_bin_centers, td_bin_edges = session._bin_trackerdata(
                                                            binsize=binsize_firing_rate_global,
                                                            t_start=session.first_spike_time,
                                                            t_stop=session.last_spike_time)

        x_indx = session.trackerdata_binned_names.index('neck_middle_x')
        y_indx = session.trackerdata_binned_names.index('neck_middle_y')

        binned_td = binned_td[[x_indx, y_indx], :]

        spike_bin_centers = spike_bin_centers.rescale(pq.min)
        td_bin_centers = td_bin_centers.rescale(pq.min)

        firing_rate = binned_spikes_all / binsize_firing_rate_global


        # --- PLOT ---
        f = plt.figure(tight_layout=True, figsize=[12, 7])
        gs = gridspec.GridSpec(5,  6)

        # --- ADD AXES -------------------------
        subplot_all = f.add_subplot(gs[0:4, 2])
        subplot_all_sorted = f.add_subplot(gs[0:4, 3])


        imshow_axes = {c : {} for c in combos}

        imshow_axes[combos[0]] = f.add_subplot(gs[0:2, 0])
        imshow_axes[combos[1]] = f.add_subplot(gs[0:2, 1])
        imshow_axes[combos[2]] = f.add_subplot(gs[2:4, 0])
        imshow_axes[combos[3]] = f.add_subplot(gs[2:4, 1])

        # placements = [(0, 0), (0, 1), (1, 0), (1, 1)]
        # for p, combo in zip(placements, combos):
        #     imshow_axes[combo] = f.add_subplot(gs[p[0], p[1]])

        occupancy_axes = {}
        occupancy_axes[ratemap_selections[0]] = f.add_subplot(gs[0, 4])
        occupancy_axes[ratemap_selections[1]] = f.add_subplot(gs[1, 4])
        occupancy_axes[ratemap_selections[2]] = f.add_subplot(gs[2, 4])

        ratemap_axes = {}
        ratemap_axes[ratemap_selections[0]] = f.add_subplot(gs[0, 5])
        ratemap_axes[ratemap_selections[1]] = f.add_subplot(gs[1, 5])
        ratemap_axes[ratemap_selections[2]] = f.add_subplot(gs[2, 5])

        if plot_decoding:
            decoding_axes = {}
            decoding_axes[experiments[0]] = f.add_subplot(gs[3, 4:])
            try:
                decoding_axes[experiments[1]] = f.add_subplot(gs[4, 4:])
            except:
                pass
        firing_rate_axis = f.add_subplot(gs[4, :4])
        trackerdata_axis = firing_rate_axis.twinx()


        # --- PLOT OCCUPANCIES ----------------
        cmap = rasters_cmap
        cmap.set_bad(color='white')
        for selection in ratemap_selections:
            occupancy_axes[selection].imshow(occupancies[selection].T, origin='lower',
                                             extent=[0 + spatial_bin_size / 2,
                                                     x_max_global + spatial_bin_size / 2,
                                                     0 + spatial_bin_size / 2,
                                                     y_max_global + spatial_bin_size / 2],
                                           cmap=cmap,
                                           vmin=0,
                                           vmax=vmax_occupancies)
            occupancy_axes[selection].set_xticks([])
            occupancy_axes[selection].set_yticks([])
            for spine in ['left', 'right', 'top', 'bottom']:
                occupancy_axes[selection].spines[spine].set_visible(False)
            #sns.despine(ax=ratemap_axes[selection], bottom=True, left=True)
            occupancy_axes[selection].set_ylabel(selection)
            for l in list(session.poke_locations)+['C']:
                x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
                y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
                occupancy_axes[selection].scatter(x, y, c=location_palette[l], zorder=10)
            occupancy_axes[selection].set_xlim([x_min_crop, x_max_crop])
            occupancy_axes[selection].set_ylim([y_min_crop, y_max_crop])


        # --- PLOT RATEMAPS ----------------
        for selection in ratemap_selections:
            ratemap_axes[selection].imshow(ratemaps[selection].T, origin='lower',
                                           extent=[0 + spatial_bin_size / 2,
                                            x_max_global + spatial_bin_size / 2,
                                            0 + spatial_bin_size / 2,
                                            y_max_global + spatial_bin_size / 2],
                                           cmap=ratemaps_cmap,
                                           vmin=0,
                                           vmax=vmax_ratemaps)
            ratemap_axes[selection].set_xticks([])
            ratemap_axes[selection].set_yticks([])
            for spine in ['left', 'right', 'top', 'bottom']:
                ratemap_axes[selection].spines[spine].set_visible(False)
                #ratemap_axes[selection].set_ylabel(selection)

            for l in list(session.poke_locations)+['C']:
                x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
                y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
                ratemap_axes[selection].scatter(x, y, c=location_palette[l], zorder=10)
            ratemap_axes[selection].set_xlim([x_min_crop, x_max_crop])
            ratemap_axes[selection].set_ylim([y_min_crop, y_max_crop])

        occupancy_axes[ratemap_selections[0]].set_title('Occupancy')
        ratemap_axes[ratemap_selections[0]].set_title('Ratemap')


        # --- PLOT FIRING RATE GLOBAL ---
        #firing_rate_axis.scatter(spike_bin_centers, binned_spikes[0, :], color='k', zorder=10)

        trackerdata_axis.scatter(td_bin_centers, binned_td[0, :], color='tab:blue',
                                 alpha=0.4, s=3, zorder=-10)
        firing_rate_axis.plot(spike_bin_centers, firing_rate[0, :], color='k')

        trackerdata_axis.legend()

        firing_rate_axis.set_ylabel('Firing rate (Hz)')
        trackerdata_axis.set_ylabel('Animal position', color='tab:blue')
        firing_rate_axis.set_xlabel('Time (min.)')
        firing_rate_axis.set_xlim([td_bin_centers[0], td_bin_centers[-1]])
        trackerdata_axis.set_xlim([td_bin_centers[0], td_bin_centers[-1]])

        trackerdata_axis.axvline(session.task_start_time.rescale(pq.min), c='grey',
                                 lw=3)
        trackerdata_axis.axvline(session.task_end_time.rescale(pq.min), c='grey',
                                 lw=3)
        trackerdata_axis.axvline(session.block_switch_time.rescale(pq.min), c='grey',
                                 lw=3, ls='--')

        task_times = [session.task_start_time, session.block_switch_time,
                      session.task_end_time]

        task_time_labels = ['Task start', 'Block switch', 'Task end']

        for label, time in zip(task_time_labels, task_times):
            firing_rate_axis.text(x=time.rescale(pq.min), y=firing_rate_axis.get_ylim()[1] + 0.2,
                               s=label, ha='center', size=6)

        sns.despine(ax=firing_rate_axis)
        sns.despine(ax=trackerdata_axis)


        # --- PLOT DECODING ---------------------
        if plot_decoding:
            for experiment in experiments:
                decoding_axis = decoding_axes[experiment]
                events = ['PokeC_off', 'stimulus_on', 'first_poke_time']
                events = ['{}_aligned'.format(e) for e in events]

                dx = df[df['experiment'] == experiment].copy()
                dx = dx.drop('experiment', axis='columns')
                dx = dx[dx['session_id'] == session_id]
                groupbys = [c for c in dx.columns if c not in ['repeat', 'score']]
                dx = dx.groupby(groupbys).mean().reset_index()
                dx = dx[np.logical_or(dx['feature_id'] == unit_id, dx['feature_type'] == 'behavior')]

                sns.lineplot(data=dx, x='time', y='score', hue='area_spikes',
                             palette=decoding_features_palette, ci=None,
                             ax=decoding_axis)
                decoding_axis.set_xlabel('Time [s]'.format(align_event))
                decoding_axis.set_ylabel('Decoding\n{}'.format(experiment))

                decoding_axis.axhline(0.5, c='grey', ls=':')
                decoding_axis.axvline(0, c='grey', ls='--')

                decoding_axis.set_ylim([0.4, 1])

                has = ['center', 'right', 'left']
                for ha, event in zip(has, events):

                    q1, q2, q3 = tf[event].quantile(
                        [0.05, 0.5, 0.95]).__array__()
                    decoding_axis.axvspan(q1, q3, alpha=0.15, color='grey', linewidth=0,
                               zorder=-10)
                    decoding_axis.axvline(q2, alpha=0.3, c='grey', zorder=-10)
                    decoding_axis.text(x=q2, y=decoding_axis.get_ylim()[1] + 0.01, s=event_labels[event],
                            ha=ha,
                            size=6)

                sns.despine(ax=decoding_axis)
                decoding_axis.set_xlim([-session.binned_data['time_before_in_s'],
                                        session.binned_data['time_after_in_s']])

        # --- PLOT IMSHOW ALL TRIALS ----------

        n_trials_all = unit_data['all'].shape[0]
        subplot_all.imshow(unit_data['all'],
                            vmin=0, vmax=vmax,
                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                    n_trials_all + 0.5, 0.5),
                            aspect='auto',
                            cmap=rasters_cmap)

        #subplot_all.set_xticks(xticks)
        subplot_all.set_yticks([1]+[n_trials_all])
        for t in [0]:
            subplot_all.axvline(t, c=sns.xkcd_rgb['white'], ls='--')
        subplot_all.set_xlabel('Time (s)')
        subplot_all.set_title('All trials')
        #subplot_all.set_xticks(xticks)

        # for trial_indx, trial_id in enumerate(tf.index):
        #     poke_location = tf.loc[trial_id, 'first_poke_location']
        #     t_first_poke_aligned = tf.loc[trial_id, 'first_poke_time_aligned']
        #     subplot_all.scatter(t_first_poke_aligned, trial_indx + 1,
        #                                    marker='|',
        #                                    color=location_palette[poke_location])
        xlim = subplot_all.get_xlim()
        for trial_indx, trial_id in enumerate(tf.index):
            for poke_location, poke_label in zip(session.poke_locations, session.poke_on_labels_aligned):
                t_first_poke = tf.loc[trial_id, poke_label]
                if t_first_poke is not None:
                    subplot_all.scatter(t_first_poke, trial_indx + 1,
                                                   marker='|',
                                                   color=location_palette[poke_location])
        subplot_all.set_xlim(xlim)

        # --- PLOT IMSHOW ALL TRIALS SORTED ----------

        n_trials_all = unit_data['all_sorted'].shape[0]
        subplot_all_sorted.imshow(unit_data['all_sorted'],
                            vmin=0, vmax=vmax,
                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                    n_trials_all + 0.5, 0.5),
                            aspect='auto',
                            cmap = rasters_cmap)

        #subplot_all_sorted.set_xticks(xticks)
        subplot_all_sorted.set_yticks([1]+[n_trials_all])
        for t in [0]:
            subplot_all_sorted.axvline(t, c=sns.xkcd_rgb['white'], ls='--')
        subplot_all_sorted.set_xlabel('Time (s)')
        subplot_all_sorted.set_title('All trials (sorted)')
        #subplot_all_sorted.set_xticks(xticks)

        xlim = subplot_all_sorted.get_xlim()
        for trial_indx, trial_id in enumerate(tf_sorted.index):
            for poke_location, poke_label in zip(session.poke_locations, session.poke_on_labels_aligned):
                t_first_poke = tf.loc[trial_id, poke_label]
                subplot_all_sorted.scatter(t_first_poke, trial_indx + 1,
                                           marker='|',
                                           color=location_palette[poke_location])
        subplot_all_sorted.set_xlim(xlim)

        # --- PLOT IMSHOW ----------------------------------------------------------

        for ic, combo in enumerate(combos) :

            if np.isin(combo, ['block_id']):
                vals = tf['block_id'].unique()
            else:
                vals = get_vals(combo)

            n_trials = [unit_data[combo][v].shape[0] for v in vals]
            n_trials_tot = np.sum(n_trials)
            ticks = list(np.cumsum(n_trials)[:-1])
            ticks = [t for t in ticks if t!=0]

            data = np.vstack([unit_data[combo][v] for v in vals])

            imshow_axes[combo].imshow(data, vmin=0, vmax=vmax,
                                            extent=(bc_aligned[0] - pq.ms * binsize_in_ms / 2,
                                                    bc_aligned[-1] + pq.ms * binsize_in_ms / 2,
                                                    n_trials_tot + 0.5, 0.5),
                                            aspect='auto',
                                            cmap=rasters_cmap)

            #imshow_axes[combo].set_xticks(xticks)
            imshow_axes[combo].set_yticks([1]+ ticks + [n_trials_tot])

            # for trial_indx, trial_id in enumerate(unit_data_trials[combo]):
            #     poke_location = tf.loc[trial_id, 'first_poke_location']
            #     t_first_poke_aligned = tf.loc[trial_id, 'first_poke_time_aligned']
            #     imshow_axes[combo].scatter(t_first_poke_aligned, trial_indx+1,
            #                                marker='|', color=location_palette[poke_location])
            imshow_axes[combo].get_xlim()
            for trial_indx, trial_id in enumerate(unit_data_trials[combo]):
                for poke_location, poke_label in zip(session.poke_locations, session.poke_on_labels_aligned):
                    t_first_poke = tf.loc[trial_id, poke_label]
                    imshow_axes[combo].scatter(t_first_poke, trial_indx+1,
                                                   marker='|', color=location_palette[poke_location])
            imshow_axes[combo].set_xlim(xlim)

            for n in ticks:
                if n != data.shape[0]:
                    imshow_axes[combo].axhline(n, c=sns.xkcd_rgb['white'])

            for t in [0]:
                imshow_axes[combo].axvline(t, c=sns.xkcd_rgb['white'], ls='--')

        for combo in combos:
            imshow_axes[combo].set_ylabel(combo)

            sns.despine(ax=imshow_axes[combo])

        sns.despine(ax=subplot_all)

        imshow_axes[combos[0]].set_title('{}-{} ({})'.format(unit_id, unit_area, session.treatment))
        imshow_axes[combos[2]].set_xlabel('Time (s)')
        imshow_axes[combos[3]].set_xlabel('Time (s)')

        plt.tight_layout()


        #gs.update(wspace=0.35)

        plot_name = 'psth_simple_{}_{}.{}'.format(settings_name, unit_id,
                                                  plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()


