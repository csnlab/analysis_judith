import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder

align_event = 'PokeC_off'
time_before_in_s = 10
time_after_in_s = 10
sliding_window = True
max_reaction_time = 5

for session_id in [SESSION_IDS[0]] :
    session = Session(session_id=session_id)
    session.load_data(generate_trackerdata_filtered=True,
                      compute_velocity=True,
                      trackerdata_likelihood_threshold=0.9)
    tf = session.tf

    session.align_event_times(align_event=align_event,
                              convert_to_seconds=True)

    session.filter_trials(max_reaction_time=max_reaction_time)

    tf['PokeC_off']

    # --- SELECT TRIALS ---
    tf = session.tf

    trial_ids = list(tf.index)

    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
        trial_ids=trial_ids,
        align_event=align_event,
        time_before_in_s=time_before_in_s,
        time_after_in_s=time_after_in_s)

    time = session.trackerdata['time']
    time_filt = session.trackerdata_filtered['time']

    tid = 'tid_R4_S01_055'
    trial_start = trial_starts[trial_ids.index(tid)]
    trial_end = trial_ends[trial_ids.index(tid)]

    # for tracker_name in ['head', 'neck_middle', 'tail']:
    #     likelihood_mask = session.trackerdata[tracker_name]['likelihood'] > 0.9
    #     perc_points = likelihood_mask.sum() / likelihood_mask.shape[0]
    #

    time_mask = np.logical_and(time >= trial_start, time < trial_end)
    likelihood_mask = session.trackerdata['head']['likelihood'] > session.trackerdata_likelihood_threshold
    time_mask_filt = np.logical_and(time_filt >= trial_start, time_filt < trial_end)

    time_trial = time[np.logical_and(time_mask, likelihood_mask)]
    time_trial_nolike = time[np.logical_and(time_mask, ~likelihood_mask)]

    time_trial_filt = time_filt[time_mask_filt]

    pos = session.trackerdata['head']['x'][np.logical_and(time_mask, likelihood_mask)]
    pos_nolike = session.trackerdata['head']['x'][np.logical_and(time_mask, ~likelihood_mask)]

    pos_filt = session.trackerdata_filtered['head']['x'][time_mask_filt]

    len(pos) / (len(pos)+len(pos_nolike))

    f, ax = plt.subplots(1, 1, figsize=[10, 4])

    ax.scatter(time_trial, pos, c='k', s=8, label='Raw')
    #ax.plot(time_trial, pos, c='k', lw=4)

    ax.scatter(time_trial_nolike, pos_nolike, c='grey', s=6, label='Excluded Raw')
    #ax.plot(time_trial_nolike, pos_nolike, c='grey')

    #ax.scatter(time_trial_filt, pos_filt, c='r', zorder=10, s=6)
    ax.plot(time_trial_filt, pos_filt, c='r', label='Filtered')

    ax.legend()
    ax.set_xlabel('Time')
    ax.set_ylabel('Head pos')

    ax.set_title(tid)