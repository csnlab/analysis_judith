import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from utils import *
from constants import *

session_id = SESSION_IDS[0]
binned_data_settings = 'mar27_allunits'

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)

session = Session(session_id=session_id)
session.load_data()
session.load_binned_data(binned_data_settings=binned_data_settings,
                         bad_units=bad_units)

print(session.binned_data['unit_indx_per_area']['CA1'])

session.align_event_times(align_event=binned_data_pars['align_event'],
                          convert_to_seconds=True)
session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

trial_ids = session.tf.index

features = 'CA1'


if np.isin(features, AREAS) :
    unit_indx = session.binned_data['unit_indx_per_area'][features]
    binned_data = session.binned_data['binned_spikes']
    binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

elif features == 'all_areas' :
    binned_data = session.binned_data['binned_spikes']
    unit_indx = session.binned_data['unit_indx']
    binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

    print(binned_data[0].shape)


