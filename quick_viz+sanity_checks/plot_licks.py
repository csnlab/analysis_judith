import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

# TIME PARAMETERS
align_event = 'PokeC_off'
time_before_in_s = 8
time_after_in_s = 8
binsize_in_ms = 300
slide_by_in_ms = 100
sliding_window = True
max_reaction_time = 5


plot_format = 'png'

plots_folder = os.path.join(DATA_PATH, 'plots', 'licks_sanity_check')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for session_id in SESSION_IDS:
    session = Session(session_id=session_id)
    session.load_data(generate_trackerdata_filtered=True)
    session.align_event_times(align_event=align_event,
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)

    # --- SELECT TRIALS ---
    tf = session.tf
    trial_ids = tf.index

    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
        trial_ids=trial_ids,
        align_event=align_event,
        time_before_in_s=time_before_in_s,
        time_after_in_s=time_after_in_s)

    binned_trackerdata, bin_centers, bin_edges = session.bin_trackerdata_per_trial(
        binsize=binsize_in_ms * pq.ms,
        trial_starts=trial_starts,
        trial_ends=trial_ends,
        sliding_window=sliding_window,
        slide_by=slide_by_in_ms * pq.ms)

    binned_licks, bin_centers, bin_edges = session.bin_licks_per_trial(
        binsize=binsize_in_ms * pq.ms,
        trial_starts=trial_starts,
        trial_ends=trial_ends,
        sliding_window=sliding_window,
        slide_by=slide_by_in_ms * pq.ms,
        binarize=True)

    bc_aligned, be_aligned = session.get_aligned_bins(
        spike_bin_centers=bin_centers,
        spike_bin_edges=bin_edges,
        align_event=align_event_times)
    bc_aligned = bc_aligned.rescale(pq.s)


    binned_trackerdata = {t : s for t, s in zip(trial_ids, binned_trackerdata)}
    binned_licks = {t : s for t, s in zip(trial_ids, binned_licks)}

    binned_licks_binarized = {}
    for t in trial_ids:
        blb = binned_licks[t].copy()
        blb[blb>0] = 1
        binned_licks_binarized[t] = blb

    trial_ids_sel = np.random.choice(trial_ids, size=20, replace=False)

    for t in trial_ids_sel:
        f, ax = plt.subplots(4, 1, figsize=[4, 5], sharex=True)

        x = binned_trackerdata[t][0, :]
        y = binned_trackerdata[t][1, :]
        l = binned_licks[t][0, :]
        lb = binned_licks_binarized[t][0, :]

        ax[0].plot(bc_aligned, x, c='k')
        ax[1].plot(bc_aligned, y, c='k')
        ax[2].plot(bc_aligned, l, c='k')
        ax[2].scatter(bc_aligned, l, c='k', s=6)
        ax[3].plot(bc_aligned, lb, c='k')
        ax[3].scatter(bc_aligned, lb, c='k',s=6)


        for i in range(4):
            ax[i].axvline(0, c='grey', ls=':', label='PokeC_off')
            ax[i].axvline(session.tf.loc[t, 'first_poke_time_aligned'], c='grey',
                          ls='--', label='First poke')
        ax[0].legend()
        ax[0].set_ylabel('Head x-pos')
        ax[1].set_ylabel('Head y-pos')
        ax[2].set_ylabel('Licks')
        ax[3].set_ylabel('Licks (binarized)')

        ax[0].set_title('{} - correct: {}'.format(t, tf.loc[t, 'correct']))
        sns.despine()
        plt.tight_layout()

        plot_name = 'licks_sanity_check_{}_{}.{}'.format(session_id, t, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()
