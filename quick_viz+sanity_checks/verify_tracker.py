import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from scipy.signal import medfilt

for session_id in [SESSION_IDS[1]]:
    session = Session(session_id=session_id, data_version='data_V6_22_03')
    session.load_data(trial_preprocessing=True)

    tf = session.tf
    uf = session.uf

    tracker_names = ['head', 'neck_middle', 'tail']
    tracking = session.trackerdata
    time = tracking['time']
    time_axis = time[pd.notna(time)]
    print(np.diff(time_axis).max())

    t1 = session.trackerdata_filtered
    t2 = session.generate_trackerdata_filtered()

    f, ax = plt.subplots(1, 1)

    ax.plot(t1['time'][0:100000], t1['head']['x'][0:100000], c='k', lw=5)
    ax.plot(t2['time'][0:100000], t2['head']['x'][0:100000], c='r')


for session_id in SESSION_IDS:
    session = Session(session_id=session_id,  data_version='data_V6_22_03')
    session.load_data(trial_preprocessing=True)

    tf = session.tf
    uf = session.uf

    tracker_names = ['head', 'neck_middle', 'tail']
    tracking = session.trackerdata
    time = tracking['time']
    time_axis = time[pd.notna(time)]
    print(np.diff(time_axis).max())




f, ax = plt.subplots(1, 1)
ax.scatter(np.arange(len(time)), time)

tracking_filt = session.trackerdata_filtered
# f, ax = plt.subplots(1, 1)
# sns.distplot(np.diff(tracking_filt['time']), ax=ax, hist=True)


time_axis = time[pd.notna(time)]
frame_period = 40 # frame rate of 40 ms
new_time_axis = np.arange(time_axis[0], time_axis[-1], frame_period)

time_axis[1:]-time_axis[0:-1]

f, ax = plt.subplots(1, 1)
sns.distplot(time_axis, hist=True, ax=ax)

a = np.unique(np.diff(time_axis))

for name in tracker_names:
    x = tracking[name]['x']
    y = tracking[name]['y']
    l = tracking[name]['likelihood']



    valid_idx = np.where((pd.notna(time)) & (l >= 0.9))[0]

    int_x = np.interp(x=time_axis, xp=time[valid_idx], fp=x[valid_idx])
    int_y = np.interp(x=time_axis, xp=time[valid_idx], fp=y[valid_idx])

    x_filt = medfilt(int_x, 5)
    y_filt = medfilt(int_y, 5)
