import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder

session_id = 'R4_S01'
session = Session(session_id=session_id,  data_version=DATA_VERSION)
session.load_data()
session.align_event_times(align_event='PokeC_off', convert_to_seconds=True)
session.filter_trials(max_reaction_time=5)
tf = session.tf

tf = tf[np.isin(tf['first_poke_location'], [4, 5])]
trial_ids = tf.index

align_event = 'PokeC_off'
time_before_in_s = 7
time_after_in_s = 5.5
binsize_in_ms = 400
slide_by_in_ms = 100
sliding_window = True

trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
    trial_ids=tf.index,
    align_event=align_event,
    time_before_in_s=time_before_in_s,
    time_after_in_s=time_after_in_s)


bins, spike_bin_centers = session._make_bins(binsize=binsize_in_ms*pq.ms,
                                          t_start=trial_starts[0],
                                          t_stop=trial_ends[0],
                                          slide_by=slide_by_in_ms*pq.ms)


num_bins = len(bins)  # Number of bins
binned_licks = np.zeros([1, num_bins], dtype=int)


lickon_labels = ['Lick{}_on'.format(l) for l in ['C']+list(range(1, 9))]

licktimes = np.hstack([session.raw_events[l] for l in lickon_labels])

for t, bin in enumerate(bins) :
    binned_licks[0, t] = np.histogram(licktimes, bin)[0]


negative_vals = (binned_licks < 0).sum()
if negative_vals > 0 :
    raise ValueError

spike_bin_centers = np.array(spike_bin_centers) * pq.ms
spike_bin_edges = [b * pq.ms for b in bins]


# binned_spikes, spike_bin_centers, spike_bin_edges = session.bin_spikes_per_trial(
#     binsize=binsize_in_ms * pq.ms,
#     trial_starts=trial_starts,
#     trial_ends=trial_ends,
#     sliding_window=sliding_window,
#     slide_by=slide_by_in_ms * pq.ms)

