import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from utils import *

SESSION_IDS = get_preprocessed_sessions()

binsize_firing_rate_global = 5 * pq.s

unit_id = 'uid_R4_S02_012'
session_id = 'R4_S02'


session = Session(session_id=session_id, data_version=DATA_VERSION)
session.load_data(trial_preprocessing=True, bad_units=None)


binned_spikes_all, spike_bin_centers, spike_bin_edges = session._bin_spikes(
    binsize=binsize_firing_rate_global,
    t_start=session.first_spike_time,
    t_stop=session.last_spike_time,
    unit_ids=[unit_id])

binned_td, td_bin_centers, td_bin_edges = session._bin_trackerdata(
    binsize=binsize_firing_rate_global,
    t_start=session.first_spike_time,
    t_stop=session.last_spike_time)


mask1 = spike_bin_centers > session.task_start_time
mask2 = spike_bin_centers < session.task_end_time
mask = np.logical_and(mask1, mask2)
binned_spikes_all = binned_spikes_all[0, mask]
binned_td = binned_td[:, mask]

x_indx = session.trackerdata_binned_names.index('neck_middle_x')
y_indx = session.trackerdata_binned_names.index('neck_middle_y')

xpos = binned_td[x_indx, :]
ypos = binned_td[y_indx, :]

xy = np.hstack((xpos[:, None], ypos[:, None]))
a = xy[:-1]
b = np.roll(xy, -1, axis=0)[:-1]
dxy = np.linalg.norm(a - b, axis=1)
speed = dxy*PIXELS_TO_CM*pq.cm / binsize_firing_rate_global


spike_bin_centers = spike_bin_centers.rescale(pq.min)
td_bin_centers = td_bin_centers.rescale(pq.min)

firing_rate = binned_spikes_all / binsize_firing_rate_global


f, ax = plt.subplots(1, 1, figsize=[4, 4])
ax.scatter(speed, firing_rate[1:], c='k' ,s=4)
ax.set_ylabel('Firing rate (Hz)')
ax.set_xlabel('Running speed (cm/s)')
ax.set_ylabel('Firing rate (Hz)')
ax.set_xlabel('Running speed (cm/s)')
ax.set_title('{} - {} - {}'.format(unit_id, session.uf.loc[unit_id, 'RecArea'],
                                  session.treatment))
ax.legend()
sns.despine()
plt.tight_layout()



# ----
x_indx = session.trackerdata_binned_names.index('vel_neck_middle_x')
y_indx = session.trackerdata_binned_names.index('vel_neck_middle_y')

vel_x = binned_td[x_indx, :]
vel_y = binned_td[y_indx, :]


spike_bin_centers = spike_bin_centers.rescale(pq.min)
td_bin_centers = td_bin_centers.rescale(pq.min)

firing_rate = binned_spikes_all / binsize_firing_rate_global


f, ax = plt.subplots(1, 1, figsize=[4, 4])
ax.scatter(vel_x, firing_rate, c='r' ,s=4, label='velocity (x-direction)')
ax.scatter(vel_y, firing_rate, c='b' ,s=4, label='velocity (y-direction)')
ax.set_ylabel('Firing rate (Hz)')
ax.set_xlabel('Running speed (cm/s)')
ax.set_title('{} - {} - {}'.format(unit_id, session.uf.loc[unit_id, 'RecArea'],
                                  session.treatment))
ax.legend()
sns.despine()
plt.tight_layout()
