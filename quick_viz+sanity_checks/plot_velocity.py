import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

# TIME PARAMETERS
align_event = 'PokeC_off'
time_before_in_s = 2
time_after_in_s = 2
binsize_in_ms = 100
slide_by_in_ms = 25
sliding_window = True
max_reaction_time = 5


plot_format = 'png'

plots_folder = os.path.join(DATA_PATH, 'plots', 'speed_sanity_check')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for session_id in [SESSION_IDS[0]]:
    session = Session(session_id=session_id)
    session.load_data(generate_trackerdata_filtered=True,
                      compute_velocity=True)

    session.align_event_times(align_event=align_event,
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)

    # --- SELECT TRIALS ---
    tf = session.tf
    trial_ids = tf.index

    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
        trial_ids=trial_ids,
        align_event=align_event,
        time_before_in_s=time_before_in_s,
        time_after_in_s=time_after_in_s)

    binned_trackerdata, bin_centers, bin_edges = session.bin_trackerdata_per_trial(
        binsize=binsize_in_ms * pq.ms,
        trial_starts=trial_starts,
        trial_ends=trial_ends,
        sliding_window=sliding_window,
        slide_by=slide_by_in_ms * pq.ms)

    binned_licks, bin_centers, bin_edges = session.bin_licks_per_trial(
        binsize=binsize_in_ms * pq.ms,
        trial_starts=trial_starts,
        trial_ends=trial_ends,
        sliding_window=sliding_window,
        slide_by=slide_by_in_ms * pq.ms,
        binarize=False)

    bc_aligned, be_aligned = session.get_aligned_bins(
        spike_bin_centers=bin_centers,
        spike_bin_edges=bin_edges,
        align_event=align_event_times)
    bc_aligned = bc_aligned.rescale(pq.s)


    binned_trackerdata = {t : s for t, s in zip(trial_ids, binned_trackerdata)}
    binned_licks = {t : s for t, s in zip(trial_ids, binned_licks)}

    binned_licks_binarized = {}
    for t in trial_ids:
        blb = binned_licks[t].copy()
        blb[blb>0] = 1
        binned_licks_binarized[t] = blb


    time = session.trackerdata_filtered['time']
    pos = session.trackerdata_filtered['neck_middle']['x']
    vel = session.trackerdata_filtered['vel_neck_middle']['x']
    f, ax = plt.subplots(1, 1)
    t1 = 20000
    t2 = 50000
    ax.plot(time[t1:t2], (pos[t1:t2]-pos[t1:t2].mean())/20)
    ax.plot(time[t1:t2], vel[t1:t2], c='red')
    ax.axhline(0)


    trial_ids_sel = np.random.choice(trial_ids, size=10, replace=False)

    for t in trial_ids_sel:
        f, ax = plt.subplots(4, 1, figsize=[4, 5], sharex=True)

        x = binned_trackerdata[t][session.trackerdata_binned_names.index('head_x'), :]
        vx = binned_trackerdata[t][session.trackerdata_binned_names.index('vel_head_x'), :]
        y = binned_trackerdata[t][session.trackerdata_binned_names.index('head_y'), :]
        vy = binned_trackerdata[t][session.trackerdata_binned_names.index('vel_head_y'), :]

        l = binned_licks[t][0, :]
        lb = binned_licks_binarized[t][0, :]

        ax[0].plot(bc_aligned, x, c='k')
        ax[1].plot(bc_aligned, vx, c='k')
        ax[2].plot(bc_aligned, y, c='k')
        ax[3].plot(bc_aligned, vy, c='k')

        for i in range(4):
            ax[i].axvline(0, c='grey', ls=':', label='PokeC_off')
            ax[i].axvline(session.tf.loc[t, 'first_poke_time_aligned'], c='grey',
                          ls='--', label='First poke')
        ax[0].legend()
        ax[0].set_ylabel('Head x-pos')
        ax[1].set_ylabel('Vel. head x-pos')
        ax[2].set_ylabel('Head y-pos')
        ax[3].set_ylabel('Vel. head y-pos')

        ax[0].set_title('{} - correct: {}'.format(t, tf.loc[t, 'correct']))
        sns.despine()
        plt.tight_layout()

        plot_name = 'speed_sanity_check_{}_{}.{}'.format(session_id, t, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

        plt.close()
