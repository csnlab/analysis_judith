import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import matplotlib
from plotting_style import *

settings_name = 'mar26'
plot_format = 'png'

smooth_sigma = 1.4
spatial_bin_size = 23 # approximately 3 cm as in rat robot
occupancy_threshold_in_s = 0
#min_spikes_bin = 3
x_max_global = 576
y_max_global = 720

align_event = 'PokeC_off'
max_reaction_time = 5

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'ratemaps_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for session_id in [SESSION_IDS[0]]:


    session = Session(session_id=session_id, data_version=DATA_VERSION)
    session.load_data()
    session.align_event_times(align_event=align_event, convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)
    uf = session.uf
    tf = session.tf

    trial_ids = tf[tf['first_poke_location'] == 2].index

    occupancy = session.compute_spatial_occupancy(spatial_bin_size=spatial_bin_size,
                                                  x_max=x_max_global,
                                                  y_max=y_max_global,
                                                  trial_ids=trial_ids,
                                                  smooth_sigma=smooth_sigma)
    #occupancy[occupancy < occupancy_threshold] = 0

    occupancy[occupancy < occupancy_threshold_in_s * pq.s] = 0 * pq.s
    occupancy = np.ma.masked_where(occupancy == 0, occupancy)
    cmap = rasters_cmap
    cmap.set_bad(color='white')

    x_pos = session.trackerdata_filtered['head']['x']
    y_pos = session.trackerdata_filtered['head']['y']

    x_max_crop = x_pos.max()
    y_max_crop = y_pos.max()

    x_min = x_pos[x_pos>0].min()
    y_min = y_pos[y_pos>0].min()

    f, ax = plt.subplots(1, 1)

    ax.scatter(x_pos[::100], y_pos[::100], c='red', s=2)
    #ax.plot(x, y)
    # for l in list(session.poke_locations)+['C']:
    #     x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
    #     y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
    #     ax.text(x-30, y, l)
    #
    #     ax.scatter(x, y, c=location_palette[l], zorder=10)

    ax.imshow(np.flipup(occupancy.T), extent=[0, x_max_global, 0, y_max_crop], cmap=cmap, alpha=0.5,
              vmin=0)

    # --- PLOT OCCUPANCIES ----------------


    for unit_id in uf.iloc[0:1].index:

        spikes_mat = session.compute_spatial_firing(unit_id=unit_id,
                                                    x_max=x_max_global,
                                                    y_max=y_max_global,
                                                    spatial_bin_size=spatial_bin_size,
                                                    trial_ids=None,
                                                    smooth_sigma=smooth_sigma)
        ratemap = spikes_mat / occupancy

        # --- PLOT ---
        cmap = matplotlib.cm.jet  # Can be any colormap that you want after the cm
        cmap.set_bad(color='white')

        f, ax = plt.subplots(1,  3, figsize=[10, 3],  sharex=True, sharey=True)

        spikes_mat = np.ma.masked_where(spikes_mat == 0, spikes_mat)
        ax[0].imshow(spikes_mat.T, extent=[0, x_max_crop, 0, y_max_crop], cmap=cmap)
        ax[0].set_xticks([0, x_max_crop])
        ax[0].set_yticks([0, y_max_crop])

        for l in session.poke_locations :
            x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
            y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
            ax[0].scatter(x, y, c=location_palette[l])

        occupancy = np.ma.masked_where(occupancy == 0, occupancy)
        ax[1].imshow(occupancy.T, extent=[0, x_max_crop, 0, y_max_crop], cmap=cmap)
        ax[1].set_xticks([0, x_max_crop])
        ax[1].set_yticks([0, y_max_crop])

        ax[2].imshow(ratemap.T, extent=[0, x_max_crop, 0, y_max_crop], cmap='jet')
        ax[2].set_xticks([0, x_max_crop])
        ax[2].set_yticks([0, y_max_crop])

        ax[0].set_title('Spikes')
        ax[1].set_title('Occupancy')
        ax[2].set_title('Ratemap')

        sns.despine(bottom=True, left=True)
        ax[0].axis('off')
        ax[1].axis('off')
        ax[2].axis('off')

        plt.tight_layout()

        plot_name = 'ratemap_panels_{}_{}.{}'.format(settings_name, unit_id, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
        plt.close()

