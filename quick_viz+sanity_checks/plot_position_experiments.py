import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *
import matplotlib.pyplot as plt
import matplotlib
import seaborn as sns

sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))

settings_name = 'mar21'

#combos = ['first_poke_location']
combos = ['first_poke_loc_previous_trial']
combos = ['separation']


area = 'CA1'
min_trials_per_class = 10
min_units = 10
# TIME PARAMETERS
align_event = 'stimulus_on'
time_before_in_s = 6
time_after_in_s = 6
binsize_in_ms = 500
slide_by_in_ms = 250
sliding_window = True
max_reaction_time = 5

plot_format = 'png'

plots_folder = os.path.join(DATA_PATH, 'plots', 'position', '{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

experiment = 'separation'

sdf = select_data_experiment(experiment=experiment,
                             session_ids=SESSION_IDS,
                             max_reaction_time=max_reaction_time,
                             filter_trials=True)

sdf_sel = sdf[(sdf['n0'] >= min_trials_per_class) &
              (sdf['n1'] >= min_trials_per_class)]

selected_session_ids = sdf_sel.index


for session_id in selected_session_ids:

    session = Session(session_id=session_id)
    session.load_data()
    session.align_event_times(align_event=align_event,
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)

    # --- SELECT TRIALS ---
    tf_sel, target, n0, n1 = select_trials_experiment(experiment=experiment,
                                                 tf=session.tf)
    trial_ids = tf_sel.index


    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
                                           trial_ids=trial_ids,
                                           align_event=align_event,
                                           time_before_in_s=time_before_in_s,
                                           time_after_in_s=time_after_in_s)


    binned_trackerdata, bin_centers, bin_edges = session.bin_trackerdata_per_trial(
                                       binsize=binsize_in_ms * pq.ms,
                                       trial_starts=trial_starts,
                                       trial_ends=trial_ends,
                                       sliding_window=sliding_window,
                                       slide_by=slide_by_in_ms * pq.ms)

    bc_aligned, be_aligned = session.get_aligned_bins(
            spike_bin_centers=bin_centers,
            spike_bin_edges=bin_edges,
            align_event=align_event_times)

    binned_trackerdata = {t : s for t, s in zip(trial_ids, binned_trackerdata)}


    cmap_0 = matplotlib.cm.get_cmap('winter')
    cmap_1 = matplotlib.cm.get_cmap('spring')

    # f, ax = plt.subplots(1, 1)
    # for tid, tval in zip(trial_ids, target):
    #     x = binned_trackerdata[tid][session.trackerdata_labels.index('head_x')]
    #     y = binned_trackerdata[tid][session.trackerdata_labels.index('head_y')]
    #     n = len(x)
    #     lin = np.linspace(0, 1, n)
    #     if tval == 0:
    #         for i, (xx, yy) in enumerate(zip(x, y)):
    #             ax.scatter(xx, yy, color=cmap_0(lin[i]))
    #
    #     elif tval == 1:
    #         for i, (xx, yy) in enumerate(zip(x, y)):
    #             ax.scatter(xx, yy, color=cmap_1(lin[i]))


    f, axes = plt.subplots(2, 3, figsize=[10,8])
    ax_indx = [(0, 0), (1, 0), (0, 1), (1, 1), (0, 2), (1, 2)]
    palette = {0 : sns.xkcd_rgb['teal'], 1 : sns.xkcd_rgb['pumpkin orange']}
    for i, label in enumerate(session.trackerdata_labels):
        ax = axes[ax_indx[i]]
        dp = pd.DataFrame(columns=['time', 'pos', 'target', 'tid'])
        for tid, tval in zip(trial_ids, target) :
            x = binned_trackerdata[tid][session.trackerdata_labels.index(label)]
            for t, xx in zip(bc_aligned, x):
                dp.loc[dp.shape[0], :] = [t.item(), xx, tval, tid]
        # sns.lineplot(data=dp, x='time', y='pos', hue='target', ax=ax,
        #              palette=palette)
        #
        sns.lineplot(data=dp, x='time', y='pos', hue='target', ax=ax,
                     palette=palette, units='tid', estimator=None)

        ax.set_title(label)
        ax.legend().remove()
    sns.despine()
    plt.tight_layout()

    plot_name = 'position_over_time_{}_{}.{}'.format(experiment,
                                                     session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()


