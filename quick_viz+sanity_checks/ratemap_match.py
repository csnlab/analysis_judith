import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter
import matplotlib
from plotting_style import *

settings_name = 'mar26'
plot_format = 'png'

smooth_sigma = 1.4
spatial_bin_size = 23 # approximately 3 cm as in rat robot
occupancy_threshold_in_s = 0
#min_spikes_bin = 3
x_max_global = 576
y_max_global = 720

align_event = 'PokeC_off'
max_reaction_time = 5

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'ratemaps_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for session_id in [SESSION_IDS[0]]:


    session = Session(session_id=session_id, data_version=DATA_VERSION)
    session.load_data()
    session.align_event_times(align_event=align_event, convert_to_seconds=True)
    session.filter_trials(max_reaction_time=max_reaction_time)
    uf = session.uf
    tf = session.tf
    cmap = rasters_cmap

    trial_ids = tf[tf['first_poke_location'] == 2].index

    pos_time = session.trackerdata_filtered['time'].copy()
    pos_head_x = session.trackerdata_filtered['head']['x'].copy()
    pos_head_y = session.trackerdata_filtered['head']['y'].copy()
    tf = session.tf

    # --- REMOVE NANS ---
    mask = ~np.isnan(pos_time)
    pos_time = pos_time[mask]
    pos_head_x = pos_head_x[mask]
    pos_head_y = pos_head_y[mask]

    x_bins = np.arange(0, x_max_global, spatial_bin_size)
    y_bins = np.arange(0, y_max_global, spatial_bin_size)
    x_binned = np.digitize(pos_head_x, bins=x_bins)-1
    y_binned = np.digitize(pos_head_y, bins=y_bins)-1

    pos_time_all = []
    x_binned_all, y_binned_all = [], []
    xpos_all, ypos_all = [], []
    for trial_id, row in tf.loc[trial_ids].iterrows() :
        tstart, tend = row['trial_start'], row['trial_end']
        mask = np.logical_and(pos_time >= tstart, pos_time <= tend)
        pos_time_all.append(pos_time[mask])
        x_binned_all.append(x_binned[mask])
        y_binned_all.append(y_binned[mask])
        xpos_all.append(pos_head_x[mask])
        ypos_all.append(pos_head_y[mask])
    pos_time = np.hstack(pos_time_all)
    x_binned = np.hstack(x_binned_all)
    y_binned = np.hstack(y_binned_all)

    xpos = np.hstack(xpos_all)
    ypos = np.hstack(ypos_all)

    occupancy = np.zeros(shape=[len(x_bins), len(y_bins)])
    for i, t in enumerate(pos_time) :
        occupancy[x_binned[i], y_binned[i]] += session.frame_period.rescale \
            (pq.s).item()

    #occupancy = gaussian_filter(occupancy, sigma=smooth_sigma)


    f, ax = plt.subplots(1, 1, figsize=[20, 20])

    ax.scatter(xpos, ypos, c='red', s=2)
    for l in list(session.poke_locations)+['C']:
        x = session.misc['video_points'].loc['Poke{}'.format(l), 'x']
        y = session.misc['video_points'].loc['Poke{}'.format(l), 'y']
        ax.text(x-30, y, l)

        ax.scatter(x, y, c=location_palette[l], zorder=10)

    ax.imshow(occupancy.T, extent=[0+spatial_bin_size/2,
                                   x_max_global+spatial_bin_size/2,
                                   0+spatial_bin_size/2,
                                   y_max_global+spatial_bin_size/2], cmap=cmap, alpha=0.5,
              vmin=0, origin='lower')
