import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn  as sns
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

# TODO add left vs right sorted by block performance


sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))

settings_name = 'mar16_smooth'
plot_format = 'png'

smooth_sigma = 1
spatial_bin_size = 10
automatic_limits = True
x_max = 576
y_max = 720

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'ratemaps_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)




for session_id in ['sid_20210723']:



    session = Session(session_id=session_id, data_version='data_mar9_realigned')
    session.load_data()
    trial_ids = session.tf.index


    pos_time = session.trackerdata_filtered['time']
    pos_head_x = session.trackerdata_filtered['head']['x']
    pos_head_y = session.trackerdata_filtered['head']['y']

    # --- REMOVE NANS ---
    mask = ~np.isnan(pos_time)
    pos_time = pos_time[mask]
    pos_head_x = pos_head_x[mask]
    pos_head_y = pos_head_y[mask]

    # --- BIN ---
    if automatic_limits:
        x_max = np.max(pos_head_x+1)
        y_max = np.max(pos_head_y+1)
    x_bins = np.arange(0, x_max, spatial_bin_size)
    y_bins = np.arange(0, y_max, spatial_bin_size)
    x_binned = np.digitize(pos_head_x, bins=x_bins) - 1
    y_binned = np.digitize(pos_head_y, bins=y_bins) - 1

    # --- SELECT TRIALS ---
    if trial_ids == 'all':
        # restrict to portion of the task
        first_trial = tf['trial_start'].iloc[0]
        last_trial = tf['trial_end'].iloc[-1]
        mask = np.logical_and(pos_time >= first_trial, pos_time <= last_trial)
        pos_time = pos_time[mask]
        x_binned = x_binned[mask]
        y_binned = y_binned[mask]
    else:
        pos_time_all = []
        x_binned_all, y_binned_all = [], []
        for trial_id, row in session.tf.loc[trial_ids].iterrows():
            tstart, tend = row['trial_start'], row['trial_end']
            mask = np.logical_and(pos_time >= tstart, pos_time <= tend)
            pos_time_all.append(pos_time[mask])
            x_binned_all.append(x_binned[mask])
            y_binned_all.append(y_binned[mask])
        pos_time = np.hstack(pos_time_all)
        x_binned = np.hstack(x_binned_all)
        y_binned = np.hstack(y_binned_all)

    occupancy = np.zeros(shape=[len(x_bins), len(y_bins)])
    for i, t in enumerate(pos_time) :
        occupancy[x_binned[i], y_binned[i]] += 1

    occupancy = gaussian_filter(occupancy, sigma=smooth_sigma)





    uf = session.uf
    tf = session.tf

    all_spikes = np.hstack([session.spiketrains[u] for u in session.spiketrains.keys()])
    first_spike = all_spikes.min()
    last_spike = all_spikes.max()

    first_trial = tf['trial_start'].iloc[0] - 3000
    last_trial = tf['trial_end'].iloc[-1] + 3000

    pos_time = session.trackerdata_filtered['time']
    pos_head_x = session.trackerdata_filtered['head']['x']
    pos_head_y = session.trackerdata_filtered['head']['y']

    # exclude nans
    mask = ~np.isnan(pos_time)
    pos_time = pos_time[mask]
    pos_head_x = pos_head_x[mask]
    pos_head_y = pos_head_y[mask]

    # restrict to portion of the task
    mask2 = np.logical_and(pos_time>=first_trial, pos_time<=last_trial)
    pos_time = pos_time[mask2]
    pos_head_x = pos_head_x[mask2]
    pos_head_y = pos_head_y[mask2]
    if automatic_limits:
        x_max = np.max(pos_head_x+1)
        y_max = np.max(pos_head_y+1)
    x_bins = np.arange(0, x_max, spatial_bin_size)
    y_bins = np.arange(0, y_max, spatial_bin_size)

    x_binned = np.digitize(pos_head_x, bins=x_bins) - 1
    y_binned = np.digitize(pos_head_y, bins=y_bins) - 1

    # compute occupancy #TODO not in any units and assuming fixed period
    occupancy = np.zeros(shape=[len(x_bins), len(y_bins)])
    for i, t in enumerate(pos_time) :
        occupancy[x_binned[i], y_binned[i]] += 1

    #occupancy[occupancy<10] = 0

    occupancy = gaussian_filter(occupancy, sigma=smooth_sigma)

    for unit_id in uf.index:

        spikes = session.spiketrains[unit_id]
        spikes = spikes[spikes>=first_trial]
        spikes = spikes[spikes<=last_trial]

        # add spikes
        spikes_mat = np.zeros(shape=[len(x_bins), len(y_bins)])
        for spike in spikes:
            indx = np.nanargmin(np.abs(pos_time - spike))
            spikes_mat[x_binned[indx], y_binned[indx]] += 1



        # ---------------------------------
        spikes = session.spiketrains[unit_id]
        trial_ids = tf.index
        pos_time = session.trackerdata_filtered['time'].copy()
        pos_head_x = session.trackerdata_filtered['head']['x'].copy()
        pos_head_y = session.trackerdata_filtered['head']['y'].copy()

        # --- SELECT TRIALS ---
        if combo == 'all' :
            # restrict to portion of the task
            first_trial = tf['trial_start'].iloc[0]
            last_trial = tf['trial_end'].iloc[-1]
            spikes = spikes[spikes >= first_trial]
            spikes = spikes[spikes <= last_trial]

        else :
            spikes_all = []
            for trial_id, row in session.tf.loc[trial_ids].iterrows() :
                tstart, tend = row['trial_start'], row['trial_end']
                spikes_all.append(spikes[np.logical_and(spikes >= tstart, spikes <= tend)])
            spikes = np.hstack(spikes_all)

        spikes_mat = np.zeros(shape=[len(x_bins), len(y_bins)])
        for spike in spikes :
            indx = np.nanargmin(np.abs(pos_time - spike))
            spikes_mat[x_binned[indx], y_binned[indx]] += 1
        spikes_mat = gaussian_filter(spikes_mat, sigma=smooth_sigma)

        # ---------------------------------


        # --- SMOOTH ---

        spikes_mat = gaussian_filter(spikes_mat, sigma=smooth_sigma)

        ratemap = spikes_mat / occupancy

        # --- PLOT ONLY SPIKES ---
        f, ax = plt.subplots(1,  1)
        ax.imshow(spikes_mat.T, extent=[0, x_max, 0, y_max], cmap='jet')
        ax.set_xticks([0, x_max])
        ax.set_yticks([0, y_max])
        sns.despine()


        plot_name = 'spikes_simple_{}_{}.{}'.format(settings_name, unit_id, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
        plt.close()

        # ratemap = spikes_mat / occupancy

        # --- PLOT RATEMAP---
        f, ax = plt.subplots(1, 1)
        ax.imshow(ratemap.T, extent=[0, x_max, 0, y_max], cmap='jet')
        ax.set_xticks([0, x_max])
        ax.set_yticks([0, y_max])
        sns.despine()

        plot_name = 'ratemap_simple_{}_{}.{}'.format(settings_name, unit_id, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
        plt.close()

        # pd = np.diff(pos_time)
        # indx = np.where(pd>100)[0]
        #
        # f, ax = plt.subplots(1,  1)
        # for pos in pos_time[indx[0]-20:indx[0]+20]:
        #     ax.axvline(pos, zorder=-10)


        # f, ax = plt.subplots(1, 1)
        # #ax.imshow(occupancy)
        #
        # for spike in spikes[::100]:
        #     plt.plot(np.abs(pos_time - spike))


        # f, ax = plt.subplots(1,  1)
        #
        # ax.axvline(np.nanmin(pos_time), zorder=-10, c='r')
        # ax.axvline(np.nanmax(pos_time), zorder=-10, c='r')
        #
        # ax.axvline(spikes[0], zorder=-10, c='b')
        # ax.axvline(spikes[-1], zorder=-10, c='b')
        #
        #
        #
        # f, ax = plt.subplots(1,  1)
        # ax.plot(pos_time, pos_time, c='r')
        # for spike in spikes:
        #     ax.axvline(spike, zorder=-10)
        #
        #
        # print('Start position time: {}'.format(np.nanmin(pos_time)))
        # print('End position time: {}'.format(np.nanmax(pos_time)))
        # print('First spike: {}'.format(spikes[0]))
        # print('Last spike: {}'.format(spikes[-1]))
        #
        # np.nanmax(pos_time)-spikes[-1]
        # np.min(spikes)

        # for every spike find closest time bin and assign to that spatial bin
