import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder

sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))
session_id = sf['ID'].iloc[0]

session_id = 'sid_20210724'
session = Session(session_id=session_id,  data_version=DATA_VERSION)
session.load_data()
session.align_event_times(align_event='PokeC_off', convert_to_seconds=True)
session.filter_trials(max_reaction_time=5)
tf = session.tf

tf = tf[np.isin(tf['first_poke_location'], [4, 5])]
trial_ids = tf.index

align_event = 'PokeC_off'
time_before_in_s = 7
time_after_in_s = 5.5
binsize_in_ms = 400
slide_by_in_ms = 100
sliding_window = True

trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
    trial_ids=tf.index,
    align_event=align_event,
    time_before_in_s=time_before_in_s,
    time_after_in_s=time_after_in_s)

# binned_spikes, spike_bin_centers, spike_bin_edges = session.bin_spikes_per_trial(
#     binsize=binsize_in_ms * pq.ms,
#     trial_starts=trial_starts,
#     trial_ends=trial_ends,
#     sliding_window=sliding_window,
#     slide_by=slide_by_in_ms * pq.ms)


binned_trackerdata, td_bin_centers, td_bin_edges = session.bin_trackerdata_per_trial(
    binsize=binsize_in_ms * pq.ms,
    trial_starts=trial_starts,
    trial_ends=trial_ends,
    sliding_window=sliding_window,
    slide_by=slide_by_in_ms * pq.ms)




binned_spikes = {t : s for t, s in zip(trial_ids, binned_spikes)}
binned_trackerdata = {t : s for t, s in zip(trial_ids, binned_trackerdata)}


f, ax = plt.subplots(1, 1)

for tid in trial_ids:
    x = binned_trackerdata[tid][session.trackerdata_labels.index('head_x')]
    y = binned_trackerdata[tid][session.trackerdata_labels.index('head_y')]

    if tf.loc[tid, 'first_poke_location'] == 4:
        ax.plot(x, y, c='grey')
        ax.scatter(x[0], y[0], c='green')
        ax.scatter(x[-1], y[-1], c='blue')
        print(x[0], y[0])
        print(x[-1], y[-1])
    elif tf.loc[tid, 'first_poke_location'] == 5:
        ax.plot(x, y, c='grey')
        ax.scatter(x[0], y[0], c='red')
        ax.scatter(x[-1], y[-1], c='yellow')
        print(x[0], y[0])
        print(x[-1], y[-1])




# np.array(spike_bin_centers) - align_event_times[0].item()
#
# np.array(bins) - align_event_times[0].item()
#
#
# keys = ['head', 'neck_middle', 'tail']
# dirs = ['x', 'y']
#
# time = session.trackerdata_filtered['time']
# td = np.vstack([session.trackerdata_filtered[key][dir] for key in keys for dir in dirs])
# labels = ['{}_{}'.format(key, dir) for key in keys for dir in dirs]
#
# num_bins = len(bins)  # Number of bins
# num_trackerpoints = td.shape[0]  # Number of neurons
# binned_td = np.empty([num_trackerpoints, num_bins])
#
#
# for t, bin in enumerate(bins):
#     mask = np.logical_and(time>=bin[0], time<bin[1])
#     binned_td[:, t] = td[:, mask].mean(axis=1)
#
#
# td_bin_centers = np.array(spike_bin_centers) * pq.ms
# td_bin_edges = [b * pq.ms for b in bins]
#
# return binned_spikes, spike_bin_centers, spike_bin_edges