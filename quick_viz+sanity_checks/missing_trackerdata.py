import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from utils import get_preprocessed_sessions

align_event = 'PokeC_off'
time_before_in_s = 10
time_after_in_s = 10
sliding_window = True
max_reaction_time = 5

SESSION_IDS = get_preprocessed_sessions()

for session_id in [SESSION_IDS[0]]:
    session = Session(session_id=session_id)
    session.load_data(generate_trackerdata_filtered=True,
                      compute_velocity=False)

    session.align_event_times(align_event=align_event,
                              convert_to_seconds=True)

    session.trackerdata

    session.filter_trials(max_reaction_time=max_reaction_time)

    # --- SELECT TRIALS ---
    tf = session.tf
    trial_ids = tf.index

    trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
        trial_ids=trial_ids,
        align_event=align_event,
        time_before_in_s=time_before_in_s,
        time_after_in_s=time_after_in_s)


    time = session.trackerdata['time']
    binsize=300*pq.ms
    slide_by=300*pq.ms

    tf['video_gaps_ms'] = 0
    for i, (tid, t_start, t_stop) in enumerate(zip(trial_ids, trial_starts, trial_ends)) :


        bins, bin_centers = session._make_bins(binsize=binsize,
                                            t_start=t_start,
                                            t_stop=t_stop,
                                            slide_by=slide_by)

        exclude = ['SampleNr', 'time']
        cols = [col for col in session.trackerdata.columns if col not in exclude]

        time = session.trackerdata['time'].__array__()
        td = session.trackerdata[cols].__array__().T
        session.trackerdata_binned_names = cols

        num_bins = len(bins)  # Number of bins
        num_trackerpoints = td.shape[0]  # Number of trackerdata variables
        binned_td = np.empty([num_trackerpoints, num_bins])

        for t, bin in enumerate(bins) :
            mask = np.logical_and(time >= bin[0], time < bin[1])
            if mask.sum() == 0:
                warnings.warn('Tracker data missing in bin')
            binned_td[:, t] = td[:, mask].mean(axis=1)

        td_bin_centers = np.array(bin_centers) * pq.ms
        td_bin_edges = [b * pq.ms for b in bins]

        print(np.isnan(binned_td).sum())

        if tid == 'tid_R4_S01_055':
            break

            (time >= t_start) & (time < t_stop)
            time_trial = time[indx]

            frame_gaps = np.diff(time_trial[pd.notna(time_trial)])
            excess_gaps = frame_gaps[frame_gaps>50].sum()

            tf.loc[tid, 'video_gaps_ms'] = excess_gaps

            max_period_filt = np.diff(time_filt_trial[pd.notna(time_filt_trial)]).max()
            if max_period_filt > 50:
                print(max_period_filt)

            missing_frames = len(time_filt_trial) - len(time_trial)
            print(missing_frames)



