import pickle
import os
import numpy as np
from constants import *
import pandas as pd
import quantities as pq
import warnings
#import elephant
from scipy.ndimage import gaussian_filter
from scipy.signal import medfilt

class Session():

    def __init__(self, session_id='', data_path=None, data_version=None,
                 verbose=2, raise_warnings=True):

        # print('\nInitializing Session object for: \n- animal ID: {}'
        #       '\n- Session ID: {}\n'.format(animal_id, session_id))
        if data_path is None:
            self.data_path = DATA_PATH
        else:
            self.data_path = data_path

        if data_version is None:
            self.data_version = DATA_VERSION
        else:
            self.data_version = data_version

        try:
            #self.so = pd.read_csv(os.path.join(self.data_path, self.data_version, 'session_overview.csv'))
            self.so = pd.read_excel(os.path.join(self.data_path, self.data_version,
                                                 'session_overview.xlsx'),
                                    index_col=0)
            self.so.index = self.so['sid']
            self.so = self.so.drop('sid', axis='columns')
            self.animal_id = self.so.loc[session_id]['Animal ID']
            self.treatment = self.so.loc[session_id]['Treatment type']
        except FileNotFoundError:
            warnings.warn('Session overview file not found!')

        self.session_id = session_id
        self.valid_locations = [2, 4, 5, 7]
        self.tracker_names_position = ['BodyPart1', 'BodyPart2', 'BodyPart3',
                                       'BodyPart4', 'BodyPart5', 'BodyPart6',
                                       'MeanBody']
        self.tracer_names_mean_position = 'MeanBody'
        self.tracker_names_speed_dir = ['BodySpeed', 'BodyDir', 'HeadDir']
        self.verbose = verbose
        self.raise_warnings = raise_warnings



    def _load_data(self):
        path = open(os.path.join(self.data_path, self.data_version,
                                 '{}.pickle'.format(self.session_id)), 'rb')
        data = pickle.load(path)
        return data

    def _load_trackerdata(self):
        path = os.path.join(self.data_path, self.data_version, 'trackingdata',
                            '{}.csv'.format(self.session_id))
        trackerdata = pd.read_csv(path)
        # convert time to milliseconds like the spikes
        trackerdata['time'] = trackerdata['Time_seconds']*(10**3)
        trackerdata = trackerdata.drop(columns=['Time_seconds'])
        return trackerdata

    def load_data(self, trial_preprocessing=True,
                  generate_trackerdata_filtered=True,
                  trackerdata_frame_period=None,
                  compute_velocity=False,
                  trackerdata_likelihood_threshold=None,
                  run_checks=False,
                  load_spiketrains=True,
                  bad_units=None):

        data = self._load_data()
        trackerdata = self._load_trackerdata()
        self.data = data
        self.tf = data['trial_df']
        self.uf = data['unit_df']
        self.uf['session_id'] = self.session_id
        self.uf['treatment'] = self.treatment
        self.trackerdata = trackerdata
        #self.trackerdata_filtered = data['trackerdata_filtered']
        self.events = data['events']
        self.raw_events = data['raw_events']
        self.misc = data['misc']
        self.misc['video_points'].index = self.misc['video_points']['name']

        if self.data_version == 'data_V15_06_05_2024':
            self.uf['ClusterGroup'] = self.uf['ClusterGroup'].replace({'GOOD' : 'SUA'})

        if load_spiketrains:
            self.spiketrains = data['spiketrains']

            if bad_units is not None :
                if self.verbose > 1:
                    print('Excluding bad units')
                # for u in bad_units:
                #     print('Excluding unit {}'.format(u))

                good_units = [u for u in self.uf.index if u not in bad_units]
                self.spiketrains = {u : self.spiketrains[u] for u in good_units}
                self.uf = self.uf.loc[good_units, :]

            self.first_spike_time = np.hstack([self.spiketrains[u] for u in
                                               self.uf.index]).min() * pq.ms
            self.last_spike_time = np.hstack([self.spiketrains[u] for u in
                                              self.uf.index]).max() * pq.ms

        # we load trackerdata already processed, so skip this
        # if trackerdata_likelihood_threshold is None:
        #     trackerdata_likelihood_threshold = 0.9
        # self.trackerdata_likelihood_threshold = trackerdata_likelihood_threshold
        #
        # if generate_trackerdata_filtered:
        #     if self.verbose > 1 :
        #         print('Regenerating trackerdata_filtered with correct time axis')
        #     self.trackerdata_filtered = self.generate_trackerdata_filtered(frame_period_in_ms=trackerdata_frame_period)
        # else:
        #     pass
        #     # raise ValueError('If you do no regenerate the trackerdata'
        #     #                  ' the time period is irregular')

        if trial_preprocessing:
            self.tf['session_id'] = self.session_id
            self.add_previous_response_location_to_trial_frame()
            self.add_stimulus_onset_to_trial_frame()
            #self.add_separation_to_trial_frame()
            self.add_poke_times_to_trial_frame()
            self.add_block_performance_indicators_to_trial_frame()
            self.add_last_central_poke_time_to_trial_frame()
            self.remove_trials_with_no_response()
            self.remove_trials_with_no_n_initiations()
            self.remove_trials_with_no_PokeC_off()
            self.add_previous_first_poke_location_to_trial_frame()
            self.cast_floats_to_integers()
            self.add_first_poke_side()
            self.add_previous_poke_side()

            # if the tracked position falls outside the task box we have nans
            # we identify and remove the few trials in which we have nans in the position
            self.add_gaps_in_trackerdata_to_trial_frame_and_remove()

        if compute_velocity:
            self.compute_velocity()

        self.add_global_task_times()

        if run_checks:
            # make sure ids in the spike trains and in the dataframe match
            unit_ids_trains = list(self.spiketrains.keys())
            unit_ids_df = self.uf.index
            np.testing.assert_array_equal(unit_ids_trains, unit_ids_df)

        self.bintd_exclude = ['BodyPart1_X',
                           'BodyPart1_Y',
                           'BodyPart2_X',
                           'BodyPart2_Y',
                           'BodyPart3_X',
                           'BodyPart3_Y',
                           'BodyPart4_X',
                           'BodyPart4_Y',
                           'BodyPart5_X',
                           'BodyPart5_Y',
                           'SampleNr',
                           'time',
                           'HeadDir']

        cols = [col for col in self.trackerdata.columns if col not in self.bintd_exclude]
        self.trackerdata_binned_names = cols


    def load_binned_data(self, binned_data_settings,
                         bad_units=None):

        binned_data_folder = os.path.join(self.data_path, 'binned_data',
                                          self.data_version, binned_data_settings)
        binned_data_file_name = 'binned_{}_{}.pkl'.format(self.session_id,
                                                          binned_data_settings)
        binned_data_full_path = os.path.join(binned_data_folder,
                                             binned_data_file_name)
        self.binned_data = pickle.load(open(binned_data_full_path, 'rb'))

        if bad_units is not None:
            if self.verbose > 1 :
                print('Excluding bad units')
            unit_ids = self.binned_data['unit_ids']
            try :
                self.binned_data['unit_indx']
            except :
                self.binned_data['unit_indx'] = np.arange(len(unit_ids))
            unit_indx = self.binned_data['unit_indx']
            #print(len(unit_indx))
            mask = ~np.isin(unit_ids, bad_units)
            self.binned_data['unit_ids'] = unit_ids[mask]
            self.binned_data['unit_indx'] = unit_indx[mask]
            #print(len(self.binned_data['unit_indx']))

            areas = list(self.binned_data['unit_ids_per_area'].keys())

            for area in areas :
                ids = self.binned_data['unit_ids_per_area'][area]
                indx = self.binned_data['unit_indx_per_area'][area]
                #print(len(ids))

                mask = ~np.isin(ids, bad_units)
                self.binned_data['unit_ids_per_area'][area] = ids[mask]
                self.binned_data['unit_indx_per_area'][area] = indx[mask]
                #ids = self.binned_data['unit_ids_per_area'][area]
                #print(len(ids))

    def add_global_task_times(self):
        for i in range(self.tf.shape[0] - 1) :
            if self.tf.iloc[i]['separation'] != self.tf.iloc[i + 1]['separation']:
                self.block_switch_time = self.tf.iloc[i]['trial_start'] * pq.ms

        self.task_start_time = self.tf.iloc[0]['trial_start'] * pq.ms
        self.task_end_time = self.tf.iloc[-1]['trial_end'] * pq.ms


    def add_gaps_in_trackerdata_to_trial_frame_and_remove(self):

        time = self.trackerdata['time']
        trial_ids = self.tf.index

        # if we want to use head direction, we should add here HeadDir,
        # so we can identify and exclude trials in which it is nan, and remove it
        # from bin_trackerdata (so that we bin it when we run bin_data.py)
        tracker_names = ['MeanBody_X', 'MeanBody_Y']

        # we look 10 seconds around PokeC_off, but can be extended
        align_event = 'stimulus_on'
        time_before_in_s = 10
        time_after_in_s = 20

        trial_starts, trial_ends, align_event_times = self.get_aligned_epoch_times(
            trial_ids=trial_ids,
            align_event=align_event,
            time_before_in_s=time_before_in_s,
            time_after_in_s=time_after_in_s)

        for tracker_name in tracker_names:
            self.tf['perc_coverage_{}'.format(tracker_name)] = 0

        for i, (tid, t_start, t_stop) in enumerate(zip(trial_ids, trial_starts, trial_ends)) :
            time_mask = np.logical_and(time >= t_start, time < t_stop)
            for tracker_name in tracker_names:
                tracking = self.trackerdata[tracker_name]
                tracking_trial = tracking[time_mask]
                perc_points = 100 * tracking_trial.isna().sum() / tracking_trial.shape[0]
                coverage = 100 - perc_points
                col = 'perc_coverage_{}'.format(tracker_name)
                self.tf.loc[tid, col] = coverage

        np.testing.assert_array_equal(self.tf['perc_coverage_{}'.format(tracker_names[0])],
                                      self.tf['perc_coverage_{}'.format(tracker_names[1])])
        col = 'perc_coverage_{}'.format(tracker_names[0])
        N = (self.tf[col] < 100).sum()
        print('{} TRIALS WITH INCOMPLETE TRACKING: REMOVING THOSE TRIALS'.format(N))
        self.tf = self.tf[self.tf[col] == 100]

    def generate_trackerdata_filtered(self, frame_period_in_ms=None):

        tracking = self.trackerdata
        time = tracking['time']
        time_axis = time[pd.notna(time)]

        if frame_period_in_ms is None:
            frame_period_in_ms = 40  # frame rate of 40 ms
        self.frame_period = frame_period_in_ms * pq.ms
        # make new reliable time axis

        time_axis = np.arange(time_axis[0], time_axis[-1], frame_period_in_ms)

        trackerdata_filtered = {n : {} for n in self.tracker_names}
        trackerdata_filtered['time'] = time_axis

        for name in self.tracker_names:
            x = tracking[name]['x']
            y = tracking[name]['y']
            l = tracking[name]['likelihood']

            # TODO could save the mask for plotting
            valid_idx = np.where((pd.notna(time)) & (l >= self.trackerdata_likelihood_threshold))[0]

            int_x = np.interp(x=time_axis, xp=time[valid_idx], fp=x[valid_idx])
            int_y = np.interp(x=time_axis, xp=time[valid_idx], fp=y[valid_idx])

            x_filt = medfilt(int_x, 5)
            y_filt = medfilt(int_y, 5)
            trackerdata_filtered[name]['x'] = x_filt
            trackerdata_filtered[name]['y'] = y_filt
        return trackerdata_filtered


    def compute_velocity(self):
        # TODO this has to be adapted to the new format
        if self.verbose > 1 :
            print('Computing velocity and adding it to self.trackerdata_filtered')
        keys = self.tracker_names.copy()
        for key in keys :
            self.trackerdata_filtered['vel_{}'.format(key)] = {}
            self.tracker_names.append('vel_{}'.format(key))
            for dir in ['x', 'y'] :
                pos = self.trackerdata_filtered[key][dir]
                vel = np.hstack([[0], np.diff(pos)])
                vel = ((vel * PIXELS_TO_CM) * pq.cm / self.frame_period).rescale(pq.m/pq.s)
                vel = medfilt(vel, 3)
                self.trackerdata_filtered['vel_{}'.format(key)][dir] = vel

    def remove_trials_with_no_response(self):
        self.tf = self.tf[~self.tf['response_location'].isna()]

    def remove_trials_with_no_n_initiations(self):
        self.tf = self.tf[~self.tf['n_initiations'].isna()]

    def remove_trials_with_no_PokeC_off(self):
        self.tf = self.tf[~self.tf['PokeC_off'].isna()]

    def cast_floats_to_integers(self):
        columns = ['response_location', 'stimulus_location',
                   'trial_nr', 'n_initiations', 'correct']

        for col in columns:
            self.tf[col] = self.tf[col].astype(int)

    def add_first_poke_side(self):
        self.tf['first_poke_side'] = ['L' if l <= 4 else 'R' for l in self.tf['first_poke_location']]

    def add_previous_response_location_to_trial_frame(self):
        shifted_loc = [r for r in self.tf['response_location'][:-1]]
        shifted_loc = np.array([-99]+shifted_loc)
        self.tf['previous_response_location'] =  shifted_loc

    def add_previous_first_poke_location_to_trial_frame(self):
        shifted_loc = [r for r in self.tf['first_poke_location'][:-1]]
        shifted_loc = np.array([-99]+shifted_loc)
        self.tf['previous_first_poke_location'] =  shifted_loc

    def add_previous_poke_side(self):
        self.tf['previous_poke_side'] = ['L' if l <= 4 else 'R' for l in self.tf['previous_first_poke_location']]

    def add_separation_to_trial_frame(self):
        self.tf['separation'] = ['small' if np.isin(s, [4, 5]) else 'large' for s in
                                 self.tf['stimulus_location']]


    def add_stimulus_onset_to_trial_frame(self):
        ev = self.events
        for trial_id in self.tf.index :
            indx = np.where(ev['stimulus_on']['labels'] == trial_id)[0]
            if len(indx) == 1:
                time = ev['stimulus_on']['times'][indx[0]]
            else:
                if self.raise_warnings:
                    warnings.warn('No stimulus onset time')
                time = np.nan
            self.tf.loc[trial_id, 'stimulus_on'] = time

    def add_block_performance_indicators_to_trial_frame(self):
        # --- add consecutive correct trial ---
        consecutive_correct = np.zeros(len(self.tf)).astype(int)
        stimulus_location = self.tf['stimulus_location']
        response_location = self.tf['response_location']

        for i in range(stimulus_location.shape[0]):
            sl = stimulus_location[i]
            rl = response_location[i]

            if i == 0 :
                if sl == rl :
                    consecutive_correct[i] = 1
                else :
                    consecutive_correct[i] = 0

            else :
                psl = stimulus_location[i - 1]
                prl = response_location[i - 1]
                if sl == psl :
                    if sl == rl :
                        consecutive_correct[i] = consecutive_correct[i - 1] + 1
                    else :
                        consecutive_correct[i] = 0
                elif sl != psl :
                    if sl == rl :
                        consecutive_correct[i] = 1
                    else :
                        consecutive_correct[i] = 0
        self.tf['consecutive_correct'] = consecutive_correct

        # --- add reversal trials (including change between easy/hard trials) ---
        reversal = np.zeros(len(self.tf)).astype(int)
        for i in range(stimulus_location.shape[0]):

            if i == 0 :
                continue
            else :
                sl = stimulus_location[i]
                psl = stimulus_location[i - 1]

                if sl == psl :
                    continue
                elif sl != psl :
                    reversal[i] = 1
        self.tf['reversal'] = reversal

        # --- identify the blocks of 10 trials (where animals perform at least
        # 9 out of 10 correct) which lead to a reversal ---

        successful_block = np.zeros(len(self.tf)).astype(int)
        for i in range(stimulus_location.shape[0]) :

            if i == 0 :
                continue
            else :
                sl = stimulus_location[i]
                psl = stimulus_location[i - 1]

                if sl == psl :
                    continue
                elif sl != psl :
                    try:
                        successful_block[i - 10 :i] = np.arange(1, 11)
                    except ValueError:
                        successful_block[i - 9 :i] = np.arange(1, 10)
        self.tf['successful_block'] = successful_block

        # --- add block id ---

        block_id = np.zeros(len(self.tf)).astype(int)
        for i in range(stimulus_location.shape[0]):

            if i == 0 :
                block_id[i] = 1
            else :
                sl = stimulus_location[i]
                psl = stimulus_location[i - 1]

                if sl == psl :
                    block_id[i] = block_id[i - 1]
                elif sl != psl :
                    block_id[i] = block_id[i - 1] + 1
        self.tf['block_id'] = block_id


        # --- compute block performance ---
        block_id = self.tf['block_id']
        block_performance = np.zeros(len(self.tf)).astype(int)

        for i in range(stimulus_location.shape[0]) :

            if i == 0 :
                block_performance[i] = 0
            else :
                sl = stimulus_location[i]
                rl = response_location[i]

                if block_id[i] == block_id[i - 1] :
                    if sl == rl :
                        block_performance[i] = max(0, block_performance[i - 1] + 1)
                    elif sl != rl :
                        block_performance[i] = max(0, block_performance[i - 1] - 1)

                elif block_id[i] != block_id[i - 1] :
                    block_performance[i] = 0
        self.tf['block_performance'] = block_performance


    def add_poke_times_to_trial_frame(self):

        rev = self.raw_events
        poke_locations = range(1, 9)
        poke_on_labels = ['Poke{}_on'.format(i) for i in poke_locations]

        self.poke_on_labels = poke_on_labels
        self.poke_locations = poke_locations

        absent_pokes = [p for p in poke_on_labels if p not in rev.keys()]

        if len(absent_pokes) > 0:
            if self.raise_warnings:
                warnings.warn('Poke locations are never visited in the session')
            if self.verbose > 1 :
                print('absent poke: {}'.format(absent_pokes))
            poke_locations = [l for l in poke_locations if 'Poke{}_on'.format(l) in rev.keys()]
            poke_on_labels = [p for p in poke_on_labels if p in rev.keys()]

        self.tf[poke_on_labels] = None
        self.tf['first_poke_location'] = None
        self.tf['first_valid_poke_location'] = None

        for trial_id in self.tf.index:

            # find the first poke per location within the boundaries of each trial
            # there are pokes also before the self-paced trial initiation so
            # we only look at ones after stimulus on
            tstart = self.tf.loc[trial_id, 'stimulus_on']
            tend = self.tf.loc[trial_id, 'trial_end']

            first_pokes = {}
            for loc, lab in zip(poke_locations, poke_on_labels):

                onset = rev[lab]
                fp = onset[np.logical_and(onset >= tstart, onset <= tend)]
                if len(fp) > 0 :
                    first_pokes[loc] = fp.min()

            # add to trial frame
            for loc, lab in zip(poke_locations, poke_on_labels) :
                try:
                    self.tf.loc[trial_id, lab] = first_pokes[loc]
                except KeyError :
                    pass

            # add the first poke (across all poke locations)
            try:
                first_poke_location = min(first_pokes, key=first_pokes.get)
                first_poke_time = first_pokes[first_poke_location]
            except ValueError:
                first_poke_location = None
                first_poke_time = None
            self.tf.loc[trial_id, 'first_poke_location'] = first_poke_location
            self.tf.loc[trial_id, 'first_poke_time'] = first_poke_time

            # determine allowed poke locations
            if self.tf.loc[trial_id]['separation'] == 'small' :
                allowed_locations = [4, 5]
            elif self.tf.loc[trial_id]['separation'] == 'large' :
                allowed_locations = [2, 7]

            # subselect allowed pokes
            valid_pokes = {i : first_pokes[i] for i in first_pokes.keys() if
                           i in allowed_locations}

            # add to trial frame
            try :
                first_valid_poke_location = min(valid_pokes, key=valid_pokes.get)
                first_valid_poke_time = valid_pokes[first_valid_poke_location]
            except ValueError :
                first_valid_poke_location = None
                first_valid_poke_time = None

            self.tf.loc[trial_id, 'first_valid_poke_location'] = first_valid_poke_location
            self.tf.loc[trial_id, 'first_valid_poke_time'] = first_valid_poke_time


    def add_last_central_poke_time_to_trial_frame(self) :

        rev = self.raw_events
        self.tf['PokeC_off'] = None

        for trial_id in self.tf.index :

            tstart = self.tf.loc[trial_id, 'stimulus_on']
            #tend = self.tf.loc[trial_id, 'trial_end']
            firstpoke = self.tf.loc[trial_id, 'first_poke_time']

            pokecoffs = rev['PokeC_off']
            pokecoffs = pokecoffs[np.logical_and(pokecoffs >= tstart, pokecoffs <= firstpoke)]
            if len(pokecoffs) > 0 :
                pokecoff = pokecoffs.max()
                self.tf.loc[trial_id, 'PokeC_off'] = pokecoff


    # def align_event_times(self, align_event='stimulus_on', convert_to_seconds=True):
    #     self.poke_on_labels_aligned = []
    #     for trial_indx, trial_id in enumerate(self.tf.index):
    #         for poke_label in self.poke_on_labels + ['first_poke_time', 'first_valid_poke_time']:
    #
    #             try:
    #                 tp = self.tf.loc[trial_id, poke_label]
    #                 ta = self.tf.loc[trial_id, align_event]
    #
    #                 aligned_time = tp - ta # time in ms
    #                 if convert_to_seconds:
    #                     aligned_time = aligned_time / 1000
    #             except (TypeError, KeyError):
    #                 aligned_time = None
    #             label_aligned = '{}_aligned'.format(poke_label)
    #             self.tf.loc[trial_id, label_aligned] = aligned_time
    #             self.poke_on_labels_aligned.append(label_aligned)


    def align_event_times(self, align_event='stimulus_on', convert_to_seconds=True):
        self.poke_on_labels_aligned = []
        for trial_indx, trial_id in enumerate(self.tf.index):
            for label in self.poke_on_labels + ['stimulus_on', 'PokeC_off', 'first_poke_time',
                          'first_valid_poke_time']:

                try:
                    tp = self.tf.loc[trial_id, label]
                    ta = self.tf.loc[trial_id, align_event]

                    aligned_time = tp - ta # time in ms
                    if convert_to_seconds:
                        aligned_time = aligned_time / 1000
                except (TypeError, KeyError):
                    aligned_time = None
                label_aligned = '{}_aligned'.format(label)
                self.tf.loc[trial_id, label_aligned] = aligned_time
                if np.isin(label, self.poke_on_labels):
                    self.poke_on_labels_aligned.append(label_aligned)

    def filter_trials(self, max_reaction_time):
        if self.raise_warnings:
            warnings.warn('First poke time could be in seconds if you have used'
                      'align_poke_time()')
        if self.verbose > 1 :
            print('Filtering trials with reaction time > {}'.format(max_reaction_time))
        self.tf = self.tf[self.tf['first_poke_time_aligned'] <= max_reaction_time]

        if self.verbose > 1 :
            print('Filtering trials where the first response is not in a valid '
              'location')
        self.tf = self.tf[np.isin(self.tf['first_poke_location'], self.valid_locations)]


    def get_aligned_epoch_times(self, trial_ids, align_event=None, time_before_in_s=1,
                                time_after_in_s=1):


        selected_trial_data = self.tf.loc[trial_ids, :]

        event_t = selected_trial_data[align_event].tolist() * pq.ms

        trial_starts = [s - time_before_in_s * pq.s for s in event_t]
        trial_ends = [s + time_after_in_s * pq.s for s in event_t]

        return trial_starts, trial_ends, event_t


    def _make_bins(self,  binsize, t_start, t_stop, slide_by=None):

        # convert everything to seconds
        t_start = t_start.rescale(pq.ms)
        t_stop = t_stop.rescale(pq.ms)
        binsize = binsize.rescale(pq.ms)

        if slide_by is not None:
            slide_by = slide_by.rescale(pq.ms)
            left_edges = np.arange(t_start, t_stop, slide_by)
            bins = [(e, e + binsize.item()) for e in left_edges]

        else:
            left_edges = np.arange(t_start, t_stop, binsize)
            bins = [(e, e + binsize.item()) for e in left_edges]

        # make sure that all the bin centers are within the event
        bins = [b for b in bins if (b[0] + b[1]) / 2 <= (t_stop)]
        # make sure that bins are fully within
        #bins = [b for b in bins if b[1] <= t_stop]

        # prepare the bin centers (to return)
        spike_bin_centers = [(b1 + b2) / 2 for b1, b2 in bins]
        return bins, spike_bin_centers


    def _bin_spikes(self, binsize, t_start, t_stop, slide_by=None,
                    unit_ids=None):

        bins, spike_bin_centers = self._make_bins(binsize=binsize,
                                                  t_start=t_start,
                                                  t_stop=t_stop,
                                                  slide_by=slide_by)


        if unit_ids is None:
            if self.raise_warnings:
                warnings.warn('No units selected, binning all units')
            unit_ids = self.spiketrains.keys()

        num_bins = len(bins)  # Number of bins
        num_neurons = len(unit_ids)  # Number of neurons
        binned_spikes = np.zeros([num_neurons, num_bins], dtype=int)

        if self.raise_warnings:
            warnings.warn('Assuming spike times are in ms!')

        spiketrains = [self.spiketrains[u] * pq.ms for u in unit_ids]

        for i, train in enumerate(spiketrains):
            for t, bin in enumerate(bins):
                binned_spikes[i, t] = np.histogram(train, bin)[0]

        binned_spikes = binned_spikes.astype(int)

        negative_vals = (binned_spikes<0).sum()
        if negative_vals > 0:
            raise ValueError

        spike_bin_centers = np.array(spike_bin_centers) * pq.ms
        spike_bin_edges = [b*pq.ms for b in bins]

        return binned_spikes, spike_bin_centers, spike_bin_edges

    def bin_spikes_per_trial(self, binsize, trial_starts, trial_ends,
                             sliding_window=False, slide_by=None, unit_ids=None):

        if sliding_window is False:
            if self.raise_warnings:
                warnings.warn('sliding_window is False, setting slide_by to None')
            slide_by=None

        binned_spikes, spike_bin_centers, spike_bin_edges = [], [], []

        for i, (t_start, t_stop) in enumerate(zip(trial_starts, trial_ends)):

            bs, bc, be = self._bin_spikes(binsize=binsize,
                                          t_start=t_start,
                                          t_stop=t_stop,
                                          slide_by=slide_by,
                                          unit_ids=unit_ids)

            if unit_ids is not None:
                if bs.shape[0] != len(unit_ids):
                    raise warnings.warn('Shape mismatch between spikes and unit ids')
            binned_spikes.append(bs)
            spike_bin_centers.append(bc)
            spike_bin_edges.append(be)

        return binned_spikes, spike_bin_centers, spike_bin_edges


    def _bin_licks(self, binsize, t_start, t_stop, slide_by=None):

        bins, bin_centers = self._make_bins(binsize=binsize,
                                                  t_start=t_start,
                                                  t_stop=t_stop,
                                                  slide_by=slide_by)

        num_bins = len(bins)  # Number of bins
        binned_licks = np.zeros([1, num_bins], dtype=int)

        lickon_labels = ['Lick{}_on'.format(l) for l in
                         ['C'] + list(range(1, 9))]

        licktimes = []
        for l in lickon_labels:
            try:
                licktimes.append(self.raw_events[l])
            except KeyError:
                pass
        licktimes = np.hstack(licktimes)

        for t, bin in enumerate(bins) :
            binned_licks[0, t] = np.histogram(licktimes, bin)[0]

        negative_vals = (binned_licks < 0).sum()
        if negative_vals > 0 :
            raise ValueError

        bin_centers = np.array(bin_centers) * pq.ms
        bin_edges = [b * pq.ms for b in bins]

        return binned_licks, bin_centers, bin_edges


    def bin_licks_per_trial(self, binsize, trial_starts, trial_ends,
                             sliding_window=False, slide_by=None, binarize=True) :

        if sliding_window is False :
            warnings.warn('sliding_window is False, setting slide_by to None')
            slide_by = None

        binned_licks, spike_bin_centers, spike_bin_edges = [], [], []

        for i, (t_start, t_stop) in enumerate(zip(trial_starts, trial_ends)) :

            bl, bc, be = self._bin_licks(binsize=binsize,
                                          t_start=t_start,
                                          t_stop=t_stop,
                                          slide_by=slide_by)
            if binarize:
                bl[bl>0] = 1
            binned_licks.append(bl)
            spike_bin_centers.append(bc)
            spike_bin_edges.append(be)

        return binned_licks, spike_bin_centers, spike_bin_edges



    # def _bin_trackerdata(self, binsize, t_start, t_stop, slide_by=None):
    #
    #
    #     bins, bin_centers = self._make_bins(binsize=binsize,
    #                                         t_start=t_start,
    #                                         t_stop=t_stop,
    #                                         slide_by=slide_by)
    #
    #     keys = self.tracker_names
    #     dirs = ['x', 'y']
    #
    #     time = self.trackerdata_filtered['time']
    #     td = np.vstack([self.trackerdata_filtered[key][dir] for key in keys for dir in dirs])
    #     labels = ['{}_{}'.format(key, dir) for key in keys for dir in dirs]
    #     self.trackerdata_binned_names = labels
    #
    #     num_bins = len(bins)  # Number of bins
    #     num_trackerpoints = td.shape[0]  # Number of neurons
    #     binned_td = np.empty([num_trackerpoints, num_bins])
    #
    #     for t, bin in enumerate(bins) :
    #         mask = np.logical_and(time >= bin[0], time < bin[1])
    #         if mask.sum() == 0:
    #             warnings.warn('Tracker data missing in bin')
    #         binned_td[:, t] = td[:, mask].mean(axis=1)
    #
    #     td_bin_centers = np.array(bin_centers) * pq.ms
    #     td_bin_edges = [b * pq.ms for b in bins]
    #
    #     return binned_td, td_bin_centers, td_bin_edges

    def _bin_trackerdata(self, binsize, t_start, t_stop, slide_by=None):


        bins, bin_centers = self._make_bins(binsize=binsize,
                                            t_start=t_start,
                                            t_stop=t_stop,
                                            slide_by=slide_by)

        time = self.trackerdata['time'].__array__()
        td = self.trackerdata[self.trackerdata_binned_names].__array__().T

        num_bins = len(bins)  # Number of bins
        num_trackerpoints = td.shape[0]  # Number of trackerdata variables
        binned_td = np.empty([num_trackerpoints, num_bins])

        for t, bin in enumerate(bins) :
            mask = np.logical_and(time >= bin[0], time < bin[1])
            if mask.sum() == 0:
                warnings.warn('Tracker data missing in bin')
            binned_td[:, t] = td[:, mask].mean(axis=1)

        td_bin_centers = np.array(bin_centers) * pq.ms
        td_bin_edges = [b * pq.ms for b in bins]

        return binned_td, td_bin_centers, td_bin_edges


    def bin_trackerdata_per_trial(self, binsize, trial_starts, trial_ends,
                             sliding_window=False, slide_by=None):

        if sliding_window is False:
            warnings.warn('sliding_window is False, setting slide_by to None')
            slide_by=None

        binned_td, td_bin_centers, td_bin_edges = [], [], []

        for i, (t_start, t_stop) in enumerate(zip(trial_starts, trial_ends)):

            btd, bc, be = self._bin_trackerdata(binsize=binsize,
                                          t_start=t_start,
                                          t_stop=t_stop,
                                          slide_by=slide_by)
            assert np.isnan(btd).sum() == 0
            binned_td.append(btd)
            td_bin_centers.append(bc)
            td_bin_edges.append(be)

        return binned_td, td_bin_centers, td_bin_edges



    def _bin_ISI(self, binsize, t_start, t_stop, slide_by=None,
                    unit_ids=None):

        bins, isi_bin_centers = self._make_bins(binsize=binsize,
                                                  t_start=t_start,
                                                  t_stop=t_stop,
                                                  slide_by=slide_by)


        if unit_ids is None:
            warnings.warn('No units selected, binning all units')
            unit_ids = self.spiketrains.keys()

        num_bins = len(bins)  # Number of bins
        num_neurons = len(unit_ids)  # Number of neurons
        binned_isi = np.zeros([num_neurons, num_bins], dtype=int)

        warnings.warn('Assuming spike times are in ms!')

        spiketrains = [self.spiketrains[u] * pq.ms for u in unit_ids]

        for i, train in enumerate(spiketrains):
            for t, bin in enumerate(bins):
                train_slice = train[np.logical_and(train>=bin[0], train<bin[1])]
                isis = train_slice[1 :] - train_slice[:-1]
                try:
                    binned_isi[i, t] = isis.mean()
                except ValueError:
                    # TODO is it smart to set it to zero or should be set to binsize?
                    binned_isi[i, t] = 0

        binned_isi = binned_isi.astype(int)

        negative_vals = (binned_isi<0).sum()
        if negative_vals > 0:
            raise ValueError

        isi_bin_centers = np.array(isi_bin_centers) * pq.ms
        isi_bin_edges = [b*pq.ms for b in bins]

        return binned_isi, isi_bin_centers, isi_bin_edges


    def bin_ISI_per_trial(self, binsize, trial_starts, trial_ends,
                             sliding_window=False, slide_by=None, unit_ids=None):

        if sliding_window is False:
            warnings.warn('sliding_window is False, setting slide_by to None')
            slide_by=None

        binned_isi, isi_bin_centers, isi_bin_edges = [], [], []

        for i, (t_start, t_stop) in enumerate(zip(trial_starts, trial_ends)):

            bs, bc, be = self._bin_ISI(binsize=binsize,
                                      t_start=t_start,
                                      t_stop=t_stop,
                                      slide_by=slide_by,
                                      unit_ids=unit_ids)

            if unit_ids is not None:
                if bs.shape[0] != len(unit_ids):
                    raise warnings.warn('Shape mismatch between ISIs and unit ids')
            binned_isi.append(bs)
            isi_bin_centers.append(bc)
            isi_bin_edges.append(be)

        return binned_isi, isi_bin_centers, isi_bin_edges

    def get_aligned_bins(self, align_event, spike_bin_centers=None,
                         spike_bin_edges=None):

        if spike_bin_centers is not None:
            bin_centers_aligned = spike_bin_centers[0] - align_event[0]
        if spike_bin_edges is not None:
            bin_edges_aligned = [s - align_event[0] for s in spike_bin_edges[0]]
        return bin_centers_aligned, bin_edges_aligned



    def compute_spatial_occupancy(self, spatial_bin_size, x_max, y_max,
                                  trial_ids=None, smooth_sigma=None):

        pos_time = self.trackerdata_filtered['time'].copy()
        pos_head_x = self.trackerdata_filtered['head']['x'].copy()
        pos_head_y = self.trackerdata_filtered['head']['y'].copy()
        tf = self.tf

        # --- REMOVE NANS ---
        mask = ~np.isnan(pos_time)
        pos_time = pos_time[mask]
        pos_head_x = pos_head_x[mask]
        pos_head_y = pos_head_y[mask]

        # --- BIN ---
        if x_max is None:
            x_max = np.max(pos_head_x + 1)

        if y_max is None:
            y_max = np.max(pos_head_y + 1)


        x_bins = np.arange(0, x_max, spatial_bin_size)
        y_bins = np.arange(0, y_max, spatial_bin_size)
        x_binned = np.digitize(pos_head_x, bins=x_bins) - 1
        y_binned = np.digitize(pos_head_y, bins=y_bins) - 1

        # --- SELECT TRIALS ---
        if trial_ids is None:
            # restrict to portion of the task
            first_trial = tf['trial_start'].iloc[0]
            last_trial = tf['trial_end'].iloc[-1]
            mask = np.logical_and(pos_time >= first_trial,
                                  pos_time <= last_trial)
            pos_time = pos_time[mask]
            x_binned = x_binned[mask]
            y_binned = y_binned[mask]
        else:
            pos_time_all = []
            x_binned_all, y_binned_all = [], []
            for trial_id, row in tf.loc[trial_ids].iterrows() :
                tstart, tend = row['trial_start'], row['trial_end']
                mask = np.logical_and(pos_time >= tstart, pos_time <= tend)
                pos_time_all.append(pos_time[mask])
                x_binned_all.append(x_binned[mask])
                y_binned_all.append(y_binned[mask])
            pos_time = np.hstack(pos_time_all)
            x_binned = np.hstack(x_binned_all)
            y_binned = np.hstack(y_binned_all)

        self.pos_time = pos_time
        self.x_bins = x_bins
        self.y_bins = y_bins
        self.x_binned = x_binned
        self.y_binned = y_binned

        occupancy = np.zeros(shape=[len(x_bins), len(y_bins)])
        for i, t in enumerate(pos_time) :
            occupancy[x_binned[i], y_binned[i]] += self.frame_period.rescale(pq.s).item()

        if smooth_sigma is None:
            pass
        else:
            occupancy = gaussian_filter(occupancy, sigma=smooth_sigma)

        return occupancy * pq.s


    def compute_spatial_firing(self, unit_id, x_max, y_max,
                               spatial_bin_size, trial_ids=None,
                               smooth_sigma=None):

        spikes = self.spiketrains[unit_id]
        tf = self.tf
        pos_time = self.trackerdata_filtered['time'].copy()
        pos_head_x = self.trackerdata_filtered['head']['x'].copy()
        pos_head_y = self.trackerdata_filtered['head']['y'].copy()

        # --- PREPARE BINNED DATA ---
        mask = ~np.isnan(pos_time)
        pos_time = pos_time[mask]
        pos_head_x = pos_head_x[mask]
        pos_head_y = pos_head_y[mask]

        if x_max is None:
            x_max = np.max(pos_head_x + 1)

        if y_max is None:
            y_max = np.max(pos_head_y + 1)

        x_bins = np.arange(0, x_max, spatial_bin_size)
        y_bins = np.arange(0, y_max, spatial_bin_size)
        x_binned = np.digitize(pos_head_x, bins=x_bins) - 1
        y_binned = np.digitize(pos_head_y, bins=y_bins) - 1

        # --- SELECT TRIALS ---
        if trial_ids is None:
            # restrict to portion of the task
            first_trial = tf['trial_start'].iloc[0]
            last_trial = tf['trial_end'].iloc[-1]
            spikes = spikes[spikes >= first_trial]
            spikes = spikes[spikes <= last_trial]
        else:
            spikes_all = []
            for trial_id, row in tf.loc[trial_ids].iterrows():
                tstart, tend = row['trial_start'], row['trial_end']
                spikes_all.append(spikes[np.logical_and(spikes>=tstart, spikes<=tend)])
            spikes = np.hstack(spikes_all)

        spikes_mat = np.zeros(shape=[len(x_bins), len(y_bins)])
        for spike in spikes :
            indx = np.nanargmin(np.abs(pos_time - spike))
            spikes_mat[x_binned[indx], y_binned[indx]] += 1

        if smooth_sigma is None:
            pass
        else:
            spikes_mat = gaussian_filter(spikes_mat, sigma=smooth_sigma)

        return spikes_mat



