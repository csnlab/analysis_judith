import seaborn as sns
import matplotlib
import numpy as np
import matplotlib.pyplot as plt

plt.rc('legend',fontsize=6) # using a size in points
plt.rc('font', family='Helvetica')

rasters_cmap = matplotlib.cm.get_cmap('magma')

ratemaps_cmap = matplotlib.cm.get_cmap('magma')

# poke_location_cmap = sns.diverging_palette(h_neg=130,
#                                            h_pos=250,
#                                            s=500,
#                                            l=70, sep=1, center='light', n=4)
# sns.palplot(poke_location_cmap)
#
# location_palette = {i+1:poke_location_cmap[i] for i in range(8)}

dashes_dict = {'VEH' : '', 'CNO' : [6, 2]}

decoding_features_palette = {'CA1' : sns.xkcd_rgb['turquoise'],
                'DG' : sns.xkcd_rgb['peach'],
                'CA3' : sns.xkcd_rgb['dark pink'],
                'trackerdata' : sns.xkcd_rgb['grey'],
                'all_areas' : sns.xkcd_rgb['lilac'],
                'all_areas+isi' : sns.xkcd_rgb['dark lilac'],
                'licks' : sns.xkcd_rgb['red'],
                'trackerdata+licks' : sns.xkcd_rgb['green']}

treatment_palette = {'VEH' : sns.xkcd_rgb['lighter purple'],
                     'CNO' : sns.xkcd_rgb['light teal']}


poke_location_cmap = matplotlib.cm.get_cmap('cool')

# location_palette = {2 : poke_location_cmap(0),
#                     4 : poke_location_cmap(0.25),
#                     5 : poke_location_cmap(0.75),
#                     7 : poke_location_cmap(0.99),
#                     1 : sns.xkcd_rgb['grey'],
#                     3 : sns.xkcd_rgb['grey'],
#                     6 : sns.xkcd_rgb['grey'],
#                     8 : sns.xkcd_rgb['grey']}

location_palette = {2 : sns.xkcd_rgb['bright green'],
                    4 : sns.xkcd_rgb['turquoise green'],
                    5 : sns.xkcd_rgb['turquoise blue'],
                    7 : sns.xkcd_rgb['bright light blue'],
                    1 : sns.xkcd_rgb['grey'],
                    3 : sns.xkcd_rgb['grey'],
                    6 : sns.xkcd_rgb['grey'],
                    8 : sns.xkcd_rgb['grey']}

location_palette = {2 : sns.xkcd_rgb['bright green'],
                    4 : sns.xkcd_rgb['bright yellow'],
                    5 : sns.xkcd_rgb['bright red'],
                    7 : sns.xkcd_rgb['bright light blue'],
                    1 : sns.xkcd_rgb['grey'],
                    3 : sns.xkcd_rgb['grey'],
                    6 : sns.xkcd_rgb['grey'],
                    8 : sns.xkcd_rgb['grey'],
                    'C' : sns.xkcd_rgb['grey']}


event_labels = {
                'stimulus_on' : 'Stim. ON',
                'stimulus_on_aligned' : 'Stim. ON',
                'first_poke_time' : 'First\npoke',
                'first_poke_time_aligned' : 'First\npoke',
                'PokeC_off' : 'Choice\nstart',
                'PokeC_off_aligned' : 'Choice\nstart'}


#location_palette = {i+1 : poke_location_cmap(np.linspace(0, 1, 8)[i]) for i in range(8)}