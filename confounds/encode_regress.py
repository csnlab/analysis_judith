import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.linear_model import SGDClassifier, SGDRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics import r2_score, make_scorer
from MLencoding import MLencoding
from sklearn.preprocessing import LabelEncoder

"""
"""


settings_name = 'may13'
binned_data_settings = 'apr18'

plot_format = 'png'

min_trials_per_class = 15
min_units = 10

# TIME PARAMETERS

# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 100

decoding_features = ['all_areas'] + AREAS + ['trackerdata', 'licks', 'trackerdata+licks']

SESSION_IDS = get_preprocessed_sessions()

plots_folder = os.path.join('/Users/pietro/data/deconf', 'plots', 'judith')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


# --- OUTPUT FILES -------------------------------------------------------------

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


features = 'all_areas'

df = pd.DataFrame(columns=['animal_id', 'session_id', 'treatment',
                           'decoding_features',
                           'decoder', 'time', 'time_bin', 't0', 't1',
                           'score', 'n_neurons'])

for session_id in ['R4_S01']: #'R5_S03']:

    session = Session(session_id=session_id)
    session.load_data()
    session.load_binned_data(binned_data_settings=binned_data_settings,
                             bad_units=bad_units)
    session.align_event_times(align_event=binned_data_pars['align_event'],
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

    # --- SELECT TRIALS ---

    tf_sel = session.tf
    trial_ids = tf_sel.index

    if np.isin(features, AREAS) :
        unit_indx = session.binned_data['unit_indx_per_area'][features]
        binned_data = session.binned_data['binned_spikes']
        binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

    elif features == 'all_areas' :
        binned_data = session.binned_data['binned_spikes']
        unit_indx = session.binned_data['unit_indx']
        binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

    elif features == 'trackerdata':
        binned_data = session.binned_data['binned_trackerdata']
        binned_data = [binned_data[t]for t in trial_ids]

    elif features == 'licks':
        binned_data = session.binned_data['binned_licks']
        binned_data = [binned_data[t]for t in trial_ids]

    binned_tracker = session.binned_data['binned_trackerdata']
    binned_licks = session.binned_data['binned_licks']
    #binned_confound = [np.vstack([binned_tracker[t], binned_licks[t]]) for t in trial_ids]
    binned_confound = [binned_tracker[t] for t in trial_ids]

    bc_aligned = session.binned_data['bin_centers_aligned']
    be_aligned = session.binned_data['bin_edges_aligned']

    #target = tf_sel.loc[trial_ids, 'separation']
    target = tf_sel.loc[trial_ids, 'consecutive_correct']
    target = np.repeat(target, session.binned_data['bin_centers_aligned'].shape[0])

    X = np.hstack([s for s in binned_data]).T
    C = np.hstack([s for s in binned_confound]).T

    C = C[:,[2]]

    T = LabelEncoder().fit_transform(target)

    predictor_matrix = np.hstack([C / C.max(), T[:, np.newaxis]])
    predictor_matrix_onlyconf = np.hstack([C / C.max(), np.zeros(shape=[T.shape[0], 1])])
    predictor_matrix_onlytarg = np.hstack([np.zeros(shape=[C.shape[0], C.shape[1]]), T[:, np.newaxis]])

    predictor_matrix = predictor_matrix.astype('float32')
    predictor_matrix_onlyconf = predictor_matrix_onlyconf.astype('float32')
    predictor_matrix_onlytarg = predictor_matrix_onlytarg.astype('float32')



    selected_id = 'uid_R4_S01_007'
    spikes_binned = X[:, session.binned_data['unit_ids'] == selected_id]

    encoding_model = 'random_forest'  # 'glm'#'random_forest'
    n_cv = 3
    encoder_params = {'n_estimators' : 100}
    cov_history = True
    spike_history = False
    max_time = 3000
    n_filters = 3
    n_every = 1  # predict all the time bins

    encoder = MLencoding(tunemodel=encoding_model,
                         cov_history=cov_history,
                         window=session.binned_data['binsize_in_ms'],
                         max_time=max_time,
                         n_filters=n_filters)

    if encoding_model == 'random_forest' :
        encoder.set_params(encoder_params)


    encoder.fit(predictor_matrix, spikes_binned[:, 0])
    rate_pred_full = encoder.predict(predictor_matrix)
    rate_pred_onlyconf = encoder.predict(predictor_matrix_onlyconf)
    rate_pred_onlytarg = encoder.predict(predictor_matrix_onlytarg)


    f, ax = plt.subplots(3, 1, figsize=[10, 7], sharex=True)

    ax[0].plot(C[:, 0] / C.max(), label='Confound', color='red')
    ax[0].plot(T, label='Target', color='green')
    ax[0].legend(loc='upper right')

    ax[1].plot(spikes_binned, label="Binned spikes", c='k', alpha=0.8, lw=0.8)
    ax[1].plot(rate_pred_full, label="RF Prediction (full model)", c='orange')
    ax[1].legend(loc='upper right')

    ax[2].plot(spikes_binned, label="Binned spikes", c='k', alpha=0.8, lw=0.8)
    ax[2].plot(rate_pred_onlyconf, label="RF Prediction (only confound)",
               c='red')
    ax[2].plot(rate_pred_onlytarg, label="RF Prediction (only target)",
               c='green')
    ax[2].legend(loc='upper right')

    ax[1].set_ylabel('spike counts')
    plt.tight_layout()
    sns.despine()



    X = rate_pred_full[:, np.newaxis]
    X_deconf = rate_pred_onlytarg[:, np.newaxis]

    n_samples_train = int(X.shape[0] / 2)

    X_train = X[0 :n_samples_train, :]
    X_test = X[n_samples_train :, :]

    X_train_deconf = X_deconf[0 :n_samples_train, :]
    X_test_deconf = X_deconf[n_samples_train :, :]

    C_train = C[0 :n_samples_train, :]
    C_test = C[n_samples_train :, :]

    T_train = T[0 :n_samples_train]
    T_test = T[n_samples_train :]

    # predict confound from features
    rf = RandomForestRegressor(n_estimators=500).fit(X_train, T_train)
    T_pred = rf.predict(X_test)
    r2 = r2_score(y_true=T_test, y_pred=T_pred)

    # predict confound from deconfounded features
    rf = RandomForestRegressor(n_estimators=500).fit(X_train_deconf, T_train)
    T_pred_deconf = rf.predict(X_test_deconf)
    r2_deconf = r2_score(y_true=T_test, y_pred=T_pred_deconf)


    f, ax = plt.subplots(1, 1, figsize=[4, 3])
    ax.plot(T_test, label='$T$')
    ax.plot(T_pred, label='$T_{pred}$', alpha=0.5, zorder=-10)
    ax.plot(T_pred_deconf, label='$T_{pred, deconf. }$')

    ax.legend()
    plt.tight_layout()
    sns.despine()
    plot_name = 'target_prediction.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


    # X = rate_pred_full[:, np.newaxis]
    # X_deconf = rate_pred_onlytarg[:, np.newaxis]
    #
    # n_samples_train = int(X.shape[0] / 2)
    #
    # X_train = X[0 :n_samples_train, :]
    # X_test = X[n_samples_train :, :]
    #
    # X_train_deconf = X_deconf[0 :n_samples_train, :]
    # X_test_deconf = X_deconf[n_samples_train :, :]
    #
    # C_train = C[0 :n_samples_train, :]
    # C_test = C[n_samples_train :, :]
    #
    # # predict confound from features
    # rf = RandomForestRegressor(n_estimators=500).fit(X_train, C_train)
    # C_pred = rf.predict(X_test)
    # r2 = r2_score(y_true=C_test, y_pred=C_pred)
    #
    # # predict confound from deconfounded features
    # rf = RandomForestRegressor(n_estimators=500).fit(X_train_deconf, C_train)
    # C_pred_deconf = rf.predict(X_test_deconf)
    # r2_deconf = r2_score(y_true=C_test, y_pred=C_pred_deconf)
    #
    #
    # f, ax = plt.subplots(1, 1, figsize=[6, 4])
    # ax.plot(C_test[:, 0], label='$C$')
    # ax.plot(C_pred, label='$C_{pred}$')
    # ax.plot(C_pred_deconf, label='$C_{pred, deconf. }$')
    # ax.legend()
    # plt.tight_layout()
    # sns.despine()
    #plot_name = 'confound_prediction.{}'.format(plot_format)
    #f.savefig(os.path.join(plots_folder, plot_name), dpi=400)






