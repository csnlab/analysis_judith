import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.linear_model import SGDClassifier, SGDRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
from utils import *
from sklearn.model_selection import cross_val_score, cross_val_predict
from sklearn.metrics import r2_score, make_scorer

"""
"""


settings_name = 'may13'
binned_data_settings = 'apr18'


min_trials_per_class = 15
min_units = 10

# TIME PARAMETERS

# DECODING PARAMETERS
n_repeats = 5
n_splits = 3
decoder_name='SGD'
n_estimators = 100

decoding_features = ['all_areas'] + AREAS + ['trackerdata', 'licks', 'trackerdata+licks']

SESSION_IDS = get_preprocessed_sessions()


# --- OUTPUT FILES -------------------------------------------------------------

binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


features = 'all_areas'

df = pd.DataFrame(columns=['animal_id', 'session_id', 'treatment',
                           'decoding_features',
                           'decoder', 'time', 'time_bin', 't0', 't1',
                           'score', 'n_neurons'])

for session_id in ['R4_S02']: #'R5_S03']:

    session = Session(session_id=session_id)
    session.load_data()
    session.load_binned_data(binned_data_settings=binned_data_settings,
                             bad_units=bad_units)
    session.align_event_times(align_event=binned_data_pars['align_event'],
                              convert_to_seconds=True)
    session.filter_trials(max_reaction_time=binned_data_pars['max_reaction_time'])

    # --- SELECT TRIALS ---

    tf_sel = session.tf
    trial_ids = tf_sel.index

    if np.isin(features, AREAS) :
        unit_indx = session.binned_data['unit_indx_per_area'][features]
        binned_data = session.binned_data['binned_spikes']
        binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

    elif features == 'all_areas' :
        binned_data = session.binned_data['binned_spikes']
        unit_indx = session.binned_data['unit_indx']
        binned_data = [binned_data[t][unit_indx, :] for t in trial_ids]

    elif features == 'trackerdata':
        binned_data = session.binned_data['binned_trackerdata']
        binned_data = [binned_data[t]for t in trial_ids]

    elif features == 'licks':
        binned_data = session.binned_data['binned_licks']
        binned_data = [binned_data[t]for t in trial_ids]

    binned_tracker = session.binned_data['binned_trackerdata']
    binned_licks = session.binned_data['binned_licks']
    #binned_confound = [np.vstack([binned_tracker[t], binned_licks[t]]) for t in trial_ids]
    binned_confound = [binned_tracker[t] for t in trial_ids]

    bc_aligned = session.binned_data['bin_centers_aligned']
    be_aligned = session.binned_data['bin_edges_aligned']


    X = np.hstack([s for s in binned_data]).T
    C = np.hstack([s for s in binned_confound]).T

    if False:
        decoder = RandomForestRegressor(n_estimators=n_estimators)

        # predict confound from neural data
        scorer = make_scorer(r2_score)
        score = cross_val_score(cv=n_splits, estimator=decoder, X=X,
                                y=C, scoring=scorer)
        C_pred = cross_val_predict(cv=n_splits, estimator=decoder, X=X, y=C)

        # regress out the confound
        rfreg = RandomForestRegressor(n_estimators=500).fit(C, X)
        X_rf_pred = rfreg.predict(C)
        X_deconf = X - X_rf_pred

        # predict confound from the deconfounded neural data
        score_deconf = cross_val_score(cv=n_splits, estimator=decoder, X=X_deconf,
                                y=C, scoring=scorer)

        C_pred_deconf = cross_val_predict(cv=n_splits, estimator=decoder, X=X_deconf, y=C)


        indxmax = 500
        f, ax = plt.subplots(3, 1, figsize=[8, 8])
        ax[0].plot(C[:, 0][0:indxmax], label='Head - x')
        ax[0].plot(C_pred[:, 0][0:indxmax], label='Head - x (predicted)')
        ax[0].plot(C_pred_deconf[:, 0][0:indxmax], label='Head - x (deconf. pred.)')

        ax[1].plot(C[:, 2][0:1000][0:indxmax], label='Neck - x')
        ax[1].plot(C_pred[:, 2][0:indxmax], label='Neck - x (predicted)')
        ax[1].plot(C_pred_deconf[:, 2][0:indxmax], label='Neck - x (deconf. pred.)')

        ax[0].legend()
        ax[1].legend()

        sns.despine()
        plt.tight_layout()


    if False:
        decoder = RandomForestRegressor(n_estimators=300)
        C = C[:, 0]
        C = C[:, np.newaxis]

        # predict confound from neural data
        scorer = make_scorer(r2_score)
        score = cross_val_score(cv=n_splits, estimator=decoder, X=X,
                                y=C, scoring=scorer)
        C_pred = cross_val_predict(cv=n_splits, estimator=decoder, X=X, y=C)

        # regress out the confound
        rfreg = RandomForestRegressor(n_estimators=500).fit(C, X)
        X_rf_pred = rfreg.predict(C)
        X_deconf = X - X_rf_pred

        # predict confound from the deconfounded neural data
        score_deconf = cross_val_score(cv=n_splits, estimator=decoder, X=X_deconf,
                                y=C, scoring=scorer)

        C_pred_deconf = cross_val_predict(cv=n_splits, estimator=decoder, X=X_deconf, y=C)


        indxmax = 500
        f, ax = plt.subplots(3, 1, figsize=[8, 8])
        ax[0].plot(C[:, 0][0:indxmax], label='Head - x')
        ax[0].plot(C_pred[0:indxmax], label='Head - x (predicted)')
        ax[0].plot(C_pred_deconf[0:indxmax], label='Head - x (deconf. pred.)')

        ax[0].legend()
        ax[1].legend()

        sns.despine()
        plt.tight_layout()


    if False:
        decoder = RandomForestRegressor(n_estimators=300)
        C = C[:, 0]
        C = C[:, np.newaxis]

        # predict confound from neural data
        scorer = make_scorer(r2_score)
        score = cross_val_score(cv=n_splits, estimator=decoder, X=X,
                                y=C, scoring=scorer)
        C_pred = cross_val_predict(cv=n_splits, estimator=decoder, X=X, y=C)

        # regress out the confound
        X_deconf = []
        for indx in np.arange(X.shape[1]):

            rfreg = RandomForestRegressor(n_estimators=500).fit(C, X[:, indx])
            X_rf_pred = rfreg.predict(C)
            X_deconf.append(X[:, indx] - X_rf_pred)
        X_deconf = np.hstack([x[:, np.newaxis] for x in X_deconf])

        # predict confound from the deconfounded neural data
        score_deconf = cross_val_score(cv=n_splits, estimator=decoder, X=X_deconf,
                                y=C, scoring=scorer)

        C_pred_deconf = cross_val_predict(cv=n_splits, estimator=decoder, X=X_deconf, y=C)


        indxmax = 500
        f, ax = plt.subplots(3, 1, figsize=[8, 8])
        ax[0].plot(C[:, 0][0:indxmax], label='Head - x')
        ax[0].plot(C_pred[0:indxmax], label='Head - x (predicted)')
        ax[0].plot(C_pred_deconf[0:indxmax], label='Head - x (deconf. pred.)')

        ax[0].legend()
        ax[1].legend()

        sns.despine()
        plt.tight_layout()




    kfold = KFold(n_splits=n_splits, shuffle=False,
                            random_state=1)

    scores, scores_deconf = [], []
    C_test_all, C_pred_all, C_pred_all_deconf = [], [], []

    for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, C)) :

        decoder = RandomForestRegressor(n_estimators=300)

        X_train = X[training_ind, :]
        X_test = X[testing_ind, :]
        C_train = C[training_ind]
        C_test = C[testing_ind]

        ss = StandardScaler()
        X_train = ss.fit_transform(X_train)
        X_test = ss.transform(X_test)

        rf = RandomForestRegressor(n_estimators=500).fit(X_train, C_train)
        C_pred = rf.predict(X_test)
        r2 = r2_score(y_true=C_test, y_pred=C_pred)
        scores.append(r2)
        C_test_all.append(C_test)
        C_pred_all.append(C_pred)

        rfdc = RandomForestRegressor(n_estimators=500).fit(C_train, X_train)
        X_train_deconf = X_train - rfdc.predict(C_train)
        X_test_deconf = X_test - rfdc.predict(C_test)


        rf = RandomForestRegressor(n_estimators=500).fit(X_train_deconf, C_train)
        C_pred_deconf = rf.predict(X_test_deconf)
        r2_deconf = r2_score(y_true=C_test, y_pred=C_pred_deconf)
        scores_deconf.append(r2_deconf)
        C_pred_all_deconf.append(C_pred_deconf)


    C_test_all = np.vstack(C_test_all)
    C_pred_all = np.vstack(C_pred_all)
    C_pred_all_deconf = np.vstack(C_pred_all_deconf)

    print(scores)
    print(scores_deconf)


    indxmax = 500
    f, ax = plt.subplots(3, 1, figsize=[7, 7], sharex=True)
    ax[0].plot(C_test_all[:, 0][0:indxmax], label='Head - x')
    ax[0].plot(C_pred_all[:, 0][0:indxmax], label='Head - x (predicted)')
    ax[0].plot(C_pred_all_deconf[:, 0][0:indxmax], label='Head - x (deconf. pred.)')

    ax[1].plot(C_test_all[:, 2][0:indxmax], label='Neck - x')
    ax[1].plot(C_pred_all[:, 2][0:indxmax], label='Neck - x (predicted)')
    ax[1].plot(C_pred_all_deconf[:, 2][0:indxmax], label='Neck - x (deconf. pred.)')

    ax[2].plot(C_test_all[:, 4][0:indxmax], label='Tail - x')
    ax[2].plot(C_pred_all[:, 4][0:indxmax], label='Tail - x (predicted)')
    ax[2].plot(C_pred_all_deconf[:, 4][0:indxmax], label='Tail - x (deconf. pred.)')

    ax[1].legend()
    ax[2].legend()

    ax[-1].set_xlabel('Samples')
    for axx in ax:
        axx.legend()
        axx.set_ylabel('Pixels')

    sns.despine()
    plt.tight_layout()


# numeric_cols = ['score']
#
# for col in numeric_cols :
#     df[col] = pd.to_numeric(df[col])
#
# df['time'] = [t.rescale(pq.s).item() for t in df['time']]
#
# pars = {'settings_name' : settings_name,
#         'binned_data_settings' : binned_data_settings,
#         'combo' : combo,
#         'experiments' : experiments,
#         'min_trials_per_class' : min_trials_per_class,
#         'min_units' : min_units,
#         'decoder_name' : decoder_name,
#         'n_splits' : n_splits,
#         'spike_bin_centers' : bc_aligned,
#         'spike_bin_edges' : be_aligned,
#         'n_repeats' : n_repeats}
# # 'score_name' : score_name,
# # 'shuffle_kfold' : shuffle_kfold,
# # not strictly user defined
#
# for k in binned_data_pars.keys() :
#     pars[k] = binned_data_pars[k]
#
# out = {'pars' : pars,
#        'decoding_scores' : df}
#
# print('Saving output to {}'.format(output_full_path))
# pickle.dump(out, open(output_full_path, 'wb'))