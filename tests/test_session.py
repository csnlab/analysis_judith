import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder


session_id = SESSION_IDS[1]
session = Session(session_id=session_id,  data_version='data_V6_22_03')
session.load_data(trial_preprocessing=False)


tf = session.tf
uf = session.uf



for col in tf.columns:
    id_nan = tf[tf[col].isna()].index
    if len(id_nan) > 0:
        print('\n{} contains {} nans'.format(col, len(id_nan)))
        for id in id_nan:
            print('{}'.format(id))
