import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns

sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))
session_id = sf['ID'].iloc[0]

plot_format = 'svg'

# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'trial_duration_diagnostic')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

for session_id in SESSION_IDS:

    session = Session(session_id=session_id)
    session.load_data()
    tf = session.tf

    tf['trial_duration'] = (tf['trial_end'] - tf['trial_start'])/1000
    tf['first_poke_time_aligned'] = (tf['first_poke_time'] - tf['stimulus_on'])/1000

    f, ax = plt.subplots(1 ,1, figsize=[5, 5])
    sns.kdeplot(tf['first_poke_time_aligned'])
    plot_name = 'distribution_reaction_time_{}.{}'.format(session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)
    plt.close()


    n_trials = len(tf)

    f, ax = plt.subplots(1 ,2, figsize=[10, 20], sharey=True, sharex=False)

    ax[0].barh(np.arange(n_trials), tf['trial_duration'],  linewidth=0)
    ax[0].set_yticks(np.arange(n_trials))
    ax[0].set_yticklabels(tf.index)
    ax[0].invert_yaxis()
    ax[0].tick_params(axis='both', which='major', labelsize=6)
    ax[0].set_ylim(-1, n_trials+2)
    ax[0].set_xlabel('Time [s]')
    ax[0].set_title('Trial duration\n(trial_end-trial_start)')

    ax[1].barh(np.arange(n_trials), tf['first_poke_time_aligned'],  linewidth=0)
    ax[1].invert_yaxis()
    ax[1].tick_params(axis='both', which='major', labelsize=6)
    ax[1].set_xlabel('Time [s]')
    ax[1].set_title('Reaction time\n(first_poke_time-stimulus_on)')


    ax[0].set_xlim([0, 30])

    ax[1].set_xlim([0, min(10, ax[1].get_xlim()[1])])

    plt.tight_layout()
    sns.despine()

    plot_name = 'trial_duration_{}.{}'.format(session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()

