import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from utils import *

binned_data_settings = 'nov22_first_poke'
binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


SESSION_IDS = get_preprocessed_sessions()


session_id = SESSION_IDS[2]


session = Session(session_id=session_id)
session.load_data()
session.load_binned_data(binned_data_settings=binned_data_settings,
                         bad_units=bad_units)
session.align_event_times(align_event=binned_data_pars['align_event'],
                          convert_to_seconds=True)
session.filter_trials(
    max_reaction_time=binned_data_pars['max_reaction_time'])

bd = session.binned_data

tf = session.tf

f, ax = plt.subplots(1, 1)
time = bd['bin_centers_aligned']

tids = tf[tf['response_location']==4].index

for tid in tids:
    x = bd['binned_trackerdata'][tid][0, :]
    y = bd['binned_trackerdata'][tid][1, :]
    ax.plot(time, y, c='r')

tids = tf[tf['response_location'] == 7].index

for tid in tids :
    x = bd['binned_trackerdata'][tid][0, :]
    y = bd['binned_trackerdata'][tid][1, :]
    ax.plot(time, y, c='b')


