import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from matplotlib import gridspec
import seaborn  as sns
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter

# TODO add left vs right sorted by block performance


sf = pd.read_csv(os.path.join(DATA_PATH_csv, 'session_overview.csv'))

settings_name = 'mar15'
plot_format = 'png'


spatial_bin_size = 8
automatic_limits = False
x_max = 576
y_max = 720


# --- set up plots folder ------------------------------------------------------
plots_folder = os.path.join(DATA_PATH, 'plots', 'alignment_diagnostic'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)




for session_id in SESSION_IDS:

    session = Session(session_id=session_id, data_version='data_mar9')
    # data = pickle.load(
    #     open(os.path.join(session.data_path, '{}.pickle'.format(session.session_id)),
    #          'rb'))

    session.load_data()


    # --- SELECT TRIALS ---
    #trial_ids = session.tf[np.isin(session.tf['response_location'], [2, 7])].index
    trial_ids = session.tf.index

    uf = session.uf
    tf = session.tf

    all_spikes = np.hstack([session.spiketrains[u] for u in session.spiketrains.keys()])
    first_spike = all_spikes.min()
    last_spike = all_spikes.max()

    first_trial = tf['trial_start'].iloc[0]
    last_trial = tf['trial_end'].iloc[-1]

    pos_time = session.trackerdata_filtered['time']
    pos_head = session.trackerdata_filtered['head'].copy()

    mask = ~np.isnan(pos_time)
    pos_time = pos_time[mask]
    pos_head['x'] = pos_head['x'][mask]
    pos_head['y'] = pos_head['y'][mask]

    f, ax = plt.subplots(1, 1)
    ax.plot(pos_time, pos_head['x'], c='r', label='x position')
    ax.plot(pos_time, pos_head['y'], c='b', label='y position')
    ax.axvline(first_spike, c='grey', ls='--', label='First and last spikes')
    ax.axvline(last_spike, c='grey', ls='--')
    ax.axvline(first_trial, c='k', ls='--', label='First and last trial')
    ax.axvline(last_trial, c='k', ls='--')
    ax.legend()
    ax.set_title('Position - {}'.format(session_id))
    ax.set_xlabel('Time [ms]')
    ax.set_ylabel('Pixels')
    sns.despine()
    plt.tight_layout()


    plot_name = 'position_over_time_{}.{}'.format(session_id,  plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()


    smooth_sigma = 1
    session = Session(session_id=session_id, data_version='data_mar9_realigned')
    session.load_data()
    occupancy = session.compute_spatial_occupancy(spatial_bin_size=spatial_bin_size,
                                                  xmax=x_max,
                                                  ymax=y_max,
                                                  trial_ids=None,
                                                  smooth_sigma=smooth_sigma)

    f, ax = plt.subplots(1, 1)
    ax.imshow(occupancy.T, extent=[0, x_max, 0, y_max], cmap='jet')
    ax.set_xticks([0, x_max])
    ax.set_yticks([0, y_max])
    sns.despine()

    plot_name = 'occupancy_{}.{}'.format(session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()




    smooth_sigma = 1
    session = Session(session_id=session_id, data_version='data_mar9_realigned')
    session.load_data()
    tf = session.tf
    #trial_ids = 'all'
    combo = 'all'
    trial_ids = tf[tf['response_location'] == 2].index


    pos_time = session.trackerdata_filtered['time']
    pos_head_x = session.trackerdata_filtered['head']['x']
    pos_head_y = session.trackerdata_filtered['head']['y']

    # --- REMOVE NANS ---
    mask = ~np.isnan(pos_time)
    pos_time = pos_time[mask]
    pos_head_x = pos_head_x[mask]
    pos_head_y = pos_head_y[mask]

    # --- BIN ---
    if automatic_limits:
        x_max = np.max(pos_head_x+1)
        y_max = np.max(pos_head_y+1)
    x_bins = np.arange(0, x_max, spatial_bin_size)
    y_bins = np.arange(0, y_max, spatial_bin_size)
    x_binned = np.digitize(pos_head_x, bins=x_bins) - 1
    y_binned = np.digitize(pos_head_y, bins=y_bins) - 1

    # --- SELECT TRIALS ---
    if combo == 'all':
        # restrict to portion of the task
        first_trial = tf['trial_start'].iloc[0]
        last_trial = tf['trial_end'].iloc[-1]
        mask = np.logical_and(pos_time >= first_trial, pos_time <= last_trial)
        pos_time = pos_time[mask]
        x_binned = x_binned[mask]
        y_binned = y_binned[mask]
    else:
        pos_time_all = []
        x_binned_all, y_binned_all = [], []
        for trial_id, row in session.tf.loc[trial_ids].iterrows():
            tstart, tend = row['trial_start'], row['trial_end']
            mask = np.logical_and(pos_time >= tstart, pos_time <= tend)
            pos_time_all.append(pos_time[mask])
            x_binned_all.append(x_binned[mask])
            y_binned_all.append(y_binned[mask])
        pos_time = np.hstack(pos_time_all)
        x_binned = np.hstack(x_binned_all)
        y_binned = np.hstack(y_binned_all)

    occupancy = np.zeros(shape=[len(x_bins), len(y_bins)])
    for i, t in enumerate(pos_time) :
        occupancy[x_binned[i], y_binned[i]] += 1

    occupancy = gaussian_filter(occupancy, sigma=smooth_sigma)

    f, ax = plt.subplots(1, 1)
    ax.imshow(occupancy.T, extent=[0, x_max, 0, y_max], cmap='jet')
    ax.set_xticks([0, x_max])
    ax.set_yticks([0, y_max])
    sns.despine()

    plot_name = 'occupancy_{}.{}'.format(session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

    plt.close()


