import os
from constants import *
import pandas as pd
import quantities as pq
import numpy as np
import pickle
from session import Session
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from utils import *

binned_data_settings = 'nov22_first_poke'
binned_data_pars = load_binned_data_pars(binned_data_settings=binned_data_settings)


SESSION_IDS = get_preprocessed_sessions()

session = Session(session_id=SESSION_IDS[3])

session.load_data(compute_velocity=False)

time = session.trackerdata['time']
trial_ids = session.tf.index
tracker_names = ['MeanBody_X', 'MeanBody_Y']

# we look 10 seconds around PokeC_off, but can be extended
align_event = 'PokeC_off'
time_before_in_s = 8
time_after_in_s = 8

trial_starts, trial_ends, align_event_times = session.get_aligned_epoch_times(
    trial_ids=trial_ids,
    align_event=align_event,
    time_before_in_s=time_before_in_s,
    time_after_in_s=time_after_in_s)


for tracker_name in tracker_names :
    session.tf['perc_coverage_{}'.format(tracker_name)] = 0

for i, (tid, t_start, t_stop) in enumerate(
        zip(trial_ids, trial_starts, trial_ends)) :
    time_mask = np.logical_and(time >= t_start, time < t_stop)
    time_trial = time[time_mask]

    for tracker_name in tracker_names :
        tracking = session.trackerdata[tracker_name]
        tracking_trial = tracking[time_mask]
        perc_points = 100 * tracking_trial.isna().sum() / tracking_trial.shape[0]
        coverage = 100 - perc_points
        col = 'perc_coverage_{}'.format(tracker_name)
        session.tf.loc[tid, col] = coverage

np.testing.assert_array_equal(session.tf['perc_coverage_{}'.format(tracker_names[0])],
                              session.tf['perc_coverage_{}'.format(tracker_names[1])])
col = 'perc_coverage_{}'.format(tracker_names[0])
n = (session.tf[col] < 100).sum()

session.tf = session.tf[session.tf[col] == 100]

tf = session.tf



f, ax = plt.subplots(1, 1)
ax.plot(session.trackerdata['MeanBody_X'])
indx = np.where(np.isnan(session.trackerdata['MeanBody_X']))[0]
ax.scatter(indx, np.repeat(1, len(indx)), c='r')


td = session.trackerdata

cm = 0.5
frame_period = 40 * pq.ms
vel = (cm * pq.cm / frame_period).rescale(pq.m / pq.s)


tf = session.tf
td = session.trackerdata
x = td['MeanBody_X']
y = td['MeanBody_Y']
time = td['time']

fp = session.tf['first_poke_time']

start_time = fp[10]
end_time = fp[16]

indx = np.where(np.logical_and(time >= start_time, time <= end_time))[0]

f, ax = plt.subplots(1, 1)
#ax.scatter(x[indx], y[indx], c='k', s=8)


ax.plot(time[indx], x[indx], c='k')
ax.plot(time[indx], y[indx], c='b')
for t in fp[10:16]:
    ax.axvline(t, c='r', ls=':')



for session_id in [SESSION_IDS[1]]:
    session = Session(session_id=session_id)
    session.load_data()
    session.load_binned_data(binned_data_settings=binned_data_settings,
                             bad_units=bad_units)
    session.align_event_times(align_event=binned_data_pars['align_event'],
                              convert_to_seconds=True)
    session.filter_trials(
        max_reaction_time=binned_data_pars['max_reaction_time'])

    bd = session.binned_data

    tf = session.tf

    f, ax = plt.subplots(1, 1)
    time = bd['bin_centers_aligned']

    tids = tf[tf['response_location']==2].index

    for tid in tids:
        x = bd['binned_trackerdata'][tid][2, :]
        y = bd['binned_trackerdata'][tid][3, :]
        ax.plot(time, y, c='r')

    tids = tf[tf['response_location'] == 7].index

    for tid in tids :
        x = bd['binned_trackerdata'][tid][2, :]
        y = bd['binned_trackerdata'][tid][3, :]
        ax.plot(time, y, c='b')


        ax.plot(time, y)
